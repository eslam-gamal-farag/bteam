<?php

namespace App\Exports;

use App\Models\Calendar;
use Maatwebsite\Excel\Concerns\FromCollection;

class CalendarsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $orgId = session()->get('organization_id');

        return Calendar::all()->where('organization_id','=',$orgId);
    }
}
