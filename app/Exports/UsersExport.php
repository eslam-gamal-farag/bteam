<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection() {
        $orgId = session()->get('organization_id');

        $orgUsers = \App\Models\Organization::find($orgId)->users()->get();
        
        return $orgUsers;
    }
}
