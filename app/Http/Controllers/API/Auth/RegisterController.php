<?php namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;

class RegisterController extends AppBaseController {
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;
	/** @var  UserRepository */
	private $userRepository;
	/** @var  OrganizationRepository */
	private $organizationRepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(UserRepository $userRepo, OrganizationRepository $organizationRepo) {
		$this->middleware('guest')->except('registerDevice');
		$this->middleware(['auth:api', 'verified'])->only('registerDevice');
		$this->userRepository = $userRepo;
		$this->organizationRepository = $organizationRepo;
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data) {
		return Validator::make($data, [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\Models\User
	 */
	protected function create(array $input) {
		$input['password'] = Hash::make($input['password']);
		$user = $this->userRepository->create($input);

		$email = $input['email'];
		$domain_name = substr(strrchr($email, "@"), 1);
		$organizations = $this->organizationRepository->all();
		$org = $organizations->whereIn('allowed_domains', $domain_name)->first()->pluck('id');
		$user->organizations()->sync($org);
		$user->syncRoles('orguser');

		return $user;
	}

	/**
	 * Override default register method from RegistersUsers trait
	 *
	 * @param array $request
	 * @return response
	 */
	public function register(Request $request) {
		$this->validator($request->all())->validate();
		$user = $this->create($request->all());
		return $this->registered($request, $user)
										?: $this->sendResponse($user, 'We have sent an activation link to your email. Please verify your account.');
	}

	/**
	 * Register a device token for PN
	 *
	 * @param array $request
	 * @return response
	 */
	public function registerDevice(Request $request) {
		$user = $request->user();
		$user->device_token = $request->get('device_token');
		$user->save();
		return $this->sendResponse($user, 'Device registered successfully');
	}
}
