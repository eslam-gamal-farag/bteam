<?php namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationController extends AppBaseController {
	use VerifiesEmails;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth:api');
		$this->middleware('signed:api')->only('verify');
		$this->middleware('throttle:6,1')->only('verify', 'resend');
	}

	public function resend(Request $request) {
		$user = $request->user();
		$status = 422;
		$response = [
			'success' => false,
			'message' => 'User already have verified email!'
		];
		if(!$user->hasVerifiedEmail()) {
			$status = 200;
			$this->sendVerificationEmail($user);
			$response = [
				'success' => true,
				'message' => 'A fresh verification link has been sent to your email address.'
			];
		}
		return response()->json($response, $status);
	}

	protected function sendVerificationEmail($user) {
		$data = [];
		$data['api_url'] = \URL::temporarySignedRoute(
			'api.verification.verify', \Carbon::now()->addMinutes(60), ['id' => $user->getKey()]
		);
		$data['url'] = \URL::temporarySignedRoute(
			'verification.verify', \Carbon::now()->addMinutes(60), ['id' => $user->getKey()]
		);
		$api_verification = parse_url($data['api_url']);
		parse_str($api_verification['query'], $data['api_verification']);
		return \Mail::send('auth.emails.verify', $data, function ($message) use($user) {
			$message->to($user->email, $user->name);
			$message->subject('Verify Your Account');
		});
	}

	/**
	 * Mark the authenticated user's email address as verified.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function verify(Request $request) {
		$user = $request->user();
		$response = ['success' => false];
		if ($request->route('id') == $user->getKey() &&
			$user->markEmailAsVerified()) {
				$response['success'] = true;
				$response['message'] = 'Email verified';
		}
		return response()->json($response);
	}

}
