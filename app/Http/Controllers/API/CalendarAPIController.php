<?php namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCalendarAPIRequest;
use App\Http\Requests\API\UpdateCalendarAPIRequest;
use App\Models\Calendar;
use App\Repositories\CalendarRepository;
use App\Models\User;
use App\Notifications\PickupRequest;
use App\Notifications\PickupAccept;
use App\Notifications\PickupDeny;
use App\Notifications\CoverRequest;
use App\Notifications\CoverAccept;
use App\Notifications\CoverDeny;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use Notification;

/**
 * Class CalendarController
 * @package App\Http\Controllers\API
 */

class CalendarAPIController extends AppBaseController {
	/** @var  CalendarRepository */
	private $calendarRepository;

	public function __construct(CalendarRepository $calendarRepo) {
		$this->middleware(['auth:api', 'verified', 'can:manage-calendars'])->except(['index', 'store', 'update']);
		$this->middleware(['auth:api', 'verified']);
		$this->calendarRepository = $calendarRepo;
	}

	/**
 * Display a listing of the Calendar.
 * GET|HEAD /calendars
 *
 * @param Request $request
 * @return Response
 */
	public function index(Request $request) {
		$this->calendarRepository->pushCriteria(new RequestCriteria($request));
		$this->calendarRepository->pushCriteria(new LimitOffsetCriteria($request));
		$calendars = $this->calendarRepository->all();

		foreach($calendars as $calendar) {
			if(strtolower($calendar->category) != 'shift') continue;
			$users = $calendar->users;
			foreach($users as $user){
				if($user->pivot->status == null && $user->id == $request->user()->id) {
					$calendar->users()->detach($request->user()->id);
					$calendar->users()->attach($request->user()->id, ['status' => true]);
				}
			}
		}

		$user = Auth::user();
		$events = [];
		if($orgId = $request->get('orgId', null)) {
			//Return Organization Events
			$allEvents = $user->events->where('organization_id', '=', $orgId);
			$events['events'] = $allEvents->where('status', '=', 1)->values();
			$events['requests'] = $this->calendarRepository->findWhere(['category' => 'cover', 'status' => 1])->load(['user', 'users'])->values();
			$events['requests'] = $events['requests']->merge($allEvents->where('status', '!=', 1)->load(['user', 'users'])->values());
			$orgadmin = \App\Models\Role::where('name', '=', 'orgadmin')->first()->id;
			$role_id = $user->organizations->where('id', '=', $orgId)->first()->pivot->role_id;
			$events['admin'] = [];
			if($orgadmin == $role_id) {
				$events['admin'] = $this->calendarRepository->findWhere(['status' => 0])->load(['user', 'users'])->values();
			}
		}
		return $this->sendResponse($events,'All Organization Events');
	}

	/**
 * Store a newly created Calendar in storage.
 * POST /calendars
 *
 * @param CreateCalendarAPIRequest $request
 *
 * @return Response
 */
	public function store(CreateCalendarAPIRequest $request) {
		$user = Auth::user();
		$input = $request->all();
		$input['status'] = 0;
		$input['creator_id'] = $user->id;
		$input['organization_id'] = $user->organizations->first()->id;
		$input['users'] = [$user->id];
		$calendars = $this->calendarRepository->create($input);

		return $this->sendResponse($calendars, 'Saved successfully');
	}

	/**
 * Display the specified Calendar.
 * GET|HEAD /calendars/{id}
 *
 * @param  int $id
 *
 * @return Response
 */
	public function show($id) {
		/** @var Calendar $calendar */
		$calendar = $this->calendarRepository->findWithoutFail($id);

		if (empty($calendar)) {
			return $this->sendError('Calendar not found');
		}

		return $this->sendResponse($calendar->toArray(), 'Calendar retrieved successfully');
	}

	/**
 * Update the specified Calendar in storage.
 * PUT/PATCH /calendars/{id}
 *
 * @param  int $id
 * @param UpdateCalendarAPIRequest $request
 *
 * @return Response
 */
	public function update($id, Request $request) {
		$input = $request->all();

		/** @var Calendar $calendar */
		$calendar = $this->calendarRepository->findWithoutFail($id);

		if (empty($calendar)) {
			return $this->sendError('Calendar not found');
		}

		$category = strtolower($calendar->category);

		$orgId = $calendar->organization_id;
		$user = \Auth::user()->load('employee');
		$departmentId = $user->employee->department_id;
		$managerId = \App\Models\Department::find($departmentId)->manager_id;
		$DepartmentManager = $user->employee->department->manager;
		$DepartmentManagerEmail = $user->employee->department->manager->email;

		//Request Shift Cover
		if($request->get('type') == 'cover' && $category == 'shift' && $user->events->contains($calendar) && $calendar->status == 1) {
			$calendar->category = 'cover';
			$calendar->status = 0;

			foreach($calendar->users as $calendarUser){
				if($calendarUser->id == $user->id){
					$calendarUser->events()->detach($id);
					$calendarUser->events()->attach($id, ['status' => 0]);
				}
			}

			//Notify Department Manager by mail, and User by notification
			Notification::route('mail',$DepartmentManagerEmail)->notify(new CoverRequest($request));
			//send push notification
			Notification::send($DepartmentManager, new CoverRequest($request));
		} elseif ($request->get('type') == 'pickup' && $category == 'cover' && $calendar->status == 1) {
			//Check if user have time	to pick up the shift
			foreach($user->events as $event){
				$isAvailable = $this->checkTimeOverlap($event->from_time, $event->to_time,$request->from_time,$request->to_time);
				if(!$isAvailable){
					return $this->sendError('There is Time Overlapping, You can not pickup this shift');
				}
			}
			$calendar->category = 'shift';
			$calendar->status = 1;
			$calendar->users()->attach([[
				'user_id' => $user->id,
				'status' => true,
			]]);
			//Notify Department Manager by mail, and User by notification
			Notification::route('mail',$DepartmentManagerEmail)->notify(new PickupRequest($request));
			//send push notification
			Notification::send($DepartmentManager, new PickupRequest($request));
		}
			//Accept Cover/Pickup Request
			if($request->has('accept')) {
				if($request->get('accept')) {
					//Accept Cover Request
					if($category == 'cover') {
						$calendar->category = 'cover';
						$calendar->status = 1;
						$calendar->users()->detach($user->id);
						$calendar->users()->attach([[
							'user_id' => $user->id,
							'status' => false,
						]]);
						//Notify Department Manager by mail.
						Notification::route('mail',$DepartmentManagerEmail)->notify(new CoverAccept($request));
						//send push notification
						Notification::send($user, new CoverAccept($request));
					} else {
						$calendar->users()->detach($request->user()->id);
						$calendar->users()->attach($request->user()->id, ['status' => true]);
					}
				}
				//Deny Cover Request
				if($request->get('accept') == false && $category == 'cover') {
					//Deny Cover Request
						$calendar->category = 'shift';
						$calendar->status = 1;
						//Add/assign users to the shift.
						$calendar->users()->attach([[
							'user_id' => $user->id,
							'status' => true,
						]]);
						//Notify Department Manager by mail.
						Notification::route('mail',$DepartmentManagerEmail)->notify(new CoverDeny($request));
						Notification::send($user, new CoverDeny($request));
				} else if($request->get('accept') == false) {
					$calendar->users()->detach($request->user()->id);
					$calendar->users()->attach($request->user()->id, ['status' => false]);
				}
			}

		$calendar->load('users');
		$calendar->save();

		return $this->sendResponse($calendar->toArray(), 'Updated successfully');
	}

	/**
 * Remove the specified Calendar from storage.
 * DELETE /calendars/{id}
 *
 * @param  int $id
 *
 * @return Response
 */
	public function destroy($id) {
		/** @var Calendar $calendar */
		$calendar = $this->calendarRepository->findWithoutFail($id);
		$orgadmin = Auth::user()->hasRole('orgadmin');

		if (!$orgadmin){
			return $this->sendError('Calendar can not be deleted, Only Organization Admin Can Delete Events');
		}

		if (empty($calendar)) {
			return $this->sendError('Calendar not found');
		}

		$calendar->delete();
		return $this->sendResponse($id, 'Calendar deleted successfully');
	}

	public function checkTimeOverlap($startTime, $endTime,$comparedFrom, $comparedTo){
		$startTime = strtotime($comparedFrom);
		$endTime   = strtotime($comparedTo);

		$chkStartTime = strtotime($startTime);
		$chkEndTime   = strtotime($endTime);

		if($chkStartTime > $startTime && $chkEndTime < $endTime)
		{	//-> Check time is in between start and end time
			return $this->sendError('Time is in between start and end time');
		}elseif(($chkStartTime > $startTime && $chkStartTime < $endTime) || ($chkEndTime > $startTime && $chkEndTime < $endTime))
		{	//-> Check start or end time is in between start and end time
			return $this->sendError('Check start or end Time is in between start and end time');
		}elseif($chkStartTime==$startTime || $chkEndTime==$endTime)
		{	//-> Check start or end time is at the border of start and end time
			return $this->sendError('Check start or end Time is at the border of start and end time');
		}elseif($startTime > $chkStartTime && $endTime < $chkEndTime)
		{	//-> start and end time is in between  the check start and end time.
			return $this->sendError('start and end Time is overlapping  check start and end time');
		} else {
			return true;
		}
	}
}
