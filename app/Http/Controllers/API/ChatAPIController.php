<?php namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use ConsoleTVs\Profanity\Facades\Profanity;
use App\Notifications\MessageSent;
use App\Models\User;
use Auth;
use Chat;
use Response;
use Notification;

class ChatAPIController extends AppBaseController {

	public function __construct() {
		$this->middleware(['auth:api', 'verified']);
	}

	public function createChat(Request $request) {
		$data = [
			'title' => $request->get('title', ''),
			'description' => $request->get('description', '')
		];
		$participants = $request->get('users', []);
		array_push($participants, $request->user()->id);
		$conversation = null;
		if(count($participants) <= 1) {
			return $this->sendError('Cannot create an empty conversation');
		} else if(count($participants) == 2) {
			$conversation = Chat::conversations()->between($participants[0], $participants[1]);
		}
		if(empty($conversation)) {
			$conversation = Chat::createConversation($participants, $data);
		}
		$conv_arr = $conversation->load('users')->toArray();
		$conv_arr['private'] = count($participants) <= 2;
		return $this->sendResponse($conv_arr, 'Conversation Created Successfully');
	}

	public function getConversations(Request $request) {
		$user = Auth::user();
		$conversations = Chat::conversations()->for($user)->setPaginationParams([
			'page' => $request->get('page', 1),
			'perPage' => 25,
			'sorting' => "desc",
		])->get();
		$conv_arr = $conversations->toArray();
		foreach ($conversations->items() as $key => $conversation) {
			$conversation->load('users.employee');
			$conv_arr[$key]['users'] = $conversation->users;
		}
		return $this->sendResponse($conversations, 'Conversations Retrieved Successfully');
	}

	public function getConversationById($id, Request $request) {
		$user = Auth::user();
		$conversation = Chat::conversations()->getById($id)->load(['users.employee', 'messages.sender.employee']);
		Chat::conversation($conversation)->for($user)->readAll();
		$messages = $conversation->messages->sortBy('created_at')->groupBy(function($message) {
			return \Carbon\Carbon::parse($message->created_at)->format('m-d-y');
		});
		$conv_arr = $conversation->toArray();
		$conv_arr['messages'] = $messages;
		return $this->sendResponse($conv_arr, 'Conversation Retrieved Successfully');
	}

	public function sendMessage($id, Request $request) {
		$user = Auth::user();
		$conversation = Chat::conversations()->getById($id);
		$text = $this->wordFilter($request->get('message'));
		$type = $request->get('type', 'text');
		$message = Chat::message($text)
			->from($user)
			->to($conversation)
			->type($type)
			->send();
		Notification::send($conversation->users, new MessageSent($message));
		return $this->sendResponse($message, 'Message Sent Successfully');
	}

	public function getMessageById($id, Request $request) {
		$message = Chat::messages()->getById($id);
		return $this->sendResponse($message, 'Message Retrieved Successfully');
	}

	public function markMessageAsRead($id, Request $request) {
		$user = Auth::user();
		$message = Chat::messages()->getById($id);
		Chat::message($message)->for($user)->markRead();
		return $this->sendResponse($message, 'Message Marked as read Successfully');
	}

	public function unreadMessagesCount(Request $request) {
		$user = Auth::user();
		$unreadCount = Chat::messages()->for($user)->unreadCount();
		return $this->sendResponse($unreadCount, 'Unread Messages Count');
	}

	public function unreadMessagesCountPerConv($id, Request $request) {
		$user = Auth::user();
		$conversation = Chat::conversations()->getById($id);
		$unreadCount = Chat::conversation($conversation)->for($user)->unreadCount();
		return $this->sendResponse($unreadCount, 'Unread Messages Count For This Conversation');
	}

	public function getConversationBetweenTwoUsers(Request $request) {
		$user1 = \App\Models\User::find($request->input('user1'));
		$user2 = \App\Models\User::find($request->input('user2'));
		$conversation = Chat::conversations()->between($user1, $user2);
		return $this->sendResponse($conversation, 'Conversation Returned Successfully.');
	}

	public function addUserToConversation($id, Request $request) {
		$user = $request->get('users', []);
		$conversation = Chat::conversations()->getById($id);
		$result = Chat::conversation($conversation)->addParticipants($users);
		return $this->sendResponse($result, 'Users Added Successfully.');
	}

	public function removeUserFromConversation($id, Request $request) {
		$user = $request->get('users', []);
		$conversation = Chat::conversations()->getById($id);
		$result = Chat::conversation($conversation)->removeParticipants($users);
		return $this->sendResponse($result, 'Users Removed Successfully.');
	}

	private function wordFilter($words) {
		//Library Used: https://github.com/ConsoleTVs/Profanity/tree/master/Dictionaries
		return Profanity::blocker($words)->filter();
	}
}
