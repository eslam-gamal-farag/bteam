<?php namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class EmployeeAPIController
 * @package App\Http\Controllers\API
 */
class EmployeeAPIController extends AppBaseController {

	public function __construct() {
		$this->middleware(['auth:api', 'verified']);
	}

	/**
	 * Get a listing of the Employees.
	 * GET|HEAD /employees
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$user = $request->user();
		$orgId = $request->get('orgId');
		$organization = $user->organizations->where('id', '=', $orgId)->first();
		$users = $organization? $organization->users->where('id', '!=', $user->id)->load('employee')->values() : [];
		return $this->sendResponse($users, 'Employees retrieved successfully');
	}
}
