<?php namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrganizationVendorsAPIRequest;
use App\Http\Requests\API\UpdateOrganizationVendorsAPIRequest;
use App\Models\OrganizationVendors;
use App\Repositories\OrganizationVendorsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class OrganizationVendorsController
 * @package App\Http\Controllers\API
 */

class OrganizationVendorsAPIController extends AppBaseController {
	/** @var  OrganizationVendorsRepository */
	private $organizationVendorsRepository;

	public function __construct(OrganizationVendorsRepository $organizationVendorsRepo) {
		$this->organizationVendorsRepository = $organizationVendorsRepo;
	}

	/**
 * Display a listing of the OrganizationVendors.
 * GET|HEAD /organizationVendors
 *
 * @param Request $request
 * @return Response
 */
	public function index(Request $request) {
		$this->organizationVendorsRepository->pushCriteria(new RequestCriteria($request));
		$this->organizationVendorsRepository->pushCriteria(new LimitOffsetCriteria($request));
		$organizationVendors = $this->organizationVendorsRepository->all();

		return $this->sendResponse($organizationVendors->toArray(), 'Organization Vendors retrieved successfully');
	}

	/**
 * Store a newly created OrganizationVendors in storage.
 * POST /organizationVendors
 *
 * @param CreateOrganizationVendorsAPIRequest $request
 *
 * @return Response
 */
	public function store(CreateOrganizationVendorsAPIRequest $request) {
		$input = $request->all();

		$organizationVendors = $this->organizationVendorsRepository->create($input);

		return $this->sendResponse($organizationVendors->toArray(), 'Organization Vendors saved successfully');
	}

	/**
 * Display the specified OrganizationVendors.
 * GET|HEAD /organizationVendors/{id}
 *
 * @param  int $id
 *
 * @return Response
 */
	public function show($id) {
		/** @var OrganizationVendors $organizationVendors */
		$organizationVendors = $this->organizationVendorsRepository->findWithoutFail($id);

		if (empty($organizationVendors)) {
			return $this->sendError('Organization Vendors not found');
		}

		return $this->sendResponse($organizationVendors->toArray(), 'Organization Vendors retrieved successfully');
	}

	/**
 * Update the specified OrganizationVendors in storage.
 * PUT/PATCH /organizationVendors/{id}
 *
 * @param  int $id
 * @param UpdateOrganizationVendorsAPIRequest $request
 *
 * @return Response
 */
	public function update($id, UpdateOrganizationVendorsAPIRequest $request) {
		$input = $request->all();

		/** @var OrganizationVendors $organizationVendors */
		$organizationVendors = $this->organizationVendorsRepository->findWithoutFail($id);

		if (empty($organizationVendors)) {
			return $this->sendError('Organization Vendors not found');
		}

		$organizationVendors = $this->organizationVendorsRepository->update($input, $id);

		return $this->sendResponse($organizationVendors->toArray(), 'OrganizationVendors updated successfully');
	}

	/**
 * Remove the specified OrganizationVendors from storage.
 * DELETE /organizationVendors/{id}
 *
 * @param  int $id
 *
 * @return Response
 */
	public function destroy($id) {
		/** @var OrganizationVendors $organizationVendors */
		$organizationVendors = $this->organizationVendorsRepository->findWithoutFail($id);

		if (empty($organizationVendors)) {
			return $this->sendError('Organization Vendors not found');
		}

		$organizationVendors->delete();

		return $this->sendResponse($id, 'Organization Vendors deleted successfully');
	}
}
