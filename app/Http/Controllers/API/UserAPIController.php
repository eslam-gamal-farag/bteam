<?php namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController {
	/** @var  UserRepository */
	private $userRepository;
	/** @var  OrganizationRepository */
	private $organizationRepository;

	public function __construct(UserRepository $userRepo, OrganizationRepository $organizationRepo) {
		$this->middleware(['auth:api', 'verified', 'can:manage-users']);
		$this->userRepository = $userRepo;
		$this->organizationRepository = $organizationRepo;
	}


	/**
 * Display a listing of the User.
 * GET|HEAD /users
 *
 * @param Request $request
 * @return Response
 */
	public function index(Request $request) {
		if($request->has('user_id')) {
			$id = $request->get('user_id');
			$user = $this->userRepository->findWithoutFail($id);
			$organizations = $user->organizations;
			$users = collect();
			foreach($organizations as $org) {
				$users = $users->merge($org->users->load('employee'));
			}
			$users = $users->filter(function($user, $key) use($id) {
				return $user->id != $id;
			});
		} else {
			$this->userRepository->pushCriteria(new RequestCriteria($request));
			$this->userRepository->pushCriteria(new LimitOffsetCriteria($request));
			$users = $this->userRepository->all()->load('employee');
		}

		return $this->sendResponse($users->load('employee')->toArray(), 'Users retrieved successfully');
	}

	/**
 * Store a newly created User in storage.
 * POST /users
 *
 * @param CreateUserAPIRequest $request
 *
 * @return Response
 */
	public function store(CreateUserAPIRequest $request) {
		$input = $request->all();

		if($request->get('password', null)) {
			$input['password'] = \Hash::make($input['password']);
		}
		$users = $this->userRepository->create($input);

		$email = $input['email'];
		$domain_name = substr(strrchr($email, "@"), 1);
		$organizations = $this->organizationRepository->all();
		$orgs = $organizations->whereIn('allowed_domains', $domain_name)->pluck('id');
		$users->organizations()->sync($orgs);
		$users->syncRoles('orguser');
		return $this->sendResponse($users->toArray(), 'User saved successfully');
	}

	/**
 * Display the specified User.
 * GET|HEAD /users/{id}
 *
 * @param  int $id
 *
 * @return Response
 */
	public function show($id) {
		/** @var User $user */
		$user = $this->userRepository->findWithoutFail($id)->load('employee');

		if (empty($user)) {
			return $this->sendError('User not found');
		}

		return $this->sendResponse($user->toArray(), 'User retrieved successfully');
	}

	/**
 * Update the specified User in storage.
 * PUT/PATCH /users/{id}
 *
 * @param  int $id
 * @param UpdateUserAPIRequest $request
 *
 * @return Response
 */
	public function update($id, UpdateUserAPIRequest $request) {
		$input = $request->all();

		/** @var User $user */
		$user = $this->userRepository->findWithoutFail($id);

		if (empty($user)) {
			return $this->sendError('User not found');
		}

		$user = $this->userRepository->update($input, $id);

		return $this->sendResponse($user->toArray(), 'User updated successfully');
	}

	/**
 * Remove the specified User from storage.
 * DELETE /users/{id}
 *
 * @param  int $id
 *
 * @return Response
 */
	public function destroy($id) {
		/** @var User $user */
		$user = $this->userRepository->findWithoutFail($id);

		if (empty($user)) {
			return $this->sendError('User not found');
		}

		$user->delete();

		return $this->sendResponse($id, 'User deleted successfully');
	}
}
