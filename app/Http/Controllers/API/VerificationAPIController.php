<?php namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationAPIController extends AppBaseController {
	use VerifiesEmails;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth:api');
		$this->middleware('signed')->only('verify');
		$this->middleware('throttle:6,1')->only('verify', 'resend');
	}

	/**
	 * Mark the authenticated user's email address as verified.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function verify(Request $request) {
		$user = $request->user();
		dd($user->hasVerifiedEmail());
		if ($request->route('id') == $user->getKey() &&
			$user->markEmailAsVerified()) {
			event(new Verified($user));
		}
		return response()->json([
			'message' => 'Email verified!',
			'user' => new UserResource($user)
		]);
	}

}
