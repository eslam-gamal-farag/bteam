<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VoximplantAPIController extends Controller
{
    private $account_email = 'a.shaltout@momentum-sol.com';
    private $account_password = 'momentum@123';
    private $session_id;
    private $account_id;
    private $api_key;
    private $application_id;
    private $application_name;
    private $admin_role_id;
    private $admin_user_api_key;

    //Login to Voximplant
    public function Login($account_email, $account_password){
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/?account_email='.$account_email.'&account_password='.$account_password);
        $response = $response->getBody()->getContents();
        
        $this->$session_id = $response->result;
        $this->account_id = $response->account_id;
        $this->api_key = $response->api_key;

        return $response;
	}
    //Logout from Voximplant
	public function Logout($account_id, $session_id){
		$client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.voximplant.com/platform_api/Logout/?account_id='. $account_id .'&session_id='.$session_id);
        $response = $response->getBody()->getContents();
        return $response;
	}
    //Create Application on Voximplant
	public function AddApplication($account_id, $api_key, $application_name){
		$client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.voximplant.com/platform_api/AddApplication/?account_id='.$account_id.'&api_key='. $api_key .'&application_name='. $application_name);
        $response = $response->getBody()->getContents();

        $this->application_id = $response->application_id;
        $this->application_name = $response->application_name;

        return $response;
	}
    //Delete Application from Voximplant
    public function DeleteApplication($account_id, $api_key, $application_id){
		$client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.voximplant.com/platform_api/DelApplication/?account_id='. $account_id .'&api_key='. $api_key .'&application_id='.$application_id);
        $response = $response->getBody()->getContents();

        return $response;
    }
    //Add Admin Role in Voximplant
	//allowed_entries Ex: (GetAccountInfo;GetCallHistory)
	public function AddAdminRole($admin_role_name, $allowed_entries){
		$client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.voximplant.com/platform_api/AddAdminRole/?account_id='. $voxi_account_id .'&api_key='. $api_key .'&admin_role_name='.$admin_role_name.'&allowed_entries='. $allowed_entries);
        $response = $response->getBody()->getContents();

        $this->admin_role_id = $response->admin_role_id;

        return $response;
    }
    //Delete Admin Role from Voximplant
    public function DeleteAdminRole($account_id, $api_key, $admin_role_id){
		$client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.voximplant.com/platform_api/DelAdminRole/?account_id='. $account_id .'&api_key='. $api_key .'&admin_role_id='.$admin_role_id);
        $response = $response->getBody()->getContents();

        return $response;
	}
    //Add Admin User to Voximplant
	public function AddAdminUser($admin_user_name,$admin_display_name,$admin_password, $admin_role_id){
		$client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.voximplant.com/platform_api/AddAdminUser/?account_id='.$account_id .'&api_key='. $api_key .'&new_admin_user_name='.$admin_user_name.'&admin_user_display_name='.$admin_display_name.'&new_admin_user_password='.$admin_password.'&admin_role_id=' .$admin_role_id);
        $response = $response->getBody()->getContents();

        $this->admin_user_api_key = $response->admin_user_api_key;

        return $response;
    }
    //Delete Admin User from Voximplant
    public function DeleteAdminUser($account_id, $api_key, $admin_user_id){
		$client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.voximplant.com/platform_api/DelAdminUser/?account_id='. $account_id .'&api_key='. $api_key .'&required_admin_user_id='.$admin_user_id);
        $response = $response->getBody()->getContents();

        return $response;
    }
    //Add User to Voximplant Application
    public function AddUser($account_id, $api_key, $user_name, $user_display_name, $user_password, $application_id){
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.voximplant.com/platform_api/AddUser/?account_id='.$account_id.'&api_key='.$api_key.'&user_name='.$user_name.'&user_display_name='.$user_display_name.'&user_password='.$user_password.'&application_id='. $application_id);

        $response = $response->getBody()->getContents();
        //returns result(1-0) & user_id
        return $response;
    }
    //Delete User from Voximplant Application
    public function DeleteUser($account_id, $api_key, $user_id, $application_id){
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.voximplant.com/platform_api/DelUser/?account_id='. $account_id .'&api_key='. $api_key .'&user_id='. $user_id.'&application_id='. $application_id);

        $response = $response->getBody()->getContents();
        //returns result(1-0) & user_id
        return $response;
    }
}