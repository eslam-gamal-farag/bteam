<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateCalendarRequest;
use App\Http\Requests\UpdateCalendarRequest;
use App\Repositories\CalendarRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Input;
use App\Notifications\RequestResponse;
use Notification;
use Excel;
use App\Imports\CalendarsImport;
use App\Exports\CalendarsExport;


use \App\Models\Organization;

class CalendarController extends AppBaseController {
	/** @var  CalendarRepository */
	private $calendarRepository;

	public function __construct(CalendarRepository $calendarRepo, OrganizationRepository $organizationRepo,UserRepository $userRepo) {
		$this->middleware('verified');
		$this->calendarRepository = $calendarRepo;
		$this->organizationRepository = $organizationRepo;
		$this->userRepository = $userRepo;
	}

	/**
	 * Display a listing of the Calendar.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$orgId = $request->session()->get('organization_id', null);
		$this->calendarRepository->pushCriteria(new RequestCriteria($request));
		$organization = $this->organizationRepository->findWithoutFail($orgId);
		$calendars = $organization->calendars()->orderBy('created_at', 'desc')->paginate();

		foreach($calendars as $calendar) {
			if(strtolower($calendar->category) != 'shift') continue;
			$users = $calendar->users;
			foreach($users as $user){
				if($user->pivot->status == null && $user->id == \Auth::user()->id) {
					$calendar->users()->detach($request->user()->id);
					$calendar->users()->attach($request->user()->id, ['status' => true]);
				}
			}
		}

		if($request->has('trashed')) {
			$calendars = $organization->calendars()->onlyTrashed()->paginate(10);
		}


		if(Input::has('statusFilter') && Input::has('categoryFilter')) {
			$statusFilter = Input::get('statusFilter');
			$categoryFilter = Input::get('categoryFilter');
			
			$calendars = $organization->calendars()
						->where([['status' , '=' , $statusFilter] , ['category' , '=' , $categoryFilter]])
						->orderBy('created_at', 'desc')
						->paginate();
			return view('calendars.index')->with('calendars', $calendars);
		}

		
		if(Input::has('statusFilter')) {
			$filter = Input::get('statusFilter');
			
			$calendars = $organization->calendars()->where('status' , '=' , $filter)->orderBy('created_at', 'desc')->paginate();
			return view('calendars.index')->with('calendars', $calendars);
		}

		if(Input::has('categoryFilter')) {
			$filter = Input::get('categoryFilter');
			$calendars = $organization->calendars()->where('category' , '=' , $filter)->orderBy('created_at', 'desc')->paginate();
			//return $this->sendResponse($calendars, 'Filtered Calendars');
			return view('calendars.index')->with('calendars', $calendars);
		}

		return view('calendars.index')->with('calendars', $calendars);
	}

	/**
	 * Show the form for creating a new Calendar.
	 *
	 * @return Response
	 */
	public function create() {
		$orgId = session()->get('organization_id', null);
		$organization = $this->organizationRepository->findWithoutFail($orgId);
		$orgUsers = $organization->users->pluck('name', 'id');
		return view('calendars.create')->with('users', $orgUsers);
	}

	/**
	 * Store a newly created Calendar in storage.
	 *
	 * @param CreateCalendarRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCalendarRequest $request) {
		// dd($request->all());
		$input = $request->all();

		$isRepeated = $request->has('repeaterCheck');
		$input['creator_id'] = Auth::user()->id;
		$input['organization_id'] = $request->session()->get('organization_id');

		$category = strtolower($request->get('category'));
		$users = $request->get('users', []);
		$input['users'] = [];
		foreach ($users as $user_id) {
			array_push($input['users'], [
				'user_id' => $user_id,
				'status' => in_array($category, ['meeting', 'shift'])? null : true
			]);
		}

		if($isRepeated) {
			$start_date = $request->input('shift_from_date');
			$end_date = $request->input('shift_to_date');
			$start_time = $request->input('shift_from_time');
			$end_time = $request->input('shift_to_time');
			$days = $request->input('days');
			$period = CarbonPeriod::create($start_date, $end_date);
			$dates = $period->toArray();
			foreach ($dates as $date) {
				if (in_array(strtolower($date->format('l')), $days)){
					$input['from_time'] = $date->format('Y-m-d') . $start_time;
					$input['to_time'] = $date->format('Y-m-d') . $end_time;
					$calendar = $this->calendarRepository->create($input);
					$calendar->status = 1;
					$calendar->save();
				}
			}
			Flash::success('Schedule saved successfully.');
			return redirect(route('calendars.index'));
		}

		$calendar = $this->calendarRepository->create($input);
		$calendar->status = 1;
		$calendar->is_private = 0;
		$calendar->save();

		Flash::success('Calendar saved successfully.');

		return redirect(route('calendars.index'));
}

	/**
	 * Display the specified Calendar.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id) {
		$calendar = $this->calendarRepository->findWithoutFail($id);

		if (empty($calendar)) {
			Flash::error('Calendar not found');

			return redirect(route('calendars.index'));
		}

		return view('calendars.show')->with('calendar', $calendar);
	}

	/**
	 * Show the form for editing the specified Calendar.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id) {
		$calendar = $this->calendarRepository->findWithoutFail($id);
		$users = $this->userRepository->pluck('name', 'id');
		$organizations = $this->organizationRepository->pluck('name', 'id');

		if (empty($calendar)) {
			Flash::error('Calendar not found');

			return redirect(route('calendars.index'));
		}


		return view('calendars.edit')->with('calendar', $calendar)->with('users', $users)->with('organizations', $organizations);
	}

	/**
	 * Update the specified Calendar in storage.
	 *
	 * @param  int			  $id
	 * @param UpdateCalendarRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCalendarRequest $request) {
		$calendar = $this->calendarRepository->findWithoutFail($id);

		// dd($request->all());
		if (empty($calendar)) {
			Flash::error('Calendar not found');

			return redirect(route('calendars.index'));
		}

		// $request['creator_id'] = Auth::user()->id;

		$calendar = $this->calendarRepository->update($request->all(), $id);

		Flash::success('Calendar updated successfully.');

		return redirect(route('calendars.index'));
	}

	/**
	 * Restore the specified Calendar from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function restore($id) {
		$calendar = \App\Models\Calendar::withTrashed()->find($id);
		
		if (empty($calendar)) {
			Flash::error('Calendar not found');
			return redirect(route('calendars.index'));
		}

		$calendar->restore();

		Flash::success('Calendar restored successfully.');
		return redirect(route('calendars.index'));
	}



	/**
	 * Remove the specified Calendar from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id) {
		$calendar = $this->calendarRepository->findWithoutFail($id);

		if (empty($calendar)) {
			$calendar = \App\Models\Calendar::withTrashed()->find($id);
			if($calendar) {
				$calendar->forceDelete();
			} 
		}
		if (empty($calendar)) {
			Flash::error('Calendar not found');
			return redirect(route('calendars.index'));
		}

		$orgadmin = \Auth::user()->hasRole(['orgadmin', 'superadmin', 'admin']);

		if (!$orgadmin){

			Flash::error('Calendar can not be deleted, Only Organization Admin Can Delete Events');
			return redirect(route('calendars.index'));
		}

		$this->calendarRepository->delete($id);

		Flash::success('Calendar deleted successfully.');

		return redirect(route('calendars.index'));
	}
	
	public function getDepUsers(Request $request){
		$orgId = $request->session()->get('organization_id', null);

		// $user = Auth::user();
		// $user->load('employee');

		// $departmentId = $user->employee->department_id;
		// $ids = \App\Models\Department::find($departmentId)->employees->pluck('id');
		// $users = \App\Models\User::whereIn('id', $ids)->get()->pluck('name','id');

			
		$users = [];	
		$departments = \App\Models\Department::where('organization_id', '=', $orgId)->pluck('id');
		$depEmployees = [];

		foreach($departments as $department) {
			$employees = \App\Models\Department::find($department)->employees->pluck('id');
		
			foreach($employees as $employee) {
				$depEmployees[$employee] = \App\Models\User::find($employee)->name;
			}
		
			$users[\App\Models\Department::find($department)->name] = $depEmployees;
		}
	
		return response()->json(['response' => $users]);
	}

	public function approve($id, Request $request){
		$calendar = $this->calendarRepository->findWithoutFail($id);
		$user = $calendar->user;

		$calendar->status = 1;
		$calendar->moderated_at = now();
		$calendar->moderated_by = Auth::user()->id;

		$calendar->save();
		Notification::send($user, new RequestResponse(['accept' => true, 'calendar' => $calendar->toArray(), 'admin' => \Auth::user()->name]));
		return redirect(route('calendars.index'));
	}

	public function reject($id, Request $request){
		$calendar = $this->calendarRepository->findWithoutFail($id);
		$user = $calendar->user;

		$calendar->status = 2;
		$calendar->moderated_at = now();
		$calendar->moderated_by = Auth::user()->id;

		$calendar->save();
		Notification::send($user, new RequestResponse(['accept' => false, 'calendar' => $calendar->toArray(), 'admin' => \Auth::user()->name]));
		return redirect(route('calendars.index'));
	}


	public function import(){
		return view('calendars.import');

	}

	public function importCalendars(Request $request){

		$path = $request->import_file->storeAs('Calendar Imports', $request->import_file->getClientOriginalName());

		Excel::import(new CalendarsImport, $path);

		Flash::success('Calendars Imported successfully.');

		return redirect(route('calendars.index'));
	}

	public function exportCalendars() {
		return Excel::download(new CalendarsExport, 'Calendars.xlsx');
	}

	public function getDownload () {
		$file= public_path(). "/download/calendarsSample.csv";
		$headers = array('Content-Type: application/csv',);

		return Response::download($file, 'calendarsSample.csv', $headers);
	}
}
