<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateContactFormRequest;
use App\Http\Requests\UpdateContactFormRequest;
use App\Repositories\ContactFormRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ContactFormController extends AppBaseController {
	/** @var  ContactFormRepository */
	private $contactFormRepository;

	public function __construct(ContactFormRepository $contactFormRepo) {
		$this->middleware('verified');
		$this->contactFormRepository = $contactFormRepo;
	}

	/**
	 * Display a listing of the ContactForm.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$this->contactFormRepository->pushCriteria(new RequestCriteria($request));
		$contactForms = $this->contactFormRepository->all();

		return view('contact_forms.index')
			->with('contactForms', $contactForms);
	}

	/**
	 * Show the form for creating a new ContactForm.
	 *
	 * @return Response
	 */
	public function create() {
		return view('contact_forms.create');
	}

	/**
	 * Store a newly created ContactForm in storage.
	 *
	 * @param CreateContactFormRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateContactFormRequest $request) {
		$input = $request->all();

		$contactForm = $this->contactFormRepository->create($input);

		Flash::success('Contact Form saved successfully.');

		return redirect(route('contactForms.index'));
	}

	/**
	 * Display the specified ContactForm.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id) {
		$contactForm = $this->contactFormRepository->findWithoutFail($id);

		if (empty($contactForm)) {
			Flash::error('Contact Form not found');

			return redirect(route('contactForms.index'));
		}

		return view('contact_forms.show')->with('contactForm', $contactForm);
	}

	/**
	 * Show the form for editing the specified ContactForm.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id) {
		$contactForm = $this->contactFormRepository->findWithoutFail($id);

		if (empty($contactForm)) {
			Flash::error('Contact Form not found');

			return redirect(route('contactForms.index'));
		}

		return view('contact_forms.edit')->with('contactForm', $contactForm);
	}

	/**
	 * Update the specified ContactForm in storage.
	 *
	 * @param  int			  $id
	 * @param UpdateContactFormRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateContactFormRequest $request) {
		$contactForm = $this->contactFormRepository->findWithoutFail($id);

		if (empty($contactForm)) {
			Flash::error('Contact Form not found');

			return redirect(route('contactForms.index'));
		}

		$contactForm = $this->contactFormRepository->update($request->all(), $id);

		Flash::success('Contact Form updated successfully.');

		return redirect(route('contactForms.index'));
	}

	/**
	 * Remove the specified ContactForm from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id) {
		$contactForm = $this->contactFormRepository->findWithoutFail($id);

		if (empty($contactForm)) {
			Flash::error('Contact Form not found');

			return redirect(route('contactForms.index'));
		}

		$this->contactFormRepository->delete($id);

		Flash::success('Contact Form deleted successfully.');

		return redirect(route('contactForms.index'));
	}
}
