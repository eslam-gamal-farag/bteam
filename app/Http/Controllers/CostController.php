<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon;
use Illuminate\Support\Facades\Input;

class CostController extends Controller {

	public function __construct() {
		$this->middleware('verified');
	}
	
	public function index(Request $request) {
		$orgId = $request->session()->get('organization_id');
		$users = \App\Models\Organization::find($orgId)->users->load('employee');
		$dateS;
		$dateE;
		if(Input::has('laborcostPeriod')){
			// $dateS = Carbon::parse(Input::get('reportStartDate'));
			// $dateE = Carbon::parse(Input::get('reportEndDate'));
			switch (Input::get('laborcostPeriod')) {
				case '1':
				$dateS = new Carbon('- 1 week');
				$dateE = Carbon::now();
					break;
				case '2':
				$dateS = new Carbon('- 2 weeks');
				$dateE = Carbon::now();
					break;
				case '3':
				$dateS = new Carbon('- 1 month');
				$dateE = Carbon::now();
					break;
				case '4':
				$dateS = new Carbon('- 3 months');
				$dateE = Carbon::now();
					break;
				case '5':
				$dateS = new Carbon('- 1 year');
				$dateE = Carbon::now();
					break;
				default:
				$dateS = new Carbon('- 2 weeks');
				$dateE = Carbon::now();
					break;
			}
			// dd($dateS);
			foreach($users as $key => $user) {
				$events = $user->load('employee')->events
												->where('from_time', '>=',$dateS)
												->where('from_time', '<=',$dateE);
				$users[$key]->totalHours = 0;
				foreach($events as $event) {
					if(strtolower($event->category) != 'shift') continue;
					$start  = new Carbon($event->from_time);
					$end	= new Carbon($event->to_time);
					$time = $start->diff($end)->h;
					$time+= $start->diff($end)->i / 60;
					$users[$key]->totalHours += $time;
				}
			}
		} else {
			foreach($users as $key => $user) {
				$events = $user->load('employee')->events;
				$users[$key]->totalHours = 0;
				foreach($events as $event) {
					if(strtolower($event->category) != 'shift') continue;
					$start  = new Carbon($event->from_time);
					$end	= new Carbon($event->to_time);
					$time = $start->diff($end)->h;
					$time+= $start->diff($end)->i / 60;
					$users[$key]->totalHours += $time;
				}
			}
		}

		return view('laborcost')->with('users',$users);
	}
}
