<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateDepartmentRequest;
use App\Http\Requests\UpdateDepartmentRequest;
use App\Repositories\DepartmentRepository;
use App\Repositories\OrganizationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class DepartmentController extends AppBaseController {
	/** @var  DepartmentRepository */
	private $departmentRepository;
	private $organizationRepository;

	public function __construct(DepartmentRepository $departmentRepo, OrganizationRepository $organizationRepo) {
		$this->middleware('verified');
		$this->departmentRepository = $departmentRepo;
		$this->organizationRepository = $organizationRepo;
	}

	/**
	 * Display a listing of the Department.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$this->departmentRepository->pushCriteria(new RequestCriteria($request));
		$org_id = session()->get('organization_id');
		$organization = $this->organizationRepository->find($org_id);
		$departments = $organization->departments()->paginate(10);
		if($request->has('trashed')) {
			$departments = $organization->departments()->onlyTrashed()->paginate(10);
		}

		return view('departments.index')
			->with('departments', $departments);
	}

	/**
	 * Show the form for creating a new Department.
	 *
	 * @return Response
	 */
	public function create() {
		$org_id = session()->get('organization_id');
		$organization = $this->organizationRepository->find($org_id);
		$users = $organization->users()->pluck('name', 'user_id');
		return view('departments.create')->with('users', $users);
	}

	/**
	 * Store a newly created Department in storage.
	 *
	 * @param CreateDepartmentRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDepartmentRequest $request) {
		$orgId = session()->get('organization_id');
		$department = $this->departmentRepository->firstOrNew([
			'name' => strtolower($request->get('name')),
			'organization_id' => $orgId,
			'data' => $request->get('data')
		]);
		$department->manager_id = $request->get('manager_id');
		$department->save();
		Flash::success('Department saved successfully.');

		return redirect(route('floorplans.index'));
	}

	/**
	 * Display the specified Department.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id) {
		$department = $this->departmentRepository->findWithoutFail($id);
		$employees = $department->employees;

		if (empty($department)) {
			Flash::error('Department not found');
			return redirect(route('floorplans.index'));
		}

		return view('departments.show')->with('department', $department)->with('users',$employees);
	}

	/**
	 * Show the form for editing the specified Department.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id) {
		$department = $this->departmentRepository->findWithoutFail($id);
		$org_id = session()->get('organization_id');
		$organization = $this->organizationRepository->find($org_id);
		$users = $organization->users()->pluck('name', 'user_id');

		if (empty($department)) {
			Flash::error('Department not found');
			return redirect(route('floorplans.index'));
		}

		return view('departments.edit')
					->with('users', $users)
					->with('department', $department);
	}

	/**
	 * Update the specified Department in storage.
	 *
	 * @param  int			  $id
	 * @param UpdateDepartmentRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateDepartmentRequest $request) {
		$department = $this->departmentRepository->findWithoutFail($id);

		if (empty($department)) {
			Flash::error('Department not found');

			return redirect(route('floorplans.index'));
		}

		$department = $this->departmentRepository->update($request->all(), $id);

		Flash::success('Department updated successfully.');

		return redirect(route('floorplans.index'));
	}

	/**
	 * Restore the specified Department from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function restore($id) {
		$department = \App\Models\Department::withTrashed()->find($id);
		if (empty($department)) {
			Flash::error('Department not found');
			return redirect(route('floorplans.index'));
		}
		$department->restore();
		Flash::success('Department restored successfully.');
		return redirect(route('floorplans.index'));
	}


	/**
	 * Remove the specified Department from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id) {
	$department = $this->departmentRepository->findWithoutFail($id);
	if (empty($department)) {
		$department = \App\Models\Department::withTrashed()->find($id);
		if($department) {
			$department->forceDelete();
		}
	} else {
		$this->departmentRepository->delete($id);
	}
	if (empty($department)) {
		Flash::error('Department not found');
		return redirect(route('floorplans.index'));
	}
		Flash::success('Department deleted successfully.');
		return redirect(route('floorplans.index'));
	}
}
