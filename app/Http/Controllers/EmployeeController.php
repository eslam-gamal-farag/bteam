<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Repositories\EmployeeRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use \App\Models\Employee;
use \App\Models\User;
use \App\Models\Department;
use \App\Models\Organization;

class EmployeeController extends AppBaseController {
	/** @var  EmployeeRepository */
	private $employeeRepository;
	private $roleRepository;
	private $userRepository;
	private $organizationRepository;

	public function __construct(EmployeeRepository $employeeRepo, RoleRepository $roleRepo, OrganizationRepository $organizationRepo, UserRepository $userRepo) {
		$this->middleware('verified');
		$this->employeeRepository = $employeeRepo;
		$this->userRepository = $userRepo;
		$this->roleRepository = $roleRepo;
		$this->organizationRepository = $organizationRepo;
	}

	/**
	 * Display a listing of the Employee.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$organization = $this->organizationRepository->find(session()->get('organization_id'));
		if($request->has('trashed')) {
			$users = $organization->users()->onlyTrashed()->paginate(10);
		} else {
			$users = $organization->users()->paginate(10) ;
		}
		return view('employees.index')
			->with('users', $users);
	}

	/**
	 * Show the form for creating a new Employee.
	 *
	 * @return Response
	 */
	public function create() {
		$organization = $this->organizationRepository->find(session()->get('organization_id'));
		$roles = $this->roleRepository->findWhereIn('name', ['orgadmin', 'orguser'])->pluck('name', 'id');
		$departments = $organization->departments()->pluck('name', 'id');
		return view('employees.create')
					->with('roles', $roles)
					->with('departments', $departments);
	}

	/**
	 * Store a newly created Employee in storage.
	 *
	 * @param CreateEmployeeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateEmployeeRequest $request) {
		$input = $request->except('organization');

		$input['organizations'] = [[
			'organization_id' => session()->get('organization_id'),
			'role_id' => $request->get('organization')['pivot']['role_id']
		]];
		$input['password'] = str_random(16);

		$user = $this->userRepository->create($input);
		$input['employee']['user_id'] = $user->id;
		$employee = $this->employeeRepository->create($input['employee']);

		Flash::success('Employee saved successfully.');

		return redirect(route('projectresponsibilities.index'));
	}

	/**
	 * Display the specified Employee.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id) {
		$user = $this->userRepository->findWithoutFail($id);
		if (empty($user)) {
			Flash::error('User not found');
			return redirect(route('projectresponsibilities.index'));
		}
		return view('employees.show')->with('user', $user);
	}

	/**
	 * Show the form for editing the specified Employee.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id) {
		$user = $this->userRepository->findWithoutFail($id)->load('organizations', 'employee.department.organization');
		$org_id = session()->get('organization_id');
		$user->organization = $user->organizations->where('id', '=', $org_id)->first();
		$roles = $this->roleRepository->findWhereIn('name', ['orgadmin', 'orguser'])->pluck('name', 'id');
		$departments = $user->organization->departments()->pluck('name', 'id');

		if (empty($user)) {
			Flash::error('User not found');
			return redirect(route('projectresponsibilities.index'));
		}

		return view('employees.edit')
					->with('user', $user)
					->with('roles', $roles)
					->with('departments', $departments);
		}

	/**
	 * Update the specified Employee in storage.
	 *
	 * @param  int			  $id
	 * @param UpdateEmployeeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateEmployeeRequest $request) {
		$user = $this->userRepository->findWithoutFail($id);
		$input = $request->except('employee');
		$input['organizations'] = [[
			'organization_id' => session()->get('organization_id'),
			'role_id' => $request->get('organization')['pivot']['role_id']
		]];

		$employee = null;
		if($user->employee) {
			$employee = $this->employeeRepository->find($user->employee->id);
		}
		$employee_input = $request->all()['employee'];

		if (empty($user)) {
			Flash::error('User not found');
			return redirect(route('projectresponsibilities.index'));
		}

		$user = $this->userRepository->update($input, $id);
		if($employee) {
			$employee = $this->employeeRepository->update($employee_input, $employee->id);
		} else {
			$employee = $user->employee()->create($employee_input);
		}
		Flash::success('Employee updated successfully.');

		return redirect(route('projectresponsibilities.index'));
	}

	/**
	 * Restore the specified Employee from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function restore($id) {
		$user = \App\Models\User::withTrashed()->find($id);
		
		if (empty($user)) {
			Flash::error('User not found');
			return redirect(route('projectresponsibilities.index'));
		}

		$user->restore();

		Flash::success('Employee restored successfully.');
		return redirect(route('projectresponsibilities.index'));
	}
		/**
	 * Remove the specified Employee from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id) {
		$user = $this->userRepository->findWithoutFail($id);

		if (empty($user)) {
			$user = \App\Models\User::withTrashed()->find($id);
			if($user) {
				$user->employee()->forceDelete();
				$user->forceDelete();
			} 
		} else {
			if($user->employee) {
				$this->employeeRepository->delete($user->employee->id);
			}
			$this->userRepository->delete($id);
		}

		if (empty($user)) {
			Flash::error('User not found');
			return redirect(route('projectresponsibilities.index'));
		}

		Flash::success('Employee deleted successfully.');
		return redirect(route('projectresponsibilities.index'));
	}

	public function getUserDepartments(Request $request){
		$user_id = $request->input('userId');
		$organizationIds = User::findWithoutFail($user_id)->organizations->pluck('id');
		$departments = [];

		foreach($organizationIds as $organizationId){
				
			array_push($departments, Department::where('organization_id','=',$organizationId)->pluck('name'));
		}

		return response()->json(['response' => $departments]);
	}
}
