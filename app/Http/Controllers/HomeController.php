<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

class HomeController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('verified');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		return view('home');
	}

	public function orgselect(Request $request){
		$organizations = Auth::user()->organizations;

		if($request->input('organization_id')){
			session()->put('organization_id', $request->input('organization_id'));
			return redirect()->route('home');
		}

		if(Auth::user()->hasRole(['superadmin', 'admin'])){
			$organizations = \App\Models\Organization::all();
		}

		return view('orgselect')->with('organizations', $organizations);
	}
}
