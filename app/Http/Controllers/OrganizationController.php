<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateOrganizationRequest;
use App\Http\Requests\UpdateOrganizationRequest;
use App\Repositories\OrganizationRepository;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class OrganizationController extends AppBaseController {
	/** @var  OrganizationRepository */
	private $organizationRepository;

	public function __construct(OrganizationRepository $organizationRepo, UserRepository $userRepo) {
		$this->middleware(['verified', 'can:manage-orgs']);
		$this->organizationRepository = $organizationRepo;
		$this->userRepository = $userRepo;
	}

	/**
	 * Display a listing of the Organization.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$this->organizationRepository->pushCriteria(new RequestCriteria($request));
		$organizations = $this->organizationRepository->paginate(15);

		return view('organizations.index')
			->with('organizations', $organizations);
	}

	/**
	 * Show the form for creating a new Organization.
	 *
	 * @return Response
	 */
	public function create() {
		$users = $this->userRepository->pluck('name', 'id');
		return view('organizations.create')
					->with('users', $users);
	}

	/**
	 * Store a newly created Organization in storage.
	 *
	 * @param CreateOrganizationRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateOrganizationRequest $request) {
		$input = $request->all();

		$organization = $this->organizationRepository->create($input);

		// $features = ($request->input('features'));
		// $organization->features = json_encode($features);
		// $organization->save();

		// if(in_array('chat',$features)){
		// 	$this->allowChat();
		// }

		// if(in_array('voiceCalls',$features)){
		// 	$this->allowVoiceCalls();
		// }

		// if(in_array('videoCalls',$features)){
		// 	$this->allowVideoCalls();
		// }

		Flash::success('Organization saved successfully.');

		return redirect(route('projects.index'));
	}

	/**
	 * Display the specified Organization.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id) {
		$organization = $this->organizationRepository->findWithoutFail($id);
		$departments = \App\Models\Department::where('organization_id', '=', $id)->get();

		if (empty($organization)) {
			Flash::error('Organization not found');

			return redirect(route('projects.index'));
		}
		// dd($departments);
		return view('organizations.show')->with('organization', $organization)->with('departments', $departments);
	}

	/**
	 * Show the form for editing the specified Organization.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id) {
		$organization = $this->organizationRepository->findWithoutFail($id);
		$users = $this->userRepository->pluck('name', 'id');

		if (empty($organization)) {
			Flash::error('Organization not found');

			return redirect(route('projects.index'));
		}

		return view('organizations.edit')->with('organization', $organization)
			->with('users', $users);
	}

	/**
	 * Update the specified Organization in storage.
	 *
	 * @param  int $id
	 * @param UpdateOrganizationRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateOrganizationRequest $request) {
		$organization = $this->organizationRepository->findWithoutFail($id);

		if (empty($organization)) {
			Flash::error('Organization not found');

			return redirect(route('projects.index'));
		}

		$organization = $this->organizationRepository->update($request->all(), $id);

		$features = ($request->input('features'));
		$organization->features = json_encode($features);
		$organization->save();

		Flash::success('Organization updated successfully.');

		return redirect(route('projects.index'));
	}

	/**
	 * Remove the specified Organization from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id) {
		$organization = $this->organizationRepository->findWithoutFail($id);

		if (empty($organization)) {
			Flash::error('Organization not found');

			return redirect(route('projects.index'));
		}

		$this->organizationRepository->delete($id);

		Flash::success('Organization deleted successfully.');

		return redirect(route('projects.index'));
	}

	public function getOrgUsers(Request $request){
		$orgId = $request->input('orgId');
		if($orgId){
			$orgUsers = \App\Models\Organization::find($orgId)->users()->get()->pluck('name', 'id');
		}
		return response()->json(['response' => $orgUsers]);

	}
	protected function allowChat(){
		//TODO::Create Chat Capability
	}
	protected function allowVoiceCalls(){
		//TODO::Create Voice Calls Capability
	}
	protected function allowVideoCalls(){
		//TODO::Create Video Calls Capability
	}
}
