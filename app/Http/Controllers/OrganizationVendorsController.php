<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateOrganizationVendorsRequest;
use App\Http\Requests\UpdateOrganizationVendorsRequest;
use App\Repositories\OrganizationVendorsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class OrganizationVendorsController extends AppBaseController {
	/** @var  OrganizationVendorsRepository */
	private $organizationVendorsRepository;

	public function __construct(OrganizationVendorsRepository $organizationVendorsRepo) {
		$this->organizationVendorsRepository = $organizationVendorsRepo;
	}

	/**
	 * Display a listing of the OrganizationVendors.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$this->organizationVendorsRepository->pushCriteria(new RequestCriteria($request));
		$organizationVendors = $this->organizationVendorsRepository->all();

		return view('organization_vendors.index')
			->with('organizationVendors', $organizationVendors);
	}

	/**
	 * Show the form for creating a new OrganizationVendors.
	 *
	 * @return Response
	 */
	public function create() {
		if(Auth::user()->hasRole(['superadmin', 'admin'])){
			$organizations = \App\Models\Organization::all()->pluck('name', 'id');
		}elseif (Auth::user()->hasRole('orgadmin')){
			$organizations = Auth::user()->organizations->pluck('name', 'id');
		}
		return view('organization_vendors.create')->with('organizations', $organizations);
	}

	/**
	 * Store a newly created OrganizationVendors in storage.
	 *
	 * @param CreateOrganizationVendorsRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateOrganizationVendorsRequest $request) {
		$input = $request->all();

		$organizationVendors = $this->organizationVendorsRepository->create($input);

		Flash::success('Organization Vendors saved successfully.');

		return redirect(route('clientdata.index'));
	}

	/**
	 * Display the specified OrganizationVendors.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id) {
		$organizationVendors = $this->organizationVendorsRepository->findWithoutFail($id);

		if (empty($organizationVendors)) {
			Flash::error('Organization Vendors not found');

			return redirect(route('clientdata.index'));
		}

		return view('organization_vendors.show')->with('organizationVendors', $organizationVendors);
	}

	/**
	 * Show the form for editing the specified OrganizationVendors.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id) {
		$organizationVendors = $this->organizationVendorsRepository->findWithoutFail($id);
		if(Auth::user()->hasRole(['superadmin', 'admin'])){
			$organizations = \App\Models\Organization::all()->pluck('name', 'id');
		}elseif (Auth::user()->hasRole('orgadmin')){
			$organizations = Auth::user()->organizations->pluck('name', 'id');
		}
		if (empty($organizationVendors)) {
			Flash::error('Organization Vendors not found');

			return redirect(route('clientdata.index'));
		}

		return view('organization_vendors.edit')->with('organizationVendors', $organizationVendors)->with('organizations',$organizations);
	}

	/**
	 * Update the specified OrganizationVendors in storage.
	 *
	 * @param  int			  $id
	 * @param UpdateOrganizationVendorsRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateOrganizationVendorsRequest $request) {
		$organizationVendors = $this->organizationVendorsRepository->findWithoutFail($id);

		if (empty($organizationVendors)) {
			Flash::error('Organization Vendors not found');

			return redirect(route('clientdata.index'));
		}

		$organizationVendors = $this->organizationVendorsRepository->update($request->all(), $id);

		Flash::success('Organization Vendors updated successfully.');

		return redirect(route('clientdata.index'));
	}

	/**
	 * Remove the specified OrganizationVendors from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id) {
		$organizationVendors = $this->organizationVendorsRepository->findWithoutFail($id);

		if (empty($organizationVendors)) {
			Flash::error('Organization Vendors not found');

			return redirect(route('clientdata.index'));
		}

		$this->organizationVendorsRepository->delete($id);

		Flash::success('Organization Vendors deleted successfully.');

		return redirect(route('clientdata.index'));
	}
}
