<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoleController extends AppBaseController {
	/** @var  RoleRepository */
	private $roleRepository;

	public function __construct(RoleRepository $roleRepo, PermissionRepository $permissionRepo) {
		$this->middleware(['verified', 'can:manage-users']);
		$this->roleRepository = $roleRepo;
		$this->permissionRepository = $permissionRepo;
	}

	/**
	 * Display a listing of the Role.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$this->roleRepository->pushCriteria(new RequestCriteria($request));
		$roles = $this->roleRepository->all();

		return view('roles.index')
			->with('roles', $roles);
	}

	/**
	 * Show the form for creating a new Role.
	 *
	 * @return Response
	 */
	public function create() {
		$permissions = $this->permissionRepository->pluck('name', 'id');
		return view('roles.create')->with('permissions', $permissions);
	}

	/**
	 * Store a newly created Role in storage.
	 *
	 * @param CreateRoleRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateRoleRequest $request) {
		$input = $request->all();

		$role = $this->roleRepository->create($input);

		Flash::success('Role saved successfully.');

		return redirect(route('roles.index'));
	}

	/**
	 * Display the specified Role.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id) {
		$role = $this->roleRepository->findWithoutFail($id);

		if (empty($role)) {
			Flash::error('Role not found');

			return redirect(route('roles.index'));
		}

		return view('roles.show')->with('role', $role);
	}

	/**
	 * Show the form for editing the specified Role.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id) {
		$role = $this->roleRepository->findWithoutFail($id);
		$permissions = $this->permissionRepository->pluck('name', 'id');

		if (empty($role)) {
			Flash::error('Role not found');
			return redirect(route('roles.index'));
		}

		return view('roles.edit')->with('role', $role)->with('permissions', $permissions);
	}

	/**
	 * Update the specified Role in storage.
	 *
	 * @param  int			  $id
	 * @param UpdateRoleRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateRoleRequest $request) {
		$role = $this->roleRepository->findWithoutFail($id);

		if (empty($role)) {
			Flash::error('Role not found');

			return redirect(route('roles.index'));
		}

		$role = $this->roleRepository->update($request->all(), $id);

		Flash::success('Role updated successfully.');

		return redirect(route('roles.index'));
	}

	/**
	 * Remove the specified Role from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy(Request $request, $id) {

		if (\App\Models\Role::find($id)->name =='superadmin' ||
				\App\Models\Role::find($id)->name =='orgadmin' || 
				\App\Models\Role::find($id)->name =='orguser') {
			
				Flash::error("Role can't be deleted");
			return redirect(route('roles.index'));
	}
		
		$role = $this->roleRepository->findWithoutFail($id);

		if (empty($role)) {
			Flash::error('Role not found');

			return redirect(route('roles.index'));
		}

		$this->roleRepository->delete($id);

		Flash::success('Role deleted successfully.');

		return redirect(route('roles.index'));
	}
}
