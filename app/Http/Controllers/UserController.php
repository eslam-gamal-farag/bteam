<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\Repositories\OrganizationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Excel;
use App\Imports\UsersImport;
use App\Exports\UsersExport;


class UserController extends AppBaseController {
	/** @var  UserRepository */
	private $userRepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(UserRepository $userRepo, RoleRepository $roleRepo, OrganizationRepository $organizationRepo) {
		$this->middleware(['verified', 'can:manage-users']);
		$this->userRepository = $userRepo;
		$this->roleRepository = $roleRepo;
		$this->organizationRepository = $organizationRepo;
	}

	/**
	 * Display a listing of the User.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$this->userRepository->pushCriteria(new RequestCriteria($request));
		$users = $this->userRepository->paginate(10);
		return view('users.index')
			->with('users', $users);
	}

	/**
	 * Show the form for creating a new User.
	 *
	 * @return Response
	 */
	public function create() {
		$superadmin = \Auth::user()->hasRole('superadmin');
		$orgadmin = \Auth::user()->hasRole('orgadmin');
		if($superadmin) {
			$roles = $this->roleRepository->pluck('name', 'id');
			$organizations = $this->organizationRepository->pluck('name', 'id');
		} else if($orgadmin) {
			$roles = $this->roleRepository->all();
			$roles = $roles->whereNotIn('name', ['superadmin', 'admin']);
			$roles = $roles->pluck('name', 'id');
			$organizations = \Auth::user()->organizations->pluck('name', 'id');
		}
		return view('users.create')->with('roles', $roles)
			->with('organizations', $organizations);
	}

	/**
	 * Store a newly created User in storage.
	 *
	 * @param CreateUserRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateUserRequest $request) {
		$input = $request->all();

		if($request->get('password', null)) {
			$input['password'] = \Hash::make($input['password']);
		}
		$user = $this->userRepository->create($input);
		if ($input['roles']) {
			$user->syncRoles($input['roles']); //Assigning role to user
			unset($input['roles']);
		}

		Flash::success('User saved successfully.');

		return redirect(route('users.index'));
	}

	/**
	 * Display the specified User.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id) {
		$user = $this->userRepository->findWithoutFail($id);

		if (empty($user)) {
			Flash::error('User not found');

			return redirect(route('users.index'));
		}

		return view('users.show')->with('user', $user);
	}

	/**
	 * Show the form for editing the specified User.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->userRepository->findWithoutFail($id);
		if(!\Auth::user()->hasRole('superadmin') && !$user->hasRole(['orguser', 'orgadmin'])) return redirect()->back();
		if(!$user->hasRole('superadmin') && $user->id === \Auth::user()->id) return redirect()->back();
		$superadmin = \Auth::user()->hasRole('superadmin');
		$orgadmin = \Auth::user()->hasRole('orgadmin');
		if($superadmin) {
			$roles = $this->roleRepository->pluck('name', 'id');
			$organizations = $this->organizationRepository->pluck('name', 'id');
		} else if($orgadmin) {
			$roles = $this->roleRepository->all();
			$roles = $roles->whereNotIn('name', ['superadmin', 'admin']);
			$roles = $roles->pluck('name', 'id');
			$organizations = \Auth::user()->organizations->pluck('name', 'id');
		}
		if (empty($user)) {
			Flash::error('User not found');

			return redirect(route('users.index'));
		}

		return view('users.edit')->with('user', $user)
			->with('roles', $roles)
			->with('organizations', $organizations);
	}

	/**
	 * Update the specified User in storage.
	 *
	 * @param  int			  $id
	 * @param UpdateUserRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateUserRequest $request) {
		$user = $this->userRepository->findWithoutFail($id);

		if (empty($user)) {
			Flash::error('User not found');
			return redirect(route('users.index'));
		}

		$inputs = $request->except('password');

		if ($request->get('roles')) {
			$user->syncRoles($inputs['roles']);
			unset($inputs['roles']);
		}

		if($request->get('password', null)) {
			$inputs = $request->all();
			$inputs['password'] = \Hash::make($inputs['password']);
		}

		$user = $this->userRepository->update($inputs, $id);

		Flash::success('User updated successfully.');

		return redirect(route('users.index'));
	}

	/**
	 * Remove the specified User from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id) {
		$user = $this->userRepository->findWithoutFail($id);
		if(!\Auth::user()->hasRole('superadmin') || $id == \Auth::user()->id) return redirect()->back();
		if (empty($user)) {
			Flash::error('User not found');

			return redirect(route('users.index'));
		}

		$this->userRepository->delete($id);

		Flash::success('User deleted successfully.');

		return redirect(route('users.index'));
	}

	public function import(){
		return view('users.import');

	}

	public function importUsers(Request $request){

		$path = $request->import_file->storeAs('User Imports', $request->import_file->getClientOriginalName());

		Excel::import(new UsersImport, $path);

		Flash::success('Employees Imported successfully.');

		return redirect(route('employees.index'));
	}

	public function export() {
		return Excel::download(new UsersExport, 'users.xlsx');
	}

	public function getDownload () {
		$file= public_path(). "/download/employeesSample.xlsx";
		$headers = array('Content-Type: application/xlsx',);
		return Response::download($file, 'employeesSample.xlsx', $headers);
	}
}
