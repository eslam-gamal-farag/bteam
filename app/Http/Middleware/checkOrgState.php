<?php namespace App\Http\Middleware;

use Closure;
use App\Repositories\OrganizationRepository;

class checkOrgState {
	/** @var  OrganizationRepository */
	private $organizationRepository;

	public function __construct(OrganizationRepository $orgRepo) {
		$this->organizationRepository = $orgRepo;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		$orgId = $request->session()->get('organization_id', null);
		$user = \Auth::user();
		if (!$orgId) {
			return redirect('/proselect');
		}
		$organization = $this->organizationRepository->find($orgId);
		view()->share('organization', $organization);
		if($user->hasRole('superadmin')) {
			return $next($request);
		}
		$organizations = $user->organizations;
		if(in_array($orgId, $organizations->pluck('id')->toArray())) {
			$orgUser = $user->organizations->where('id', $orgId)->first();
			$roleId = $orgUser->pivot->role_id;
			$role = \App\Models\Role::find($roleId);
			if($role->name == 'orgadmin') {
				return $next($request);
			}
		}
		\Flash::error('You have to be an admin');
		return redirect('/proselect');
	}
}
