<?php namespace App\Http\Requests\API;

use App\Models\OrganizationVendors;
use InfyOm\Generator\Request\APIRequest;

class UpdateOrganizationVendorsAPIRequest extends APIRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return OrganizationVendors::$rules;
	}

	public function messages () {
		return [
			'name.required' => 'Name is Required.',
			'name.unique' => 'Name Must be unique',
			'phone.required' => 'Phone is Required.',
			'email.required' => 'Email is Required.',
			'organization_id.required' => 'Organization is Required.'
		];
	}
}
