<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Organization;

class CreateOrganizationRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return Organization::$rules;
	}

	public function messages() {
    return [
			'name.required' => 'You Must Insirt Name',
			'logo.required'  => 'Organization Must have a Logo',
			'industry.required' => 'Organization Industry is Required'
    ];
	}
}
