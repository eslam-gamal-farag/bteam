<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class CreateUserRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return User::$rules;
	}

	public function messages() {
		return [
			'name.required' => 'Name is Required.',
			'email.required' => 'Email is Required.',
			'email.unique' => 'Email Must be unique',
			'password.required' => 'Password is Required.',
			'password_confirmation.required' => 'Password Confirmation is Required.',
			'roles.required' => 'Roles are Required.',
			'organization.required' => 'Organization is Required.'
		];
	}
}
