<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Calendar;

class UpdateCalendarRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return Calendar::$rules;
	}

	public function messages() {
    return [  
		'title.required' => 'Event Title is Required',
		'category.required' => 'Event Type is Required',
		'users.required' => 'Event Users Must be Selected',
		
		'*.date_format' => 'Date Format Is Invalid, Please Check, and Try Again',
		
		'from_time.required_without' => 'From Time Must Be Set',
		'to_time.required_without' => 'To Time Must Be Set',

		'to_time.after' => 'To Time, Must be After From Time',

		'shift_to_date.after' => 'To Date, Must Be After From Date',

		'shift_from_date.required_with' => 'From Date Must Be Declared In Repeating Shifts',
		'to_from_date.required_with' => 'To Date Must Be Declared In Repeating Shifts',

		'shift_from_time.required_with' => 'From Time Must Be Declared In Repeating Shifts',
		'shift_to_time.required_with' => 'To Time Must Be Declared In Repeating Shifts',
		'shift_to_time.after' => 'To Time Must Be After From Time',
    ];
	}
}
