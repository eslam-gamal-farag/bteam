<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Employee;

class UpdateEmployeeRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return Employee::$rules;
	}

	public function messages() {
		return [
			'avatar.image' => 'Avatar Must Be an Image',
			'name.required' => 'Name is Required',
			'email.required' => 'Email is Required',
			'department_id.required' => 'Department is Required',
			'job_title.required' => 'Job Title is Required',
			'cost.required' => 'Cost is Required',
			'cost.numeric' => 'Cost Must be Numeric Value',
		];
	}
}
