<?php namespace App\Imports;

use App\Models\Calendar;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon;
use Carbon\CarbonPeriod;
class CalendarsImport implements ToCollection, WithHeadingRow {

    protected $validationRules = [
		'*.title' => 'required',
		// '*.email' => 'required||email|unique:users,email',
		'*.from_date' => 'required|date_format:Y-m-d',
		'*.to_date' => 'required|date_format:Y-m-d',
		'*.from_time' => 'required|date_format:H:i',
		'*.to_time' => 'required|date_format:H:i',
		'*.days' => 'required',
	];
	
	protected $validationMessages = [
		'*.required' => 'The :attribute is required.',
		// '*.email' => 'The :attribute must be a valid email address.',
		'*.date_required' => 'The :attribute Format is Invalid'
	];
    
	public function collection(Collection $rows) {
		Validator::make(
		$rows->toArray(),
		$this->validationRules,
		$this->validationMessages
		)->validate();

		$user = \Auth::user();
		$orgId = session()->get('organization_id');

		foreach ($rows as $row) {
			$input = [
				'title' => $row['title'],
				'notes' => $row['notes'],
				'category' => 'Shift',
				'creator_id' => $user->id,
				'organization_id' => $orgId,
				'from_time' => $row['from_date'] . ' ' . $row['from_time'],
				'to_time' => $row['to_date'] . ' ' . $row['to_time'],
			];
			$period = CarbonPeriod::create($row['from_date'], $row['to_date']);
			$dates = $period->toArray();
			$days = strtolower($row['days']);
			foreach ($dates as $date) {
					if(strpos($days,strtolower($date->format('l'))) !== false) {
						$input['from_time'] = $date->format('Y-m-d') . ' ' . $row['from_time'];
						$input['to_time'] = $date->format('Y-m-d') . ' ' . $row['to_time'];

						$calendar = Calendar::create($input);
						$calendar->status = 1;
						$calendar->moderated_at = now();
						$calendar->moderated_by = $user->id;

						$emails =  explode(';', $row['emails']);
						foreach($emails as $email) {
							$calendar->users()->attach(User::where('email',$email)-> first()->id, ['status' => null]);
						}
						$calendar->save();
				}
			}
		}
	}    
}

