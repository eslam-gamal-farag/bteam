<?php namespace App\Imports;

use App\Models\User;
use App\Models\Employee;
use App\Models\Department;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToCollection, WithHeadingRow {

	protected $validationRules = [
		'*.name' => 'required',
		'*.email' => 'required|email|unique:users,email',
		'*.department' => 'required',
		'*.cost' => 'required|numeric',
		'*.avatar' => 'required'
	];
	
	protected $validationMessages = [
		'*.required' => 'The :attribute is required.',
		'*.email' => 'The :attribute must be a valid email address.',
	];

	public function collection(Collection $rows) {
		Validator::make(
			$rows->toArray(),
			$this->validationRules,
			$this->validationMessages
		)->validate();

		$orgId = session()->get('organization_id');
		$orgUser = \App\Models\Role::where('name', '=', 'orguser')->first();
		$roleId = ($orgUser)? $orgUser->id : null;

		foreach ($rows as $row) {
			$input = [
				'name' => $row['name'],
				'email' => $row['email'],
				'password' => str_random(16)
			];
			$input['organizations'] = [[
				'organization_id' => $orgId,
				'role_id' => $roleId
			]];
			$user = User::create($input);
			$user->syncRoles('orguser');
			$user->organizations()->sync($input['organizations']);
			$department = Department::firstOrCreate([
				'name' => strtolower($row['department']),
				'organization_id' => $orgId
			]);
			$input['employee'] = [
				'avatar' => $row['avatar'],
				'department_id' => $department->id,
				'job_title' => $row['job_title'],
				'cost' => $row['cost'],
				'user_id' => $user->id
			];
			$employee = Employee::create($input['employee']);
		}
	}
}