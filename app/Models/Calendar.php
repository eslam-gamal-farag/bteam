<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;
/**
 * Class Calendar
 * @package App\Models
 * @version November 11, 2018, 2:47 pm UTC
 *
  * @property \Illuminate\Database\Eloquent\Collection calendarUsers
 * @property string title
 * @property string notes
 * @property string is_private
 * @property string category
 * @property integer creator_id

 */
class Calendar extends Model {
	use SoftDeletes;

	public $table = 'calendars';
	protected $dates = ['deleted_at'];

	protected $appends = ['status_string'];

	public $fillable = [
		'title',
		'notes',
		'is_private',
		'category',
		'creator_id',
		'organization_id',
		'from_time',
		'to_time'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'title' => 'string',
		'notes' => 'string',
		'is_private' => 'string',
		'category' => 'string',
		'creator_id' => 'integer',
		'organization_id' => 'integer',
		'from_time'=>'datetime',
		'to_time'=>'datetime'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

		'title' => 'required',
		'category' => 'required',
		'users' => 'required',

		'from_time' => 'required_without:repeaterCheck|nullable|date_format:Y-m-d H:i',
		'to_time'	 => 'required_without:repeaterCheck|nullable|date_format:Y-m-d H:i|after:from_time',

		'shift_from_date' => 'required_with:repeaterCheck|nullable|date_format:Y-m-d',
		'shift_to_date' => 'required_with:repeaterCheck|nullable|date_format:Y-m-d|after:shift_from_date',

		'shift_from_time' => 'required_with:repeaterCheck|nullable|date_format:H:i',
		'shift_to_time' => 'required_with:repeaterCheck|nullable|date_format:H:i|after:shift_from_time',
		'days' => 'required_with:repeaterCheck|nullable|array|min:1'
	];

	/**
	* @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	*/

		public function setOrganizationIdAttribute($value) {
			if(is_array($value))
				$this->attributes['organization_id'] = $value[0];
				else {
					$this->attributes['organization_id'] = $value;
			}
		}
		public function users() {
			return $this->belongsToMany(\App\Models\User::class, 'calendar_users')->withPivot('status');
		}

		public function user() {
			return $this->belongsTo(\App\Models\User::class, 'creator_id', 'id');
		}

		public function setFromTimeAttribute($value){
			$value = str_replace('T', ' ', $value);
			$this->attributes['from_time'] = Carbon::createFromFormat('Y-m-d H:i',$value);
		}
		public function setToTimeAttribute($value){
			$value = str_replace('T', ' ', $value);
			$this->attributes['to_time'] = Carbon::createFromFormat('Y-m-d H:i',$value);
		}
		public function getFromTimeAttribute($fromTime){

			$dateTime = $this->attributes['from_time'];
			$fromTime = str_replace(' ', 'T', $dateTime);

			return $fromTime;
		}

		public function getToTimeAttribute(){

			$dateTime = $this->attributes['to_time'];
			$toTime = str_replace(' ', 'T', $dateTime);

			return $toTime;
		}

		public function organization(){
			return $this->belongsTo('App\Organization');
		}

		public function getStatusStringAttribute() {
			$value = isset($this->attributes['status'])? $this->attributes['status'] : '';
			switch ($value) {
				case '0':
					return 'Pending';
				case '1':
					return 'Approved';
				case '2':
					return 'Rejected';
				case '3':
					return 'Acknowledged';
				default:
					return '';
			}
		}
}
