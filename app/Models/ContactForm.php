<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ContactForm
 * @package App\Models
 * @version December 17, 2018, 8:12 pm UTC
 *
  * @property \App\Models\User user
 * @property string type
 * @property string subject
 * @property string message
 * @property integer user_id

 */
class ContactForm extends Model {
	use SoftDeletes;

	public $table = 'contact_forms';
	protected $dates = ['deleted_at'];

	public $fillable = [
		'type',
		'subject',
		'message',
		'user_id'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'type' => 'string',
		'subject' => 'string',
		'message' => 'string',
		'user_id' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'type' => 'required',
		'subject' => 'required',
		'message' => 'required'
	];

	/**
 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	*/
	public function user() {
		return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
	}
}
