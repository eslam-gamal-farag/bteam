<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Department
 * @package App\Models
 * @version November 22, 2018, 10:54 am UTC
 *
  * @property \Illuminate\Database\Eloquent\Collection calendarUsers
 * @property \Illuminate\Database\Eloquent\Collection DepartmentEmployee
 * @property \Illuminate\Database\Eloquent\Collection organizationUsers
 * @property \Illuminate\Database\Eloquent\Collection roleHasPermissions
 * @property string name
 * @property integer organization_id

 */
class Department extends Model {
	use SoftDeletes;

	public $table = 'departments';
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';
	protected $dates = ['deleted_at'];

	public $fillable = [
		'name',
		'organization_id',
		'manager_id',
		'data'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'name' => 'string',
		'organization_id' => 'integer',
		'data' => 'required'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'name' => 'required'
		
	];

	/**
 	* @return \Illuminate\Database\Eloquent\Relations\HasMany
 	*/
	public function employees() {
		return $this->hasMany(\App\Models\Employee::class);
	}
	
	/**
 	* @return \Illuminate\Database\Eloquent\Relations\BelongsTo
 	*/
	public function organization() {
		return $this->BelongsTo(\App\Models\Organization::class);
	}
	
	/**
 	* @return \Illuminate\Database\Eloquent\Relations\BelongsTo
 	*/
	public function manager() {
		return $this->BelongsTo(\App\Models\User::class, 'manager_id', 'id');
	}
}
