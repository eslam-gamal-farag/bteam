<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Employee
 * @package App\Models
 * @version November 22, 2018, 12:25 pm UTC
 *
  * @property \Illuminate\Database\Eloquent\Collection calendarUsers
 * @property \Illuminate\Database\Eloquent\Collection DepartmentEmployee
 * @property \Illuminate\Database\Eloquent\Collection organizationUsers
 * @property \Illuminate\Database\Eloquent\Collection roleHasPermissions
 * @property integer user_id
 * @property string department
 * @property float cost

 */
class Employee extends Model {
	    use SoftDeletes;

	public $table = 'employees';
	
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

	
    protected $dates = ['deleted_at'];

	
	public $fillable = [
		'user_id',
		'department_id',
		'cost',
		'avatar',
		'job_title'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'user_id' => 'integer',
		'department_id' => 'integer',
		'cost' => 'float',
		'avatar' => 'string',
		'job_title' => 'string'

	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'employee.avatar' => 'image',
		'name' => 'required',
		'email' => 'required',
		'employee.department_id' => 'required',
		'employee.job_title' => 'required',
		'employee.cost' => 'required|numeric',

	];

	public function setAvatarAttribute($file) {
		if (is_file($file)) {
			$file = \Storage::put('public/employees', $file, 'public');
		}
		$this->attributes['avatar'] = $file;
	}

	public function getAvatarAttribute($file) {
		if (\Storage::exists($this->attributes['avatar'])) {
			$url = url(\Storage::url($this->attributes['avatar']));
			return $url;
		}
		return $file;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function department() {
		return $this->belongsTo(\App\Models\Department::class);
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() {
		return $this->belongsTo(\App\Models\User::class);
	}
}
