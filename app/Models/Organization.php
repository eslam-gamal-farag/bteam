<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Organization
 * @package App\Models
 * @version October 31, 2018, 2:11 pm UTC
 *
  * @property \Illuminate\Database\Eloquent\Collection organizationUsers
 * @property string name
 * @property string logo
 * @property string allowed_domains
 * @property string industry

 */
class Organization extends Model {
	use SoftDeletes;

	public $table = 'organizations';

	protected $dates = ['deleted_at'];

	public $fillable = [
		'name',
		'logo',
		'allowed_domains',
		'industry'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'name' => 'string',
		'logo' => 'string',
		'allowed_domains' => 'string',
		'industry' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'name' => 'required|unique:organizations,name',
		'logo' => 'required|image',
		'industry' => 'required'
	];

	public function setLogoAttribute($file) {
		if (is_file($file)) {
			$file = \Storage::put('public/organizations', $file, 'public');
		}
		$this->attributes['logo'] = $file;
	}

	public function getLogoAttribute($file) {
		$url = '';
		if (\Storage::exists($this->attributes['logo'])) {
			$url = url(\Storage::url($this->attributes['logo']));
		}
		return $url;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function users() {
		return $this->belongsToMany(\App\Models\User::class, 'organization_users', 'organization_id', 'user_id')->withPivot('role_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\hasMany
	 */
	public function calendars() {
		return $this->hasMany('App\Models\Calendar', 'organization_id', 'id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\hasMany
	 */
	public function departments() {
		return $this->hasMany('App\Models\Department');
	}
}
