<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrganizationVendors
 * @package App\Models
 * @version December 26, 2018, 6:38 pm UTC
 *
  * @property string name
 * @property string email
 * @property string phone
 * @property integer organization_id

 */
class OrganizationVendors extends Model {
	    use SoftDeletes;

	public $table = 'organization_vendors';
	
	
    protected $dates = ['deleted_at'];

	
	public $fillable = [
		'name',
        'email',
        'phone',
        'organization_id'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'organization_id' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'name' => 'required|unique:organizations,name',
		'phone' => 'required',
		'email' => 'required',
        'organization_id' => 'required'
	];

	
}
