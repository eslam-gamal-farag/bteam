<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * @package App\Models
 * @version October 8, 2018, 11:12 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection roleHasPermissions
 * @property string name
 * @property string email
 * @property string|\Carbon\Carbon email_verified_at
 * @property string password
 * @property string remember_token

 */
class User extends Authenticatable implements MustVerifyEmail {
	use Notifiable;
	use HasRoles;
	use SoftDeletes;

	public $table = 'users';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	protected $dates = ['deleted_at'];


	public $fillable = [
		'name',
		'email',
		'password',
	];

	protected $hidden = [
		'password',
		'remember_token',
		'api_token',
		'email_verified_at'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'name' => 'string',
		'email' => 'string',
		'password' => 'string',
		'remember_token' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'name' => 'required',
		'email' => 'required|email|unique:users,email',
		'password' => 'required|confirmed',
		'roles' => 'required',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function organizations() {
		return $this->belongsToMany(\App\Models\Organization::class, 'organization_users', 'user_id', 'organization_id')->withPivot('role_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function events(){
		return $this->belongsToMany(\App\Models\Calendar::class,'calendar_users', 'user_id', 'calendar_id')->withPivot('status');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function employee(){
		return $this->hasOne(\App\Models\Employee::class, 'user_id', 'id');
	}

	public function routeNotificationForOneSignal() {
		if(\Auth::user()->id == $this->id) return null;
		// return '1806e00f-579b-46f5-a140-ae8422205ef7';
		return $this->device_token;
	}
}
