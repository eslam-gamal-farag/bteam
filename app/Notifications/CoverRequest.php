<?php namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;

class CoverRequest extends Notification {
	// use Queueable;
	private $data;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($request) {
			$this->data = $request;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		// return ['mail'];
		return [OneSignalChannel::class];

	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	// public function toMail($notifiable)
	// {
	//	 return (new MailMessage)
	//				 ->line('There is a request for a shift cover.')
	//				 ->action('Shift: ', url('/'))
	//				 ->line('Thank you for using our application!');
	// }

	public function toOneSignal($notifiable) {
		$user = $this->data->user()->name;
		return OneSignalMessage::create()
			->subject("Request by {$user}")
						->body("Cover Request")
						->button(
							OneSignalButton::create('open')
								->text('Open')
			);
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
