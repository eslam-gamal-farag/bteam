<?php namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;

class MessageSent extends Notification {
	// use Queueable;
	private $data;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($request) {
		$this->data = $request;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return [OneSignalChannel::class];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		return (new MailMessage)
					->line('The introduction to the notification.')
					->action('Notification Action', url('/'))
					->line('Thank you for using our application!');
		}

	public function toOneSignal($notifiable) {
		$user = $this->data->sender;
		$message = $this->data->body;
		$subject = "{$user->name} sent a message";
		if($this->data->type == 'call') {
			$subject = $user->name;
			$message = 'Calling';
		}
		return OneSignalMessage::create()
			->setData('conversation_id', $this->data->conversation_id)
			->setData('conversation_users', $this->data->conversation->users)
			->setData('message_type', $this->data->type)
			->subject($subject)
						->body($message)
						->button(
							OneSignalButton::create('conversation')
								->text('Open')
			);
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
