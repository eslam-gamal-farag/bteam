<?php namespace App\Repositories;

use App\Models\Calendar;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CalendarRepository
 * @package App\Repositories
 * @version November 11, 2018, 2:47 pm UTC
 *
 * @method Calendar findWithoutFail($id, $columns = ['*'])
 * @method Calendar find($id, $columns = ['*'])
 * @method Calendar first($columns = ['*'])
 */
class CalendarRepository extends BaseRepository {
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'title',
		'is_private',
		'category',
		'creator_id',
		'from_time',
		'to_time'
	];

	/**
	 * Configure the Model
	 **/
	public function model() {
		return Calendar::class;
	}
}
