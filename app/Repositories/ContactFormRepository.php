<?php namespace App\Repositories;

use App\Models\ContactForm;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContactFormRepository
 * @package App\Repositories
 * @version December 17, 2018, 8:12 pm UTC
 *
 * @method ContactForm findWithoutFail($id, $columns = ['*'])
 * @method ContactForm find($id, $columns = ['*'])
 * @method ContactForm first($columns = ['*'])
 */
class ContactFormRepository extends BaseRepository {
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'type',
        'subject',
        'message'
	];

	/**
	 * Configure the Model
	 **/
	public function model() {
		return ContactForm::class;
	}
}
