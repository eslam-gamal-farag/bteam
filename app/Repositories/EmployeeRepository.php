<?php namespace App\Repositories;

use App\Models\Employee;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EmployeeRepository
 * @package App\Repositories
 * @version November 22, 2018, 12:25 pm UTC
 *
 * @method Employee findWithoutFail($id, $columns = ['*'])
 * @method Employee find($id, $columns = ['*'])
 * @method Employee first($columns = ['*'])
 */
class EmployeeRepository extends BaseRepository {
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'user_id',
		'department_id',
		'cost'
	];

	/**
	 * Configure the Model
	 **/
	public function model() {
		return Employee::class;
	}
}
