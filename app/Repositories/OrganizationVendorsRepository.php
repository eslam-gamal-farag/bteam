<?php namespace App\Repositories;

use App\Models\OrganizationVendors;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OrganizationVendorsRepository
 * @package App\Repositories
 * @version December 26, 2018, 6:38 pm UTC
 *
 * @method OrganizationVendors findWithoutFail($id, $columns = ['*'])
 * @method OrganizationVendors find($id, $columns = ['*'])
 * @method OrganizationVendors first($columns = ['*'])
 */
class OrganizationVendorsRepository extends BaseRepository {
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'name',
        'phone',
        'organization_id'
	];

	/**
	 * Configure the Model
	 **/
	public function model() {
		return OrganizationVendors::class;
	}
}
