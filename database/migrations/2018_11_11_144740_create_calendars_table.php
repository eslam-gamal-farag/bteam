<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendarsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('calendars', function (Blueprint $table) {
			$table->increments('id');
            $table->string('title');
            $table->text('notes');
            $table->string('is_private');
            $table->string('category');
						$table->integer('creator_id');
						$table->integer('organization_id');
						$table->dateTime('from_time')->nullable();
						$table->dateTime('to_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('calendars');
	}
}
