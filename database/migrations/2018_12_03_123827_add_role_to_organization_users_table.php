<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleToOrganizationUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('organization_users', function (Blueprint $table) {
			$table->integer('role_id')->unsigned()->nullable();

			$table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('organization_users', function (Blueprint $table) {
			$table->dropForeign('organization_users_role_id_foreign');
			$table->dropColumn('role_id');
		});
	}
}
