<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestFieldsToCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendars', function (Blueprint $table) {
            $table->smallInteger('status')->nullable()->default(0); //Pending - Approved - Rejected
            $table->dateTime('moderated_at')->nullable();
            $table->integer('moderated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendars', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('moderated_at');
            $table->dropColumn('moderated_by');
        });
    }
}
