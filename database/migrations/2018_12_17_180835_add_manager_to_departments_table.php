<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManagerToDepartmentsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('departments', function (Blueprint $table) {
			$table->integer('manager_id')->unsigned()->nullable()->after('organization_id');
			$table->foreign('manager_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('departments', function (Blueprint $table) {
			$table->dropForeign('departments_manager_id_foreign');
			$table->dropColumn('manager_id');
		});
	}
}
