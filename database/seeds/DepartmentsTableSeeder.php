<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 20; $i++) {
            App\Models\Department::create([
                'name' => $faker->name,
                'organization_id' => $faker->biasedNumberBetween($min = 10, $max = 20, $function = 'sqrt')
            ]);
        }
    }
}
