<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 200; $i++) {
            App\Models\Employee::create([
                'user_id' => $i+1,
                'department_id' => rand(1,20),
                'cost' => rand(100,5000),
                'job_title'=>$faker->jobTitle,
                'avatar' => $faker->slug
            ]);
        }
    }
}
