<?php

use Illuminate\Database\Seeder;

class OrganizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 20; $i++) {
            App\Models\Organization::create([
                'name' => $faker->name,
                'logo' => $faker->slug,
                'allowed_domains' => $faker->domainName,
                'industry' => $faker->company
            ]);
        }
    }
}
