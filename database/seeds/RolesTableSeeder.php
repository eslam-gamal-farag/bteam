<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'superadmin',
            'guard_name' => 'web',
        ]);
        DB::table('roles')->insert([
            'name' => 'projectadmin',
            'guard_name' => 'web',
        ]);
        DB::table('roles')->insert([
            'name' => 'projectuser',
            'guard_name' => 'web',
        ]);
    }
}
