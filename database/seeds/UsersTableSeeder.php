<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'info@momentum-sol.com',
            'password' => bcrypt('password'),
        ]);

        // $faker = Faker\Factory::create();

        // for($i = 0; $i < 200; $i++) {
        //     App\Models\User::create([
        //         'password' => bcrypt('password'),
        //         'name' => $faker->name,
        //         'email' => $faker->email
        //     ]);
        // }
    }
}
