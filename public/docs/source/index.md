---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_c6c5c00d6ac7f771f157dff4a2889b1a -->
## _debugbar/open
> Example request:

```bash
curl -X GET -G "http://localhost/_debugbar/open" 
```

```javascript
const url = new URL("http://localhost/_debugbar/open");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (404):

```json
{
    "message": "",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 940,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/DebugbarEnabled.php",
            "line": 36,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\DebugbarEnabled",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET _debugbar/open`


<!-- END_c6c5c00d6ac7f771f157dff4a2889b1a -->

<!-- START_7b167949c615f4a7e7b673f8d5fdaf59 -->
## Return Clockwork output

> Example request:

```bash
curl -X GET -G "http://localhost/_debugbar/clockwork/{id}" 
```

```javascript
const url = new URL("http://localhost/_debugbar/clockwork/{id}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (404):

```json
{
    "message": "",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 940,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/DebugbarEnabled.php",
            "line": 36,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\DebugbarEnabled",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET _debugbar/clockwork/{id}`


<!-- END_7b167949c615f4a7e7b673f8d5fdaf59 -->

<!-- START_5f8a640000f5db43332951f0d77378c4 -->
## Return the stylesheets for the Debugbar

> Example request:

```bash
curl -X GET -G "http://localhost/_debugbar/assets/stylesheets" 
```

```javascript
const url = new URL("http://localhost/_debugbar/assets/stylesheets");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (404):

```json
{
    "message": "",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 940,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/DebugbarEnabled.php",
            "line": 36,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\DebugbarEnabled",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET _debugbar/assets/stylesheets`


<!-- END_5f8a640000f5db43332951f0d77378c4 -->

<!-- START_db7a887cf930ce3c638a8708fd1a75ee -->
## Return the javascript for the Debugbar

> Example request:

```bash
curl -X GET -G "http://localhost/_debugbar/assets/javascript" 
```

```javascript
const url = new URL("http://localhost/_debugbar/assets/javascript");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (404):

```json
{
    "message": "",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 940,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/DebugbarEnabled.php",
            "line": 36,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\DebugbarEnabled",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET _debugbar/assets/javascript`


<!-- END_db7a887cf930ce3c638a8708fd1a75ee -->

<!-- START_0973671c4f56e7409202dc85c868d442 -->
## Forget a cache key

> Example request:

```bash
curl -X DELETE "http://localhost/_debugbar/cache/{key}/{tags?}" 
```

```javascript
const url = new URL("http://localhost/_debugbar/cache/{key}/{tags?}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE _debugbar/cache/{key}/{tags?}`


<!-- END_0973671c4f56e7409202dc85c868d442 -->

<!-- START_caaeb290ada2ed91cfc05495f2d67fac -->
## Show the application&#039;s login form.

> Example request:

```bash
curl -X GET -G "http://localhost/api/login" 
```

```javascript
const url = new URL("http://localhost/api/login");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (500):

```json
{
    "message": "Undefined variable: errors (View: \/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/resources\/views\/auth\/login.blade.php)",
    "exception": "ErrorException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/storage\/framework\/views\/5d16caed6cdec4f03c39affbe87733b50b32aa77.php",
    "line": 53,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Engines\/PhpEngine.php",
            "line": 45,
            "function": "handleViewException",
            "class": "Illuminate\\View\\Engines\\CompilerEngine",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Engines\/CompilerEngine.php",
            "line": 59,
            "function": "evaluatePath",
            "class": "Illuminate\\View\\Engines\\PhpEngine",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/View.php",
            "line": 142,
            "function": "get",
            "class": "Illuminate\\View\\Engines\\CompilerEngine",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/View.php",
            "line": 125,
            "function": "getContents",
            "class": "Illuminate\\View\\View",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/View.php",
            "line": 90,
            "function": "renderContents",
            "class": "Illuminate\\View\\View",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Http\/Response.php",
            "line": 42,
            "function": "render",
            "class": "Illuminate\\View\\View",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/http-foundation\/Response.php",
            "line": 202,
            "function": "setContent",
            "class": "Illuminate\\Http\\Response",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 750,
            "function": "__construct",
            "class": "Symfony\\Component\\HttpFoundation\\Response",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 722,
            "function": "toResponse",
            "class": "Illuminate\\Routing\\Router",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 682,
            "function": "prepareResponse",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/app\/Http\/Middleware\/RedirectIfAuthenticated.php",
            "line": 24,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "App\\Http\\Middleware\\RedirectIfAuthenticated",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET api/login`


<!-- END_caaeb290ada2ed91cfc05495f2d67fac -->

<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## api/login
> Example request:

```bash
curl -X POST "http://localhost/api/login" 
```

```javascript
const url = new URL("http://localhost/api/login");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/login`


<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_61739f3220a224b34228600649230ad1 -->
## api/logout
> Example request:

```bash
curl -X POST "http://localhost/api/logout" 
```

```javascript
const url = new URL("http://localhost/api/logout");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/logout`


<!-- END_61739f3220a224b34228600649230ad1 -->

<!-- START_2ed165150fc1d9bdc90f8edbc8a40fe0 -->
## Show the application registration form.

> Example request:

```bash
curl -X GET -G "http://localhost/api/register" 
```

```javascript
const url = new URL("http://localhost/api/register");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (500):

```json
{
    "message": "Undefined variable: errors (View: \/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/resources\/views\/auth\/register.blade.php)",
    "exception": "ErrorException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/storage\/framework\/views\/8f1222f80d79854cf6fe6b4cfdb527b463deeabb.php",
    "line": 56,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Engines\/PhpEngine.php",
            "line": 45,
            "function": "handleViewException",
            "class": "Illuminate\\View\\Engines\\CompilerEngine",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Engines\/CompilerEngine.php",
            "line": 59,
            "function": "evaluatePath",
            "class": "Illuminate\\View\\Engines\\PhpEngine",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/View.php",
            "line": 142,
            "function": "get",
            "class": "Illuminate\\View\\Engines\\CompilerEngine",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/View.php",
            "line": 125,
            "function": "getContents",
            "class": "Illuminate\\View\\View",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/View.php",
            "line": 90,
            "function": "renderContents",
            "class": "Illuminate\\View\\View",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Http\/Response.php",
            "line": 42,
            "function": "render",
            "class": "Illuminate\\View\\View",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/http-foundation\/Response.php",
            "line": 202,
            "function": "setContent",
            "class": "Illuminate\\Http\\Response",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 750,
            "function": "__construct",
            "class": "Symfony\\Component\\HttpFoundation\\Response",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 722,
            "function": "toResponse",
            "class": "Illuminate\\Routing\\Router",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 682,
            "function": "prepareResponse",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/app\/Http\/Middleware\/RedirectIfAuthenticated.php",
            "line": 24,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "App\\Http\\Middleware\\RedirectIfAuthenticated",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET api/register`


<!-- END_2ed165150fc1d9bdc90f8edbc8a40fe0 -->

<!-- START_d7b7952e7fdddc07c978c9bdaf757acf -->
## Override default register method from RegistersUsers trait

> Example request:

```bash
curl -X POST "http://localhost/api/register" 
```

```javascript
const url = new URL("http://localhost/api/register");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/register`


<!-- END_d7b7952e7fdddc07c978c9bdaf757acf -->

<!-- START_2d698b6d6bc7441f9c1a9cf11aec4059 -->
## Show the email verification notice.

> Example request:

```bash
curl -X GET -G "http://localhost/api/email/verify" 
```

```javascript
const url = new URL("http://localhost/api/email/verify");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/email/verify`


<!-- END_2d698b6d6bc7441f9c1a9cf11aec4059 -->

<!-- START_d83e982c7c8172810ed08568400567aa -->
## Mark the authenticated user&#039;s email address as verified.

> Example request:

```bash
curl -X GET -G "http://localhost/api/email/verify/{id}" 
```

```javascript
const url = new URL("http://localhost/api/email/verify/{id}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/email/verify/{id}`


<!-- END_d83e982c7c8172810ed08568400567aa -->

<!-- START_31f430322462abe3fc3e4ba369b8f77d -->
## api/email/resend
> Example request:

```bash
curl -X GET -G "http://localhost/api/email/resend" 
```

```javascript
const url = new URL("http://localhost/api/email/resend");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/email/resend`


<!-- END_31f430322462abe3fc3e4ba369b8f77d -->

<!-- START_222b40d710868bb5dfdda0f4c99eacac -->
## Display a listing of the Calendar.

GET|HEAD /calendars

> Example request:

```bash
curl -X GET -G "http://localhost/api/calendars" 
```

```javascript
const url = new URL("http://localhost/api/calendars");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/calendars`


<!-- END_222b40d710868bb5dfdda0f4c99eacac -->

<!-- START_f6ddbc3235eeec4eebe4052fe7277b08 -->
## Store a newly created Calendar in storage.

POST /calendars

> Example request:

```bash
curl -X POST "http://localhost/api/calendars" 
```

```javascript
const url = new URL("http://localhost/api/calendars");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/calendars`


<!-- END_f6ddbc3235eeec4eebe4052fe7277b08 -->

<!-- START_7dd916f3e02f2cf4a2425a098c8bfc3b -->
## Display the specified Calendar.

GET|HEAD /calendars/{id}

> Example request:

```bash
curl -X GET -G "http://localhost/api/calendars/{calendar}" 
```

```javascript
const url = new URL("http://localhost/api/calendars/{calendar}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/calendars/{calendar}`


<!-- END_7dd916f3e02f2cf4a2425a098c8bfc3b -->

<!-- START_b5d6103dcb42d8a4c0a5a15b67f293f6 -->
## Update the specified Calendar in storage.

PUT/PATCH /calendars/{id}

> Example request:

```bash
curl -X PUT "http://localhost/api/calendars/{calendar}" 
```

```javascript
const url = new URL("http://localhost/api/calendars/{calendar}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/calendars/{calendar}`

`PATCH api/calendars/{calendar}`


<!-- END_b5d6103dcb42d8a4c0a5a15b67f293f6 -->

<!-- START_639c53b94cce2449fe29dc8439c0d70e -->
## Remove the specified Calendar from storage.

DELETE /calendars/{id}

> Example request:

```bash
curl -X DELETE "http://localhost/api/calendars/{calendar}" 
```

```javascript
const url = new URL("http://localhost/api/calendars/{calendar}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/calendars/{calendar}`


<!-- END_639c53b94cce2449fe29dc8439c0d70e -->

<!-- START_fc1e4f6a697e3c48257de845299b71d5 -->
## Display a listing of the User.

GET|HEAD /users

> Example request:

```bash
curl -X GET -G "http://localhost/api/users" 
```

```javascript
const url = new URL("http://localhost/api/users");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/users`


<!-- END_fc1e4f6a697e3c48257de845299b71d5 -->

<!-- START_12e37982cc5398c7100e59625ebb5514 -->
## Store a newly created User in storage.

POST /users

> Example request:

```bash
curl -X POST "http://localhost/api/users" 
```

```javascript
const url = new URL("http://localhost/api/users");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/users`


<!-- END_12e37982cc5398c7100e59625ebb5514 -->

<!-- START_8653614346cb0e3d444d164579a0a0a2 -->
## Display the specified User.

GET|HEAD /users/{id}

> Example request:

```bash
curl -X GET -G "http://localhost/api/users/{user}" 
```

```javascript
const url = new URL("http://localhost/api/users/{user}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/users/{user}`


<!-- END_8653614346cb0e3d444d164579a0a0a2 -->

<!-- START_48a3115be98493a3c866eb0e23347262 -->
## Update the specified User in storage.

PUT/PATCH /users/{id}

> Example request:

```bash
curl -X PUT "http://localhost/api/users/{user}" 
```

```javascript
const url = new URL("http://localhost/api/users/{user}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/users/{user}`

`PATCH api/users/{user}`


<!-- END_48a3115be98493a3c866eb0e23347262 -->

<!-- START_d2db7a9fe3abd141d5adbc367a88e969 -->
## Remove the specified User from storage.

DELETE /users/{id}

> Example request:

```bash
curl -X DELETE "http://localhost/api/users/{user}" 
```

```javascript
const url = new URL("http://localhost/api/users/{user}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/users/{user}`


<!-- END_d2db7a9fe3abd141d5adbc367a88e969 -->

<!-- START_42db014707f615cd5cafb5ad1b6d0675 -->
## Display a listing of the Permission.

GET|HEAD /permissions

> Example request:

```bash
curl -X GET -G "http://localhost/api/permissions" 
```

```javascript
const url = new URL("http://localhost/api/permissions");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/permissions`


<!-- END_42db014707f615cd5cafb5ad1b6d0675 -->

<!-- START_d513e82f79d47649a14d2e59fda93073 -->
## Store a newly created Permission in storage.

POST /permissions

> Example request:

```bash
curl -X POST "http://localhost/api/permissions" 
```

```javascript
const url = new URL("http://localhost/api/permissions");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/permissions`


<!-- END_d513e82f79d47649a14d2e59fda93073 -->

<!-- START_29ec1a9c6f20445dcd75bf6a4cc63e2a -->
## Display the specified Permission.

GET|HEAD /permissions/{id}

> Example request:

```bash
curl -X GET -G "http://localhost/api/permissions/{permission}" 
```

```javascript
const url = new URL("http://localhost/api/permissions/{permission}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/permissions/{permission}`


<!-- END_29ec1a9c6f20445dcd75bf6a4cc63e2a -->

<!-- START_cbdd1fce06181b5d5d8d0f3ae85ed0ee -->
## Update the specified Permission in storage.

PUT/PATCH /permissions/{id}

> Example request:

```bash
curl -X PUT "http://localhost/api/permissions/{permission}" 
```

```javascript
const url = new URL("http://localhost/api/permissions/{permission}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/permissions/{permission}`

`PATCH api/permissions/{permission}`


<!-- END_cbdd1fce06181b5d5d8d0f3ae85ed0ee -->

<!-- START_58309983000c47ce901812498144165a -->
## Remove the specified Permission from storage.

DELETE /permissions/{id}

> Example request:

```bash
curl -X DELETE "http://localhost/api/permissions/{permission}" 
```

```javascript
const url = new URL("http://localhost/api/permissions/{permission}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/permissions/{permission}`


<!-- END_58309983000c47ce901812498144165a -->

<!-- START_6470e6b987921f5c45bf7a2d8e674f57 -->
## Display a listing of the Role.

GET|HEAD /roles

> Example request:

```bash
curl -X GET -G "http://localhost/api/roles" 
```

```javascript
const url = new URL("http://localhost/api/roles");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/roles`


<!-- END_6470e6b987921f5c45bf7a2d8e674f57 -->

<!-- START_90c780acaefab9740431579512d07101 -->
## Store a newly created Role in storage.

POST /roles

> Example request:

```bash
curl -X POST "http://localhost/api/roles" 
```

```javascript
const url = new URL("http://localhost/api/roles");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/roles`


<!-- END_90c780acaefab9740431579512d07101 -->

<!-- START_eb37fe1fa9305b4b78850dd87031670b -->
## Display the specified Role.

GET|HEAD /roles/{id}

> Example request:

```bash
curl -X GET -G "http://localhost/api/roles/{role}" 
```

```javascript
const url = new URL("http://localhost/api/roles/{role}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/roles/{role}`


<!-- END_eb37fe1fa9305b4b78850dd87031670b -->

<!-- START_cccebfff0074c9c5f499e215eee84e86 -->
## Update the specified Role in storage.

PUT/PATCH /roles/{id}

> Example request:

```bash
curl -X PUT "http://localhost/api/roles/{role}" 
```

```javascript
const url = new URL("http://localhost/api/roles/{role}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/roles/{role}`

`PATCH api/roles/{role}`


<!-- END_cccebfff0074c9c5f499e215eee84e86 -->

<!-- START_9aab750214722ffceebef64f24a2e175 -->
## Remove the specified Role from storage.

DELETE /roles/{id}

> Example request:

```bash
curl -X DELETE "http://localhost/api/roles/{role}" 
```

```javascript
const url = new URL("http://localhost/api/roles/{role}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/roles/{role}`


<!-- END_9aab750214722ffceebef64f24a2e175 -->

<!-- START_434c81a9abb0283f205ef7cb7378688e -->
## Display a listing of the Organization.

GET|HEAD /organizations

> Example request:

```bash
curl -X GET -G "http://localhost/api/organizations" 
```

```javascript
const url = new URL("http://localhost/api/organizations");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/organizations`


<!-- END_434c81a9abb0283f205ef7cb7378688e -->

<!-- START_a3d4660c575d6fd59c9718ff69a12cc7 -->
## Store a newly created Organization in storage.

POST /organizations

> Example request:

```bash
curl -X POST "http://localhost/api/organizations" 
```

```javascript
const url = new URL("http://localhost/api/organizations");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/organizations`


<!-- END_a3d4660c575d6fd59c9718ff69a12cc7 -->

<!-- START_7d1f86cd2d17ff6e8f7bce97aeabc7f3 -->
## Display the specified Organization.

GET|HEAD /organizations/{id}

> Example request:

```bash
curl -X GET -G "http://localhost/api/organizations/{organization}" 
```

```javascript
const url = new URL("http://localhost/api/organizations/{organization}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/organizations/{organization}`


<!-- END_7d1f86cd2d17ff6e8f7bce97aeabc7f3 -->

<!-- START_e6c758d4cc6e3b90231537145573cfd8 -->
## Update the specified Organization in storage.

PUT/PATCH /organizations/{id}

> Example request:

```bash
curl -X PUT "http://localhost/api/organizations/{organization}" 
```

```javascript
const url = new URL("http://localhost/api/organizations/{organization}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/organizations/{organization}`

`PATCH api/organizations/{organization}`


<!-- END_e6c758d4cc6e3b90231537145573cfd8 -->

<!-- START_b9047b90f047db47c77810fd8de29af9 -->
## Remove the specified Organization from storage.

DELETE /organizations/{id}

> Example request:

```bash
curl -X DELETE "http://localhost/api/organizations/{organization}" 
```

```javascript
const url = new URL("http://localhost/api/organizations/{organization}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/organizations/{organization}`


<!-- END_b9047b90f047db47c77810fd8de29af9 -->

<!-- START_b34ff4c850834f00b1be8da46fa5d7e8 -->
## Display a listing of the ContactForm.

GET|HEAD /contactForms

> Example request:

```bash
curl -X GET -G "http://localhost/api/contact_forms" 
```

```javascript
const url = new URL("http://localhost/api/contact_forms");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/contact_forms`


<!-- END_b34ff4c850834f00b1be8da46fa5d7e8 -->

<!-- START_11f3c90a4d5250497c6811bbef9c9b35 -->
## Store a newly created ContactForm in storage.

POST /contactForms

> Example request:

```bash
curl -X POST "http://localhost/api/contact_forms" 
```

```javascript
const url = new URL("http://localhost/api/contact_forms");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/contact_forms`


<!-- END_11f3c90a4d5250497c6811bbef9c9b35 -->

<!-- START_845a7b2130e7126efecd354bff4c646b -->
## Display the specified ContactForm.

GET|HEAD /contactForms/{id}

> Example request:

```bash
curl -X GET -G "http://localhost/api/contact_forms/{contact_form}" 
```

```javascript
const url = new URL("http://localhost/api/contact_forms/{contact_form}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/contact_forms/{contact_form}`


<!-- END_845a7b2130e7126efecd354bff4c646b -->

<!-- START_3648a0aad91f1c463b60395a37e8a0de -->
## Update the specified ContactForm in storage.

PUT/PATCH /contactForms/{id}

> Example request:

```bash
curl -X PUT "http://localhost/api/contact_forms/{contact_form}" 
```

```javascript
const url = new URL("http://localhost/api/contact_forms/{contact_form}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/contact_forms/{contact_form}`

`PATCH api/contact_forms/{contact_form}`


<!-- END_3648a0aad91f1c463b60395a37e8a0de -->

<!-- START_c1458f9064d4691a979f572775c31a94 -->
## Remove the specified ContactForm from storage.

DELETE /contactForms/{id}

> Example request:

```bash
curl -X DELETE "http://localhost/api/contact_forms/{contact_form}" 
```

```javascript
const url = new URL("http://localhost/api/contact_forms/{contact_form}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/contact_forms/{contact_form}`


<!-- END_c1458f9064d4691a979f572775c31a94 -->

<!-- START_f5e8a697ca98df10d199250db7ec4673 -->
## Display a listing of the OrganizationVendors.

GET|HEAD /organizationVendors

> Example request:

```bash
curl -X GET -G "http://localhost/api/organization_vendors" 
```

```javascript
const url = new URL("http://localhost/api/organization_vendors");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (500):

```json
{
    "message": "SQLSTATE[HY000] [2002] Connection refused (SQL: select * from `organization_vendors` where `organization_vendors`.`deleted_at` is null)",
    "exception": "Illuminate\\Database\\QueryException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
    "line": 664,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
            "line": 624,
            "function": "runQueryCallback",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
            "line": 333,
            "function": "run",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2095,
            "function": "select",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2083,
            "function": "runSelect",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2569,
            "function": "Illuminate\\Database\\Query\\{closure}",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2084,
            "function": "onceWithColumns",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Eloquent\/Builder.php",
            "line": 516,
            "function": "get",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Eloquent\/Builder.php",
            "line": 500,
            "function": "getModels",
            "class": "Illuminate\\Database\\Eloquent\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Eloquent\/Model.php",
            "line": 461,
            "function": "get",
            "class": "Illuminate\\Database\\Eloquent\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/prettus\/l5-repository\/src\/Prettus\/Repository\/Eloquent\/BaseRepository.php",
            "line": 328,
            "function": "all",
            "class": "Illuminate\\Database\\Eloquent\\Model",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/app\/Http\/Controllers\/API\/OrganizationVendorsAPIController.php",
            "line": 36,
            "function": "all",
            "class": "Prettus\\Repository\\Eloquent\\BaseRepository",
            "type": "->"
        },
        {
            "function": "index",
            "class": "App\\Http\\Controllers\\API\\OrganizationVendorsAPIController",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Controller.php",
            "line": 54,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/ControllerDispatcher.php",
            "line": 45,
            "function": "callAction",
            "class": "Illuminate\\Routing\\Controller",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 219,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\ControllerDispatcher",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 176,
            "function": "runController",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 682,
            "function": "run",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET api/organization_vendors`


<!-- END_f5e8a697ca98df10d199250db7ec4673 -->

<!-- START_ea725c34e9816b6c18e57a4f87c5dd2a -->
## Store a newly created OrganizationVendors in storage.

POST /organizationVendors

> Example request:

```bash
curl -X POST "http://localhost/api/organization_vendors" 
```

```javascript
const url = new URL("http://localhost/api/organization_vendors");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/organization_vendors`


<!-- END_ea725c34e9816b6c18e57a4f87c5dd2a -->

<!-- START_34d36374c7d68d941071302480f9ea41 -->
## Display the specified OrganizationVendors.

GET|HEAD /organizationVendors/{id}

> Example request:

```bash
curl -X GET -G "http://localhost/api/organization_vendors/{organization_vendor}" 
```

```javascript
const url = new URL("http://localhost/api/organization_vendors/{organization_vendor}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (404):

```json
{
    "success": false,
    "message": "Organization Vendors not found"
}
```

### HTTP Request
`GET api/organization_vendors/{organization_vendor}`


<!-- END_34d36374c7d68d941071302480f9ea41 -->

<!-- START_49dcb27d1240c44f345c335eb6d198da -->
## Update the specified OrganizationVendors in storage.

PUT/PATCH /organizationVendors/{id}

> Example request:

```bash
curl -X PUT "http://localhost/api/organization_vendors/{organization_vendor}" 
```

```javascript
const url = new URL("http://localhost/api/organization_vendors/{organization_vendor}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/organization_vendors/{organization_vendor}`

`PATCH api/organization_vendors/{organization_vendor}`


<!-- END_49dcb27d1240c44f345c335eb6d198da -->

<!-- START_dc37c3be56889caacd931c079bbbc722 -->
## Remove the specified OrganizationVendors from storage.

DELETE /organizationVendors/{id}

> Example request:

```bash
curl -X DELETE "http://localhost/api/organization_vendors/{organization_vendor}" 
```

```javascript
const url = new URL("http://localhost/api/organization_vendors/{organization_vendor}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/organization_vendors/{organization_vendor}`


<!-- END_dc37c3be56889caacd931c079bbbc722 -->

<!-- START_bbf5f53764a43af9eab11504db0c39b5 -->
## Get a listing of the Employees.

GET|HEAD /employees

> Example request:

```bash
curl -X GET -G "http://localhost/api/employees" 
```

```javascript
const url = new URL("http://localhost/api/employees");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/employees`


<!-- END_bbf5f53764a43af9eab11504db0c39b5 -->

<!-- START_2b6e5a4b188cb183c7e59558cce36cb6 -->
## api/user
> Example request:

```bash
curl -X GET -G "http://localhost/api/user" 
```

```javascript
const url = new URL("http://localhost/api/user");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/user`


<!-- END_2b6e5a4b188cb183c7e59558cce36cb6 -->

<!-- START_cda150622f594ef884360228effaaaa2 -->
## Register a device token for PN

> Example request:

```bash
curl -X POST "http://localhost/api/register_device" 
```

```javascript
const url = new URL("http://localhost/api/register_device");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/register_device`


<!-- END_cda150622f594ef884360228effaaaa2 -->

<!-- START_50f5969ffa4376ab4d09a74616534468 -->
## api/conversations
> Example request:

```bash
curl -X GET -G "http://localhost/api/conversations" 
```

```javascript
const url = new URL("http://localhost/api/conversations");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/conversations`


<!-- END_50f5969ffa4376ab4d09a74616534468 -->

<!-- START_1d6ac3c69bc5f2271b33806815418dc6 -->
## api/conversations
> Example request:

```bash
curl -X POST "http://localhost/api/conversations" 
```

```javascript
const url = new URL("http://localhost/api/conversations");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/conversations`


<!-- END_1d6ac3c69bc5f2271b33806815418dc6 -->

<!-- START_307f6253da9f4a9333e71e08a4c15acf -->
## api/conversations/{id}
> Example request:

```bash
curl -X GET -G "http://localhost/api/conversations/{id}" 
```

```javascript
const url = new URL("http://localhost/api/conversations/{id}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/conversations/{id}`


<!-- END_307f6253da9f4a9333e71e08a4c15acf -->

<!-- START_515b241d5cd54dc3ac69d5acda42af89 -->
## api/conversations/{id}/send
> Example request:

```bash
curl -X POST "http://localhost/api/conversations/{id}/send" 
```

```javascript
const url = new URL("http://localhost/api/conversations/{id}/send");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/conversations/{id}/send`


<!-- END_515b241d5cd54dc3ac69d5acda42af89 -->

<!-- START_129040231a10c71c8618090a5c148134 -->
## api/getMessage
> Example request:

```bash
curl -X GET -G "http://localhost/api/getMessage" 
```

```javascript
const url = new URL("http://localhost/api/getMessage");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/getMessage`


<!-- END_129040231a10c71c8618090a5c148134 -->

<!-- START_ce2b7398541542f559944b74de78943a -->
## api/unreadMessagesCount
> Example request:

```bash
curl -X GET -G "http://localhost/api/unreadMessagesCount" 
```

```javascript
const url = new URL("http://localhost/api/unreadMessagesCount");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/unreadMessagesCount`


<!-- END_ce2b7398541542f559944b74de78943a -->

<!-- START_49d829f1ab520c98beb2be5697975ee8 -->
## api/unreadMessagesCountPerConv
> Example request:

```bash
curl -X GET -G "http://localhost/api/unreadMessagesCountPerConv" 
```

```javascript
const url = new URL("http://localhost/api/unreadMessagesCountPerConv");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/unreadMessagesCountPerConv`


<!-- END_49d829f1ab520c98beb2be5697975ee8 -->

<!-- START_d2293a94f30ac54ea9fb5169e8d352f1 -->
## api/getConversationBetweenTwoUsers
> Example request:

```bash
curl -X GET -G "http://localhost/api/getConversationBetweenTwoUsers" 
```

```javascript
const url = new URL("http://localhost/api/getConversationBetweenTwoUsers");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/getConversationBetweenTwoUsers`


<!-- END_d2293a94f30ac54ea9fb5169e8d352f1 -->

<!-- START_f900803af9388f7c931c8a40ecd2fab9 -->
## api/markAsRead
> Example request:

```bash
curl -X POST "http://localhost/api/markAsRead" 
```

```javascript
const url = new URL("http://localhost/api/markAsRead");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/markAsRead`


<!-- END_f900803af9388f7c931c8a40ecd2fab9 -->

<!-- START_db1d57a319e02bcc5a6e3104ddd55d26 -->
## api/addUserToConversation
> Example request:

```bash
curl -X POST "http://localhost/api/addUserToConversation" 
```

```javascript
const url = new URL("http://localhost/api/addUserToConversation");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/addUserToConversation`


<!-- END_db1d57a319e02bcc5a6e3104ddd55d26 -->

<!-- START_a84dd15569a1e70ebd09b06a30fafbf3 -->
## api/removeUserFromConversation
> Example request:

```bash
curl -X POST "http://localhost/api/removeUserFromConversation" 
```

```javascript
const url = new URL("http://localhost/api/removeUserFromConversation");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/removeUserFromConversation`


<!-- END_a84dd15569a1e70ebd09b06a30fafbf3 -->

<!-- START_66e08d3cc8222573018fed49e121e96d -->
## Show the application&#039;s login form.

> Example request:

```bash
curl -X GET -G "http://localhost/login" 
```

```javascript
const url = new URL("http://localhost/login");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
null
```

### HTTP Request
`GET login`


<!-- END_66e08d3cc8222573018fed49e121e96d -->

<!-- START_ba35aa39474cb98cfb31829e70eb8b74 -->
## Handle a login request to the application.

> Example request:

```bash
curl -X POST "http://localhost/login" 
```

```javascript
const url = new URL("http://localhost/login");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST login`


<!-- END_ba35aa39474cb98cfb31829e70eb8b74 -->

<!-- START_e65925f23b9bc6b93d9356895f29f80c -->
## Log the user out of the application.

> Example request:

```bash
curl -X POST "http://localhost/logout" 
```

```javascript
const url = new URL("http://localhost/logout");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST logout`


<!-- END_e65925f23b9bc6b93d9356895f29f80c -->

<!-- START_ff38dfb1bd1bb7e1aa24b4e1792a9768 -->
## Show the application registration form.

> Example request:

```bash
curl -X GET -G "http://localhost/register" 
```

```javascript
const url = new URL("http://localhost/register");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
null
```

### HTTP Request
`GET register`


<!-- END_ff38dfb1bd1bb7e1aa24b4e1792a9768 -->

<!-- START_d7aad7b5ac127700500280d511a3db01 -->
## Handle a registration request for the application.

> Example request:

```bash
curl -X POST "http://localhost/register" 
```

```javascript
const url = new URL("http://localhost/register");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST register`


<!-- END_d7aad7b5ac127700500280d511a3db01 -->

<!-- START_d72797bae6d0b1f3a341ebb1f8900441 -->
## Display the form to request a password reset link.

> Example request:

```bash
curl -X GET -G "http://localhost/password/reset" 
```

```javascript
const url = new URL("http://localhost/password/reset");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
null
```

### HTTP Request
`GET password/reset`


<!-- END_d72797bae6d0b1f3a341ebb1f8900441 -->

<!-- START_feb40f06a93c80d742181b6ffb6b734e -->
## Send a reset link to the given user.

> Example request:

```bash
curl -X POST "http://localhost/password/email" 
```

```javascript
const url = new URL("http://localhost/password/email");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST password/email`


<!-- END_feb40f06a93c80d742181b6ffb6b734e -->

<!-- START_e1605a6e5ceee9d1aeb7729216635fd7 -->
## Display the password reset view for the given token.

If no token is present, display the link request form.

> Example request:

```bash
curl -X GET -G "http://localhost/password/reset/{token}" 
```

```javascript
const url = new URL("http://localhost/password/reset/{token}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
null
```

### HTTP Request
`GET password/reset/{token}`


<!-- END_e1605a6e5ceee9d1aeb7729216635fd7 -->

<!-- START_cafb407b7a846b31491f97719bb15aef -->
## Reset the given user&#039;s password.

> Example request:

```bash
curl -X POST "http://localhost/password/reset" 
```

```javascript
const url = new URL("http://localhost/password/reset");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST password/reset`


<!-- END_cafb407b7a846b31491f97719bb15aef -->

<!-- START_c88fc6aa6eb1bee7a494d3c0a02038b1 -->
## Show the email verification notice.

> Example request:

```bash
curl -X GET -G "http://localhost/email/verify" 
```

```javascript
const url = new URL("http://localhost/email/verify");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET email/verify`


<!-- END_c88fc6aa6eb1bee7a494d3c0a02038b1 -->

<!-- START_af069c1c23cec25f2be1688621969179 -->
## Mark the authenticated user&#039;s email address as verified.

> Example request:

```bash
curl -X GET -G "http://localhost/email/verify/{id}" 
```

```javascript
const url = new URL("http://localhost/email/verify/{id}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET email/verify/{id}`


<!-- END_af069c1c23cec25f2be1688621969179 -->

<!-- START_b44c38c624a55f23870089f09fba5cef -->
## Resend the email verification notification.

> Example request:

```bash
curl -X GET -G "http://localhost/email/resend" 
```

```javascript
const url = new URL("http://localhost/email/resend");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET email/resend`


<!-- END_b44c38c624a55f23870089f09fba5cef -->

<!-- START_02ae14aac4e98b3e7489cc5a5801e1e0 -->
## orgselect
> Example request:

```bash
curl -X GET -G "http://localhost/orgselect" 
```

```javascript
const url = new URL("http://localhost/orgselect");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (403):

```json
{
    "message": "Your email address is not verified.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 943,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Auth\/Middleware\/EnsureEmailIsVerified.php",
            "line": 24,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\EnsureEmailIsVerified",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET orgselect`


<!-- END_02ae14aac4e98b3e7489cc5a5801e1e0 -->

<!-- START_a65a4f3dbc060aa12d6f9ef308fc74ef -->
## orgselect
> Example request:

```bash
curl -X POST "http://localhost/orgselect" 
```

```javascript
const url = new URL("http://localhost/orgselect");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST orgselect`


<!-- END_a65a4f3dbc060aa12d6f9ef308fc74ef -->

<!-- START_89966bfb9ab533cc3249b91a9090d3dc -->
## Display a listing of the User.

> Example request:

```bash
curl -X GET -G "http://localhost/users" 
```

```javascript
const url = new URL("http://localhost/users");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (403):

```json
{
    "message": "Your email address is not verified.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 943,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Auth\/Middleware\/EnsureEmailIsVerified.php",
            "line": 24,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\EnsureEmailIsVerified",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET users`


<!-- END_89966bfb9ab533cc3249b91a9090d3dc -->

<!-- START_04094f136cb91c117bde084191e6859d -->
## Show the form for creating a new User.

> Example request:

```bash
curl -X GET -G "http://localhost/users/create" 
```

```javascript
const url = new URL("http://localhost/users/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (403):

```json
{
    "message": "Your email address is not verified.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 943,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Auth\/Middleware\/EnsureEmailIsVerified.php",
            "line": 24,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\EnsureEmailIsVerified",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET users/create`


<!-- END_04094f136cb91c117bde084191e6859d -->

<!-- START_57a8a4ba671355511e22780b1b63690e -->
## Store a newly created User in storage.

> Example request:

```bash
curl -X POST "http://localhost/users" 
```

```javascript
const url = new URL("http://localhost/users");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST users`


<!-- END_57a8a4ba671355511e22780b1b63690e -->

<!-- START_5693ac2f2e21af3ebc471cd5a6244460 -->
## Display the specified User.

> Example request:

```bash
curl -X GET -G "http://localhost/users/{user}" 
```

```javascript
const url = new URL("http://localhost/users/{user}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (403):

```json
{
    "message": "Your email address is not verified.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 943,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Auth\/Middleware\/EnsureEmailIsVerified.php",
            "line": 24,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\EnsureEmailIsVerified",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET users/{user}`


<!-- END_5693ac2f2e21af3ebc471cd5a6244460 -->

<!-- START_9c6e6c2d3215b1ba7d13468e7cd95e62 -->
## Show the form for editing the specified User.

> Example request:

```bash
curl -X GET -G "http://localhost/users/{user}/edit" 
```

```javascript
const url = new URL("http://localhost/users/{user}/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (403):

```json
{
    "message": "Your email address is not verified.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 943,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Auth\/Middleware\/EnsureEmailIsVerified.php",
            "line": 24,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\EnsureEmailIsVerified",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET users/{user}/edit`


<!-- END_9c6e6c2d3215b1ba7d13468e7cd95e62 -->

<!-- START_7fe085c671e1b3d51e86136538b1d63f -->
## Update the specified User in storage.

> Example request:

```bash
curl -X PUT "http://localhost/users/{user}" 
```

```javascript
const url = new URL("http://localhost/users/{user}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT users/{user}`

`PATCH users/{user}`


<!-- END_7fe085c671e1b3d51e86136538b1d63f -->

<!-- START_a948aef61c80bf96137d023464fde21f -->
## Remove the specified User from storage.

> Example request:

```bash
curl -X DELETE "http://localhost/users/{user}" 
```

```javascript
const url = new URL("http://localhost/users/{user}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE users/{user}`


<!-- END_a948aef61c80bf96137d023464fde21f -->

<!-- START_cb859c8e84c35d7133b6a6c8eac253f8 -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET -G "http://localhost/home" 
```

```javascript
const url = new URL("http://localhost/home");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET home`


<!-- END_cb859c8e84c35d7133b6a6c8eac253f8 -->

<!-- START_2d588dfb3fe3dcbf1948e019c11b9703 -->
## Display a listing of the Organization.

> Example request:

```bash
curl -X GET -G "http://localhost/organizations" 
```

```javascript
const url = new URL("http://localhost/organizations");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET organizations`


<!-- END_2d588dfb3fe3dcbf1948e019c11b9703 -->

<!-- START_d02f27fe8215f08b6a5d3201d9e03113 -->
## Show the form for creating a new Organization.

> Example request:

```bash
curl -X GET -G "http://localhost/organizations/create" 
```

```javascript
const url = new URL("http://localhost/organizations/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET organizations/create`


<!-- END_d02f27fe8215f08b6a5d3201d9e03113 -->

<!-- START_b4846fc0e5e4c262bbe6925dc613a4fe -->
## Store a newly created Organization in storage.

> Example request:

```bash
curl -X POST "http://localhost/organizations" 
```

```javascript
const url = new URL("http://localhost/organizations");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST organizations`


<!-- END_b4846fc0e5e4c262bbe6925dc613a4fe -->

<!-- START_5f2f9b2364f5e9db007268a05f29da51 -->
## Display the specified Organization.

> Example request:

```bash
curl -X GET -G "http://localhost/organizations/{organization}" 
```

```javascript
const url = new URL("http://localhost/organizations/{organization}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET organizations/{organization}`


<!-- END_5f2f9b2364f5e9db007268a05f29da51 -->

<!-- START_bff658d83670fc82141bc4dd01be5c61 -->
## Show the form for editing the specified Organization.

> Example request:

```bash
curl -X GET -G "http://localhost/organizations/{organization}/edit" 
```

```javascript
const url = new URL("http://localhost/organizations/{organization}/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET organizations/{organization}/edit`


<!-- END_bff658d83670fc82141bc4dd01be5c61 -->

<!-- START_0d0f15c63312229662eefb7b661a9844 -->
## Update the specified Organization in storage.

> Example request:

```bash
curl -X PUT "http://localhost/organizations/{organization}" 
```

```javascript
const url = new URL("http://localhost/organizations/{organization}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT organizations/{organization}`

`PATCH organizations/{organization}`


<!-- END_0d0f15c63312229662eefb7b661a9844 -->

<!-- START_5ac9f43f415e51184088586b6c4f0fcb -->
## Remove the specified Organization from storage.

> Example request:

```bash
curl -X DELETE "http://localhost/organizations/{organization}" 
```

```javascript
const url = new URL("http://localhost/organizations/{organization}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE organizations/{organization}`


<!-- END_5ac9f43f415e51184088586b6c4f0fcb -->

<!-- START_65de2d794708d136d59060172ca88093 -->
## Display a listing of the Calendar.

> Example request:

```bash
curl -X GET -G "http://localhost/calendars" 
```

```javascript
const url = new URL("http://localhost/calendars");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET calendars`


<!-- END_65de2d794708d136d59060172ca88093 -->

<!-- START_8e56d1eac96ffb4ca89a965999badf15 -->
## Show the form for creating a new Calendar.

> Example request:

```bash
curl -X GET -G "http://localhost/calendars/create" 
```

```javascript
const url = new URL("http://localhost/calendars/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET calendars/create`


<!-- END_8e56d1eac96ffb4ca89a965999badf15 -->

<!-- START_bd49a23558163cf817d82c4df6c09c2a -->
## Store a newly created Calendar in storage.

> Example request:

```bash
curl -X POST "http://localhost/calendars" 
```

```javascript
const url = new URL("http://localhost/calendars");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST calendars`


<!-- END_bd49a23558163cf817d82c4df6c09c2a -->

<!-- START_6a802d67b303137196e0388e16f86e5a -->
## Display the specified Calendar.

> Example request:

```bash
curl -X GET -G "http://localhost/calendars/{calendar}" 
```

```javascript
const url = new URL("http://localhost/calendars/{calendar}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET calendars/{calendar}`


<!-- END_6a802d67b303137196e0388e16f86e5a -->

<!-- START_3a1158c2fe1e5fe2762f0abc6f91992b -->
## Show the form for editing the specified Calendar.

> Example request:

```bash
curl -X GET -G "http://localhost/calendars/{calendar}/edit" 
```

```javascript
const url = new URL("http://localhost/calendars/{calendar}/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET calendars/{calendar}/edit`


<!-- END_3a1158c2fe1e5fe2762f0abc6f91992b -->

<!-- START_3a182c31d27cbca36a3512d39d7cc65d -->
## Update the specified Calendar in storage.

> Example request:

```bash
curl -X PUT "http://localhost/calendars/{calendar}" 
```

```javascript
const url = new URL("http://localhost/calendars/{calendar}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT calendars/{calendar}`

`PATCH calendars/{calendar}`


<!-- END_3a182c31d27cbca36a3512d39d7cc65d -->

<!-- START_a2f246bf190cfcc0e7640ee3519747d0 -->
## Remove the specified Calendar from storage.

> Example request:

```bash
curl -X DELETE "http://localhost/calendars/{calendar}" 
```

```javascript
const url = new URL("http://localhost/calendars/{calendar}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE calendars/{calendar}`


<!-- END_a2f246bf190cfcc0e7640ee3519747d0 -->

<!-- START_8ac29bcd9493435712423764e2eb2ba5 -->
## Restore the specified Calendar from storage.

> Example request:

```bash
curl -X GET -G "http://localhost/calendars/{id}/restore" 
```

```javascript
const url = new URL("http://localhost/calendars/{id}/restore");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET calendars/{id}/restore`


<!-- END_8ac29bcd9493435712423764e2eb2ba5 -->

<!-- START_fabf0ab0617d740fb9c88675f7c3ba0a -->
## Display a listing of the Department.

> Example request:

```bash
curl -X GET -G "http://localhost/departments" 
```

```javascript
const url = new URL("http://localhost/departments");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET departments`


<!-- END_fabf0ab0617d740fb9c88675f7c3ba0a -->

<!-- START_3644a448c0a17aa47593764993cf8ba6 -->
## Show the form for creating a new Department.

> Example request:

```bash
curl -X GET -G "http://localhost/departments/create" 
```

```javascript
const url = new URL("http://localhost/departments/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET departments/create`


<!-- END_3644a448c0a17aa47593764993cf8ba6 -->

<!-- START_c30ce9fabab53b9895cc4c5c4d12a279 -->
## Store a newly created Department in storage.

> Example request:

```bash
curl -X POST "http://localhost/departments" 
```

```javascript
const url = new URL("http://localhost/departments");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST departments`


<!-- END_c30ce9fabab53b9895cc4c5c4d12a279 -->

<!-- START_f5ab3f08844f383a56b3b309cd8d570a -->
## Display the specified Department.

> Example request:

```bash
curl -X GET -G "http://localhost/departments/{department}" 
```

```javascript
const url = new URL("http://localhost/departments/{department}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET departments/{department}`


<!-- END_f5ab3f08844f383a56b3b309cd8d570a -->

<!-- START_12f6179c32f433ba900dbe9e8180acc2 -->
## Show the form for editing the specified Department.

> Example request:

```bash
curl -X GET -G "http://localhost/departments/{department}/edit" 
```

```javascript
const url = new URL("http://localhost/departments/{department}/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET departments/{department}/edit`


<!-- END_12f6179c32f433ba900dbe9e8180acc2 -->

<!-- START_e4253dc409b7fb8c6944e452f28fa6df -->
## Update the specified Department in storage.

> Example request:

```bash
curl -X PUT "http://localhost/departments/{department}" 
```

```javascript
const url = new URL("http://localhost/departments/{department}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT departments/{department}`

`PATCH departments/{department}`


<!-- END_e4253dc409b7fb8c6944e452f28fa6df -->

<!-- START_87b1583adde689aa0bf0d307ce80aa46 -->
## Remove the specified Department from storage.

> Example request:

```bash
curl -X DELETE "http://localhost/departments/{department}" 
```

```javascript
const url = new URL("http://localhost/departments/{department}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE departments/{department}`


<!-- END_87b1583adde689aa0bf0d307ce80aa46 -->

<!-- START_e1b3ab13af668c31ca19ab2c45f9e799 -->
## Restore the specified Department from storage.

> Example request:

```bash
curl -X GET -G "http://localhost/departments/{id}/restore" 
```

```javascript
const url = new URL("http://localhost/departments/{id}/restore");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET departments/{id}/restore`


<!-- END_e1b3ab13af668c31ca19ab2c45f9e799 -->

<!-- START_815be665d85e6cdc4f9dd2e4ed02b43a -->
## Display a listing of the Employee.

> Example request:

```bash
curl -X GET -G "http://localhost/employees" 
```

```javascript
const url = new URL("http://localhost/employees");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET employees`


<!-- END_815be665d85e6cdc4f9dd2e4ed02b43a -->

<!-- START_432e997f4f2d85d78a195d97ea41fab5 -->
## Show the form for creating a new Employee.

> Example request:

```bash
curl -X GET -G "http://localhost/employees/create" 
```

```javascript
const url = new URL("http://localhost/employees/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET employees/create`


<!-- END_432e997f4f2d85d78a195d97ea41fab5 -->

<!-- START_9cc70b33d737e330f7bffac718b713a3 -->
## Store a newly created Employee in storage.

> Example request:

```bash
curl -X POST "http://localhost/employees" 
```

```javascript
const url = new URL("http://localhost/employees");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST employees`


<!-- END_9cc70b33d737e330f7bffac718b713a3 -->

<!-- START_06f2eb5f856a0ef55af429d03bf744c9 -->
## Display the specified Employee.

> Example request:

```bash
curl -X GET -G "http://localhost/employees/{employee}" 
```

```javascript
const url = new URL("http://localhost/employees/{employee}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET employees/{employee}`


<!-- END_06f2eb5f856a0ef55af429d03bf744c9 -->

<!-- START_e6358a0946e3afd44725f739c4701f7c -->
## Show the form for editing the specified Employee.

> Example request:

```bash
curl -X GET -G "http://localhost/employees/{employee}/edit" 
```

```javascript
const url = new URL("http://localhost/employees/{employee}/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET employees/{employee}/edit`


<!-- END_e6358a0946e3afd44725f739c4701f7c -->

<!-- START_f8822467a2c47bdd2d1757a482aefc44 -->
## Update the specified Employee in storage.

> Example request:

```bash
curl -X PUT "http://localhost/employees/{employee}" 
```

```javascript
const url = new URL("http://localhost/employees/{employee}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT employees/{employee}`

`PATCH employees/{employee}`


<!-- END_f8822467a2c47bdd2d1757a482aefc44 -->

<!-- START_b29cedce4a92f368e8497302c9ecae93 -->
## Remove the specified Employee from storage.

> Example request:

```bash
curl -X DELETE "http://localhost/employees/{employee}" 
```

```javascript
const url = new URL("http://localhost/employees/{employee}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE employees/{employee}`


<!-- END_b29cedce4a92f368e8497302c9ecae93 -->

<!-- START_1119b320aeaffbb76de8a2cd16497ab2 -->
## Restore the specified Employee from storage.

> Example request:

```bash
curl -X GET -G "http://localhost/employees/{id}/restore" 
```

```javascript
const url = new URL("http://localhost/employees/{id}/restore");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET employees/{id}/restore`


<!-- END_1119b320aeaffbb76de8a2cd16497ab2 -->

<!-- START_4362b419b02521076efdbf5b992a46e6 -->
## laborcost
> Example request:

```bash
curl -X GET -G "http://localhost/laborcost" 
```

```javascript
const url = new URL("http://localhost/laborcost");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET laborcost`


<!-- END_4362b419b02521076efdbf5b992a46e6 -->

<!-- START_a367bef3718919066008f5c7feaab4ae -->
## getUserDepartments
> Example request:

```bash
curl -X POST "http://localhost/getUserDepartments" 
```

```javascript
const url = new URL("http://localhost/getUserDepartments");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST getUserDepartments`


<!-- END_a367bef3718919066008f5c7feaab4ae -->

<!-- START_bea8d4b233fdf1cdd89f6573dc0df768 -->
## getOrgUsers
> Example request:

```bash
curl -X POST "http://localhost/getOrgUsers" 
```

```javascript
const url = new URL("http://localhost/getOrgUsers");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST getOrgUsers`


<!-- END_bea8d4b233fdf1cdd89f6573dc0df768 -->

<!-- START_2f4ebf8f545d850ef7eece0d17d076e0 -->
## getDepUsers
> Example request:

```bash
curl -X POST "http://localhost/getDepUsers" 
```

```javascript
const url = new URL("http://localhost/getDepUsers");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST getDepUsers`


<!-- END_2f4ebf8f545d850ef7eece0d17d076e0 -->

<!-- START_f5f6012ff5b8317dcd9f1b3bebda8e44 -->
## calendars/{id}/approve
> Example request:

```bash
curl -X GET -G "http://localhost/calendars/{id}/approve" 
```

```javascript
const url = new URL("http://localhost/calendars/{id}/approve");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET calendars/{id}/approve`


<!-- END_f5f6012ff5b8317dcd9f1b3bebda8e44 -->

<!-- START_e860a0b18a7942b7680b60730f9f9a70 -->
## calendars/{id}/reject
> Example request:

```bash
curl -X GET -G "http://localhost/calendars/{id}/reject" 
```

```javascript
const url = new URL("http://localhost/calendars/{id}/reject");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET calendars/{id}/reject`


<!-- END_e860a0b18a7942b7680b60730f9f9a70 -->

<!-- START_9712002c78676839c737021063255128 -->
## importUsers
> Example request:

```bash
curl -X GET -G "http://localhost/importUsers" 
```

```javascript
const url = new URL("http://localhost/importUsers");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET importUsers`


<!-- END_9712002c78676839c737021063255128 -->

<!-- START_1a700f21a441e468db2a0fd048f1d135 -->
## exportUsers
> Example request:

```bash
curl -X GET -G "http://localhost/exportUsers" 
```

```javascript
const url = new URL("http://localhost/exportUsers");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET exportUsers`


<!-- END_1a700f21a441e468db2a0fd048f1d135 -->

<!-- START_1a2adcf4a9cc802ba4ee6c985b967877 -->
## downloadEmployeeCsv
> Example request:

```bash
curl -X GET -G "http://localhost/downloadEmployeeCsv" 
```

```javascript
const url = new URL("http://localhost/downloadEmployeeCsv");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET downloadEmployeeCsv`


<!-- END_1a2adcf4a9cc802ba4ee6c985b967877 -->

<!-- START_2f87fe698eb2ded97ecbb8384d645096 -->
## importCalendars
> Example request:

```bash
curl -X GET -G "http://localhost/importCalendars" 
```

```javascript
const url = new URL("http://localhost/importCalendars");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET importCalendars`


<!-- END_2f87fe698eb2ded97ecbb8384d645096 -->

<!-- START_f059a1e6d142fbfedc81ea8124ac24de -->
## exportCalendars
> Example request:

```bash
curl -X GET -G "http://localhost/exportCalendars" 
```

```javascript
const url = new URL("http://localhost/exportCalendars");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET exportCalendars`


<!-- END_f059a1e6d142fbfedc81ea8124ac24de -->

<!-- START_5b88414320c77ad61f074b8bc954aeb2 -->
## downloadCalendarCsv
> Example request:

```bash
curl -X GET -G "http://localhost/downloadCalendarCsv" 
```

```javascript
const url = new URL("http://localhost/downloadCalendarCsv");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET downloadCalendarCsv`


<!-- END_5b88414320c77ad61f074b8bc954aeb2 -->

<!-- START_7d0386c193964efff4de71fa2b9ffbf2 -->
## Display a listing of the ContactForm.

> Example request:

```bash
curl -X GET -G "http://localhost/contactForms" 
```

```javascript
const url = new URL("http://localhost/contactForms");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (403):

```json
{
    "message": "Your email address is not verified.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 943,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Auth\/Middleware\/EnsureEmailIsVerified.php",
            "line": 24,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\EnsureEmailIsVerified",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET contactForms`


<!-- END_7d0386c193964efff4de71fa2b9ffbf2 -->

<!-- START_bcea042f64f578706d79f6a9b968d014 -->
## Show the form for creating a new ContactForm.

> Example request:

```bash
curl -X GET -G "http://localhost/contactForms/create" 
```

```javascript
const url = new URL("http://localhost/contactForms/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (403):

```json
{
    "message": "Your email address is not verified.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 943,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Auth\/Middleware\/EnsureEmailIsVerified.php",
            "line": 24,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\EnsureEmailIsVerified",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET contactForms/create`


<!-- END_bcea042f64f578706d79f6a9b968d014 -->

<!-- START_751cae7350ae65586c3ee0796b91ec7a -->
## Store a newly created ContactForm in storage.

> Example request:

```bash
curl -X POST "http://localhost/contactForms" 
```

```javascript
const url = new URL("http://localhost/contactForms");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST contactForms`


<!-- END_751cae7350ae65586c3ee0796b91ec7a -->

<!-- START_f14b830da3495c45519c810694663d88 -->
## Display the specified ContactForm.

> Example request:

```bash
curl -X GET -G "http://localhost/contactForms/{contactForm}" 
```

```javascript
const url = new URL("http://localhost/contactForms/{contactForm}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (403):

```json
{
    "message": "Your email address is not verified.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 943,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Auth\/Middleware\/EnsureEmailIsVerified.php",
            "line": 24,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\EnsureEmailIsVerified",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET contactForms/{contactForm}`


<!-- END_f14b830da3495c45519c810694663d88 -->

<!-- START_6a48ba500db093f09a05adbb3d739eb8 -->
## Show the form for editing the specified ContactForm.

> Example request:

```bash
curl -X GET -G "http://localhost/contactForms/{contactForm}/edit" 
```

```javascript
const url = new URL("http://localhost/contactForms/{contactForm}/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (403):

```json
{
    "message": "Your email address is not verified.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Application.php",
    "line": 943,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/helpers.php",
            "line": 46,
            "function": "abort",
            "class": "Illuminate\\Foundation\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Auth\/Middleware\/EnsureEmailIsVerified.php",
            "line": 24,
            "function": "abort"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Auth\\Middleware\\EnsureEmailIsVerified",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET contactForms/{contactForm}/edit`


<!-- END_6a48ba500db093f09a05adbb3d739eb8 -->

<!-- START_d61ec75966d972253dd5105b873deca1 -->
## Update the specified ContactForm in storage.

> Example request:

```bash
curl -X PUT "http://localhost/contactForms/{contactForm}" 
```

```javascript
const url = new URL("http://localhost/contactForms/{contactForm}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT contactForms/{contactForm}`

`PATCH contactForms/{contactForm}`


<!-- END_d61ec75966d972253dd5105b873deca1 -->

<!-- START_db29705652a478707272bfa0ec721c29 -->
## Remove the specified ContactForm from storage.

> Example request:

```bash
curl -X DELETE "http://localhost/contactForms/{contactForm}" 
```

```javascript
const url = new URL("http://localhost/contactForms/{contactForm}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE contactForms/{contactForm}`


<!-- END_db29705652a478707272bfa0ec721c29 -->

<!-- START_0c3dc1c68ddf7945c4402a85f646c2c8 -->
## Display a listing of the OrganizationVendors.

> Example request:

```bash
curl -X GET -G "http://localhost/organizationVendors" 
```

```javascript
const url = new URL("http://localhost/organizationVendors");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (500):

```json
{
    "message": "SQLSTATE[HY000] [2002] Connection refused (SQL: select * from `organization_vendors` where `organization_vendors`.`deleted_at` is null)",
    "exception": "Illuminate\\Database\\QueryException",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
    "line": 664,
    "trace": [
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
            "line": 624,
            "function": "runQueryCallback",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Connection.php",
            "line": 333,
            "function": "run",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2095,
            "function": "select",
            "class": "Illuminate\\Database\\Connection",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2083,
            "function": "runSelect",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2569,
            "function": "Illuminate\\Database\\Query\\{closure}",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Query\/Builder.php",
            "line": 2084,
            "function": "onceWithColumns",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Eloquent\/Builder.php",
            "line": 516,
            "function": "get",
            "class": "Illuminate\\Database\\Query\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Eloquent\/Builder.php",
            "line": 500,
            "function": "getModels",
            "class": "Illuminate\\Database\\Eloquent\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Database\/Eloquent\/Model.php",
            "line": 461,
            "function": "get",
            "class": "Illuminate\\Database\\Eloquent\\Builder",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/prettus\/l5-repository\/src\/Prettus\/Repository\/Eloquent\/BaseRepository.php",
            "line": 328,
            "function": "all",
            "class": "Illuminate\\Database\\Eloquent\\Model",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/app\/Http\/Controllers\/OrganizationVendorsController.php",
            "line": 29,
            "function": "all",
            "class": "Prettus\\Repository\\Eloquent\\BaseRepository",
            "type": "->"
        },
        {
            "function": "index",
            "class": "App\\Http\\Controllers\\OrganizationVendorsController",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Controller.php",
            "line": 54,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/ControllerDispatcher.php",
            "line": 45,
            "function": "callAction",
            "class": "Illuminate\\Routing\\Controller",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 219,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\ControllerDispatcher",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 176,
            "function": "runController",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 682,
            "function": "run",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET organizationVendors`


<!-- END_0c3dc1c68ddf7945c4402a85f646c2c8 -->

<!-- START_a0feeb888ee3e528365aa0a26504ea12 -->
## Show the form for creating a new OrganizationVendors.

> Example request:

```bash
curl -X GET -G "http://localhost/organizationVendors/create" 
```

```javascript
const url = new URL("http://localhost/organizationVendors/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (500):

```json
{
    "message": "Call to a member function hasRole() on null",
    "exception": "Symfony\\Component\\Debug\\Exception\\FatalThrowableError",
    "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/app\/Http\/Controllers\/OrganizationVendorsController.php",
    "line": 41,
    "trace": [
        {
            "function": "create",
            "class": "App\\Http\\Controllers\\OrganizationVendorsController",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Controller.php",
            "line": 54,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/ControllerDispatcher.php",
            "line": 45,
            "function": "callAction",
            "class": "Illuminate\\Routing\\Controller",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 219,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\ControllerDispatcher",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 176,
            "function": "runController",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 682,
            "function": "run",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/VerifyCsrfToken.php",
            "line": 75,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\VerifyCsrfToken",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/AuthenticateSession.php",
            "line": 39,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\AuthenticateSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 66,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 684,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 659,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 614,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/barryvdh\/laravel-debugbar\/src\/Middleware\/InjectDebugbar.php",
            "line": 58,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Barryvdh\\Debugbar\\Middleware\\InjectDebugbar",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 31,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/CheckForMaintenanceMode.php",
            "line": 62,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 151,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 104,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 272,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 256,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseStrategies\/ResponseCallStrategy.php",
            "line": 33,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 49,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/ResponseResolver.php",
            "line": 68,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Tools\/Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 196,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/mpociot\/laravel-apidoc-generator\/src\/Commands\/GenerateDocumentation.php",
            "line": 57,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 572,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 901,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 262,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/symfony\/console\/Application.php",
            "line": 145,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 89,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 122,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/Users\/aymanshaltout\/MyHard\/Momentum\/Buttercup\/Project\/bc_admin_portal\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET organizationVendors/create`


<!-- END_a0feeb888ee3e528365aa0a26504ea12 -->

<!-- START_e4acc04413b7997926a00537f752ee91 -->
## Store a newly created OrganizationVendors in storage.

> Example request:

```bash
curl -X POST "http://localhost/organizationVendors" 
```

```javascript
const url = new URL("http://localhost/organizationVendors");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST organizationVendors`


<!-- END_e4acc04413b7997926a00537f752ee91 -->

<!-- START_ba7fb231cad66fc4a50e984d1ec5cd27 -->
## Display the specified OrganizationVendors.

> Example request:

```bash
curl -X GET -G "http://localhost/organizationVendors/{organizationVendor}" 
```

```javascript
const url = new URL("http://localhost/organizationVendors/{organizationVendor}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET organizationVendors/{organizationVendor}`


<!-- END_ba7fb231cad66fc4a50e984d1ec5cd27 -->

<!-- START_d6f04f378fb650f3cd84ac10d48fa140 -->
## Show the form for editing the specified OrganizationVendors.

> Example request:

```bash
curl -X GET -G "http://localhost/organizationVendors/{organizationVendor}/edit" 
```

```javascript
const url = new URL("http://localhost/organizationVendors/{organizationVendor}/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (302):

```json
null
```

### HTTP Request
`GET organizationVendors/{organizationVendor}/edit`


<!-- END_d6f04f378fb650f3cd84ac10d48fa140 -->

<!-- START_3ac874b598c55b0ac5f1ed0f3bd08e6a -->
## Update the specified OrganizationVendors in storage.

> Example request:

```bash
curl -X PUT "http://localhost/organizationVendors/{organizationVendor}" 
```

```javascript
const url = new URL("http://localhost/organizationVendors/{organizationVendor}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT organizationVendors/{organizationVendor}`

`PATCH organizationVendors/{organizationVendor}`


<!-- END_3ac874b598c55b0ac5f1ed0f3bd08e6a -->

<!-- START_873c3e4d5b47ba4b32914ba7408ee957 -->
## Remove the specified OrganizationVendors from storage.

> Example request:

```bash
curl -X DELETE "http://localhost/organizationVendors/{organizationVendor}" 
```

```javascript
const url = new URL("http://localhost/organizationVendors/{organizationVendor}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE organizationVendors/{organizationVendor}`


<!-- END_873c3e4d5b47ba4b32914ba7408ee957 -->


