$(document).ready(function() {
	//Select Boxes
	$('.select2').select2();
	//End Select Boxes
	var shiftDiv = $('#shiftManagment');
	shiftDiv.hide();
	$('.repeat').addClass('d-none');
	/* Region Start Calendars Page */
	var datas;
	// $('#users_field').empty();
	var userSelect = $('#users_field');
	var orgSelect = $('#organizations_field');

	var departmentSelect = $('#department_field');

	// userSelect.append('<option selected="true" disabled>Choose Users</option>');
	// orgSelect.append(
	//     '<option selected="true" disabled>Choose Organization</option>'
	// );

	//Make the User Field select Only 1 field if the Event Type is Shift
	$('#categoryType').on('change', function(e) {
		$('#users_field')
			.val(null)
			.trigger('change');
		var cat = document.getElementById('categoryType');
		selectedCat = cat.options[cat.selectedIndex].value;

		if (selectedCat != 'Meeting' && selectedCat != 'Shift') {
			userSelect.select2({ maximumSelectionLength: 1 });
		} else {
			userSelect.select2({ multiple: true });
		}

		if (selectedCat == 'Meeting') {
			$('#users_field').empty();
			$.ajax({
				type: 'POST',
				url: '/getDepUsers',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(data) {
					datas = data.response;
					// var datas = [
					// 	{
					// 		text: 'Citrus',
					// 		children: [
					// 			{ id: 'c1', text: 'Grapefruit' },
					// 			{ id: 'c2', text: 'Orange' },
					// 			{ id: 'c3', text: 'Lemon' },
					// 			{ id: 'c4', text: 'Lime' }
					// 		]
					// 	},
					// 	{
					// 		text: 'Other',
					// 		children: [
					// 			{ id: 'o1', text: 'Apple' },
					// 			{ id: 'o2', text: 'Mango' },
					// 			{ id: 'o3', text: 'Banana' }
					// 		]
					// 	}
					// ];
					console.log(datas);
					// $.each(datas, function(key, value) {
					// 	userSelect.append(
					// 		'<option value=' + key + '>' + value + '</option>'
					// 	);
					// });
				}
			});

			$.fn.select2.amd.require(
				['select2/utils', 'select2/dropdown', 'select2/dropdown/attachBody'],
				function(Utils, Dropdown, AttachBody) {
					function SelectAll() {}

					SelectAll.prototype.render = function(decorated) {
						var $rendered = decorated.call(this);
						var self = this;
						var $selectAll = $(
							'<button type="button" class="btn btn-secondary">Add All</button>'
						);
						$rendered.find('.select2-dropdown').prepend($selectAll);
						$selectAll.on('click', function(e) {
							var $results = $rendered.find(
								'.select2-results__option[aria-selected=false]'
							);
							// Get all results that aren't selected
							$results.each(function() {
								var $result = $(this);
								// Get the data object for it
								var data = $result.data('data');
								// Trigger the select event
								self.trigger('select', {
									data: data
								});
							});
							self.trigger('close');
						});
						return $rendered;
					};

					$('#users_field').select2({
						placeholder: 'Select Option(s)...',
						data: datas,
						width: 'auto',
						dropdownAdapter: Utils.Decorate(
							Utils.Decorate(Dropdown, AttachBody),
							SelectAll
						)
					});
				}
			);
		}
		if (selectedCat == 'Shift') {
			$('.repeat').removeClass('d-none');
			$('input[type=checkbox]').on('click', function() {
				if ($('#check1').is(':checked')) {
					shiftDiv.show();
					$('.default-data-event').addClass('d-none');
				} else {
					shiftDiv.hide();
					$('.default-data-event').removeClass('d-none');
				}
			});
		}

		if (selectedCat != 'Shift') {
			shiftDiv.hide();
			$('.default-data-event').removeClass('d-none');
			$('.repeat').addClass('d-none');
		}
	});

	// $('#OrganizationUsers').css('display', 'none');

	/**
	 * Make The Organization users field show only the Users is this organization
	 */
	$('#organizations_field').on('change', function(e) {
		if ((selectedCat = 'Meeting')) {
			$('#users_field').empty();
			// $('#OrganizationUsers').css('display', 'block');
			var org = document.getElementById('organizations_field');
			var selectedOrg = org.options[org.selectedIndex].value;

			$.ajax({
				type: 'POST',
				url: '/getOrgUsers',
				data: {
					orgId: selectedOrg
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(data) {
					datas = data.response;

					$.each(datas, function(key, value) {
						userSelect.append(
							'<option value=' + key + '>' + value + '</option>'
						);
					});
				}
			});
		}
	});

	/* End Region calendar page*/
	/****************************************************************************************************************
	 * In case of Event "Event" Allow to add all department employees
	 */
	/*Region Create Employee Page */
	// $('#department_field').empty();

	$('#user_field').on('change', function(e) {
		$('#department_field').empty();

		selectedUserId = $('#user_field')
			.find(':selected')
			.val();

		$.ajax({
			type: 'POST',
			url: '/getUserDepartments',
			data: {
				userId: selectedUserId
			},
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function(data) {
				datas = data.response;
				console.log(datas);
				$.each(datas, function(key, value) {
					departmentSelect.append(
						'<option value=' + key + '>' + value + '</option>'
					);
				});
			}
		});
	});
	/*End Region Create Employee Page */
	/***************************************************************************************************************/

	// Datepicker
	$('.datetimepicker').datetimepicker({ format: 'YYYY-MM-DD HH:mm' });
	$('#from_time').datetimepicker({ format: 'YYYY-MM-DD HH:mm' });
	$('#to_time').datetimepicker({ format: 'YYYY-MM-DD HH:mm' });
	$('#shift_from_date').datetimepicker({ format: 'YYYY-MM-DD' });
	$('#shift_to_date').datetimepicker({ format: 'YYYY-MM-DD' });
	$('#shift_from_time').datetimepicker({ format: 'HH:mm' });
	$('#shift_to_time').datetimepicker({ format: 'HH:mm' });

	/************ avatar image *************************/
	var readURL = function(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('.profile-pic').attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	};

	$('.file-upload').on('change', function() {
		readURL(this);
		if ($('.upload-button').hasClass('fa-camera')) {
			$('.upload-button').toggleClass('fa-camera fa-times');
		}
	});

	$('.upload-button').on('click', function() {
		$('.file-upload').click();
	});
	/************* Action Buttons  */
	var el = $('.more-action');

	function showMenu(e) {
		e.preventDefault();
		$(this)
			.parent()
			.toggleClass('show-more-menu');
		return false;
	}

	function hideMenu(e) {
		console.log('hideMenu');
		el.removeClass('show-more-menu');
	}

	$('body').on('click', ':not(.show-more-menu .more-btn)', hideMenu);
	$('body').on('click', '.more-btn', showMenu);
});
