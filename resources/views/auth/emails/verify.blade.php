# Hello!

Please click the button below to verify your email address.

Verify Email Address: {!! $url !!}

or copy below values to your mobile app

expires: {!! $api_verification['expires'] !!}
signature: {!! $api_verification['signature'] !!}

If you did not create an account, no further action is required.

Regards,Buttercup.io

If you’re having trouble clicking the "Verify Email Address" button, copy and paste the URL below
into your web browser: [{!! $url !!}]({!! $url !!})

© 2018 Buttercup.io All rights reserved.