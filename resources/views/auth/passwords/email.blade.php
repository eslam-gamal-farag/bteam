<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Twitter -->
	<meta name="twitter:site" content="@bteam.io">
	<meta name="twitter:creator" content="@bteam.io">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="BTeam.io">
	<meta name="twitter:description" content="bteam.io">
	<meta name="twitter:image" content="######bteam.io#######">

	<!-- Facebook -->
	<meta property="og:url" content="http://bteam.io">
	<meta property="og:title" content="BTeam.io">
	<meta property="og:description" content="BTeam.io">
	<meta property="og:image" content="######BTeam.io#######">
	<meta property="og:image:secure_url" content="######BTeam.io#######">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="1200">
	<meta property="og:image:height" content="600">

	<!-- Meta -->
	<meta name="description" content="BTeam.io">
	<meta name="author" content="momentum-sol">

	<title>BTeam.io | Reset Password</title>

	<!-- vendor css -->
	<link href="{!! url('/lib/font-awesome/css/font-awesome.css') !!}" rel="stylesheet">
	<link href="{!! url('/lib/Ionicons/css/ionicons.css') !!}" rel="stylesheet">

	<!-- BTeam.io CSS -->
	<link rel="stylesheet" href="{!! url('/css/slim.css') !!}">
	
	<!-- iCheck -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

</head>
<body>
	<div class="d-md-flex flex-row-reverse">
		<div class="signin-right">
			<div class="signin-box">
				<h2 class="signin-title-primary">Reset password!</h2>	
				<h3 class="signin-title-secondary">Enter Email to reset password</h3>
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif
				<form method="post" action="{{ url('/password/email') }}">
					{!! csrf_field() !!}
					<div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
						<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Enter your email">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						@if ($errors->has('email'))
							<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif
					</div><!-- form-group -->

					<button type="submit" class="btn btn-primary btn-block btn-signin">Send Password Reset Link</button>
				</form>
				<p class="mg-b-0"><a href="{{ url('/password/reset') }}">I forgot my password</a></p>
				<p class="mg-b-0">Don't have an account? <a href="{{ url('/register') }}">Sign Up</a></p>
			</div>

		</div><!-- signin-right -->
		<div class="signin-left">
			<div class="signin-box">
			
				<h2 class="slim-logo"><a href="#">{!! Html::image('/assets/images/bteam_logo_full.png', null, ['class' => 'img-fit-cover']) !!}</a></h2>

			</div>
		</div><!-- signin-left -->
	</div><!-- d-flex -->

	<script src="{!! url('/lib/jquery/js/jquery.js') !!}"></script>
	<script src="{!! url('/lib/popper.js/js/popper.js') !!}"></script>
	<script src="{!! url('/lib/bootstrap/js/bootstrap.js') !!}"></script>
	<script src="{!! url('/js/slim.js') !!}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' // optional
			});
		});
	</script>
</body>
</html>
