<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Twitter -->
	<meta name="twitter:site" content="@bteam.io">
	<meta name="twitter:creator" content="@bteam.io">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="BTeam.io">
	<meta name="twitter:description" content="bteam.io">
	<meta name="twitter:image" content="######bteam.io#######">

	<!-- Facebook -->
	<meta property="og:url" content="http://bteam.io">
	<meta property="og:title" content="BTeam.io">
	<meta property="og:description" content="BTeam.io">
	<meta property="og:image" content="######BTeam.io#######">
	<meta property="og:image:secure_url" content="######BTeam.io#######">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="1200">
	<meta property="og:image:height" content="600">

	<!-- Meta -->
	<meta name="description" content="BTeam.io">
	<meta name="author" content="momentum-sol">

	<title>BTeam.io | Reset Password</title>

	<!-- vendor css -->
	<link href="{!! url('/lib/font-awesome/css/font-awesome.css') !!}" rel="stylesheet">
	<link href="{!! url('/lib/Ionicons/css/ionicons.css') !!}" rel="stylesheet">

	<!-- BTeam.io CSS -->
	<link rel="stylesheet" href="{!! url('/css/slim.css') !!}">
	
	<!-- iCheck -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

</head>
<body>
	<div class="d-md-flex flex-row-reverse">
		<div class="signin-right">
			<div class="signin-box">
				<h2 class="signin-title-primary">Reset password!</h2>
				<h3 class="signin-title-secondary">Enter your new password.</h3>
				<form method="post" action="{{ url('/password/reset') }}">
					{!! csrf_field() !!}
					<input type="hidden" name="token" value="{{ $token }}">
					<div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
						<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						@if ($errors->has('email'))
							<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif
					</div><!-- form-group -->
						
					<div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
						<input type="password" class="form-control" name="password" placeholder="Password">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>

						@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div><!-- form-group -->

					<div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
						<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm password">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>

						@if ($errors->has('password_confirmation'))
							<span class="help-block">
								<strong>{{ $errors->first('password_confirmation') }}</strong>
							</span>
						@endif
					</div><!-- form-group -->
					

					<button type="submit" class="btn btn-primary btn-block btn-signin">Reset Password</button>
				</form>
			</div>

		</div><!-- signin-right -->
		<div class="signin-left">
			<div class="signin-box">
			
				<h2 class="slim-logo"><a href="#">{!! Html::image('/assets/images/bc_logo_full.png', null, ['class' => 'img-fit-cover']) !!}</a></h2>
			
				<p>Browse our site and see for yourself why you need Buttercup.io</p>

				<p><a href="{{ url('/') }}" class="btn btn-outline-secondary pd-x-25">Learn More</a></p>

				
			</div>
		</div><!-- signin-left -->
	</div><!-- d-flex -->

	<script src="{!! url('/lib/jquery/js/jquery.js') !!}"></script>
	<script src="{!! url('/lib/popper.js/js/popper.js') !!}"></script>
	<script src="{!! url('/lib/bootstrap/js/bootstrap.js') !!}"></script>
	<script src="{!! url('/js/slim.js') !!}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' // optional
			});
		});
	</script>
</body>
</html>
