@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::open(['route' => 'calendars.store', 'files' => true]) !!}
				@include('calendars.fields')
			{!! Form::close() !!}
		</div>
	</div>
@endsection
