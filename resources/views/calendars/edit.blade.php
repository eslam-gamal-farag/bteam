@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::model($calendar, ['route' => ['calendars.update', $calendar->id], 'method' => 'patch', 'files' => true]) !!}
				@include('calendars.fields')
			{!! Form::close() !!}
		</div>
	</div>
@endsection