<div class="row">
    <div class="form-group col-lg-6">
        <!-- Title Field -->
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Is Private Field -->
        {{-- <div >
            {!! Form::label('is_private', 'Is Private:') !!}
            <label class="radio-inline">
            {!! Form::radio('is_private', "Public", true) !!} Public
        </label>

        <label class="radio-inline">
            {!! Form::radio('is_private', "Private", null) !!} Private
        </label>

        </div> --}}
    </div>

    <div class="form-group col-lg-6">
        <!-- Category Field -->
        <div class="form-group">
            {!! Form::label('category', 'Category:') !!}
            {!! Form::select('category', ['Meeting' => 'Meeting','Shift' => 'Shift', 'Request Time off' => 'Request Time off', 'Call Out Sick' => 'Call Out Sick','Late To Work' => 'Late To Work'], null, ['class' => 'form-control','id' => 'categoryType']) !!}
        </div>
    </div>
<div class="repeat col-lg-12">
	<label class="ckbox">
    <input id="check1" type="checkbox" name="repeaterCheck">
    <span>Repeat</span>
	</label>
</div>

<div id="shiftManagment" class="form-group col-lg-12 ">
    <div>
				<h6>Repeat at:</h6>
				<label class="ckbox inline">
						{!! Form::checkbox('days[]', 'sunday'); !!}
						<span>Sunday</span>
				</label>

				<label class="ckbox inline">
						{!! Form::checkbox('days[]', 'monday'); !!}
						<span>Monday</span>
				</label>

				<label class="ckbox inline">
						{!! Form::checkbox('days[]', 'tuesday'); !!}
						<span>Tuesday</span>
				</label>

				<label class="ckbox inline">
						{!! Form::checkbox('days[]', 'wednesday'); !!}
						<span>Wednesday</span>
				</label>

				<label class="ckbox inline">
						{!! Form::checkbox('days[]', 'thursday'); !!}
						<span>Thursday</span>
				</label>

				<label class="ckbox inline">
						{!! Form::checkbox('days[]', 'friday'); !!}
						<span>Friday</span>
				</label>

				<label class="ckbox inline">
						{!! Form::checkbox('days[]', 'saturday'); !!}
						<span>Saturday</span>
				</label>

       <div class="row">
            <!-- Event Start Time Field -->
            <div  class="form-group col-sm-6">
                {!! Form::label('from_time', 'From Date:') !!}
                <div   class="input-group date " id="shift_from_date" data-target-input="nearest">
                    {{ Form::input('','shift_from_date', null, [ 'id' => ' shift_from_date', 'class' => 'form-control datetimepicker-input' ,'data-target' =>  '#shift_from_date',  'data-toggle' => 'datetimepicker']) }}
                    <div class="input-group-append" data-target="#shift_from_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>

            <div  class="form-group col-sm-6">
                {!! Form::label('from_time', 'To Date :') !!}
                <div   class="input-group date " id="shift_to_date" data-target-input="nearest">
                    {{ Form::input('','shift_to_date', null, [ 'id' => ' shift_to_date', 'class' => 'form-control datetimepicker-input' ,'data-target' =>  '#shift_to_date',  'data-toggle' => 'datetimepicker']) }}
                    <div class="input-group-append" data-target="#shift_to_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>


            <div  class="form-group col-sm-6">
                {!! Form::label('from_time', 'From Time:') !!}
                <div   class="input-group date " id="shift_from_time" data-target-input="nearest">
                    {{ Form::input('','shift_from_time', null, [ 'id' => ' shift_from_time', 'class' => 'form-control datetimepicker-input' ,'data-target' =>  '#shift_from_time',  'data-toggle' => 'datetimepicker']) }}
                    <div class="input-group-append" data-target="#shift_from_time" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
             </div>





            <div  class="form-group col-sm-6">
                {!! Form::label('from_time', 'To Time:') !!}
                <div   class="input-group date " id="shift_to_time" data-target-input="nearest">
                    {{ Form::input('','shift_to_time', null, [ 'id' => ' shift_to_time', 'class' => 'form-control datetimepicker-input' ,'data-target' =>  '#shift_to_time',  'data-toggle' => 'datetimepicker']) }}
                    <div class="input-group-append" data-target="#shift_to_time" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="default-data-event form-group col-md-12">
    <div class="row">
    <!-- Event Start Time Field -->
    <div  class="form-group col-sm-6">
        {!! Form::label('from_time', 'From:') !!}
        <div   class="input-group date " id="from_time" data-target-input="nearest">
            {{ Form::input('','from_time', null, [ 'id' => ' from_time', 'class' => 'form-control datetimepicker-input' ,'data-target' =>  '#from_time',  'data-toggle' => 'datetimepicker']) }}
            <div class="input-group-append" data-target="#from_time" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
         </div>
    </div>



    <!-- Event End Time Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('to_time', 'To:') !!}
        <div   class="input-group date " id="to_time" data-target-input="nearest">
            {{ Form::input('','to_time', null, [ 'id' => ' to_time', 'class' => 'form-control datetimepicker-input' ,'data-target' =>  '#to_time',  'data-toggle' => 'datetimepicker' ]) }}
            <div class="input-group-append" data-target="#to_time" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
         </div>
    </div>
</div>
</div>
<!-- Users Field -->
<div id="OrganizationUsers" class="form-group col-sm-6">
	{!! Form::label('users', 'Users:') !!}
	{!! Form::select('users[]',$users, null, ['id'=>'users_field','class' => 'form-control select2', 'multiple' => true]) !!}
</div>

<!-- Notes Field -->
<div class="form-group col-sm-12 col-lg-12">
	{!! Form::label('notes', 'Notes:') !!}
	{!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('calendars.index') !!}" class="btn btn-default">Cancel</a>
</div>
</div>
