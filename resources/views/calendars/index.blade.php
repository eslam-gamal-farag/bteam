@extends('layouts.app')

@section('content')
	@include('flash::message')
	<div class="card card-primary">
		<div class="card-header ">
			<div class="row">
				<div class="col-md-6">
					@if(request()->has('trashed'))
					<a class="btn btn-success" href="{!! route('calendars.index') !!}">
						All
					</a>
					@else
					<a class="btn btn-primary" href="{!! route('calendars.create') !!}">
						<i class="fa fa-plus"></i> Add New
					</a>
					<a class="btn btn-primary" href="{!! route('importCalendars') !!}">
						<i class="fa fa-plus"></i> Import Events
					</a>
					<a class="btn btn-primary" href="{!! route('exportCalendars') !!}">
							<i class="fa fa-plus"></i>Export Events
						</a>
					<a class="btn btn-danger" href="{!! route('calendars.index', 'trashed') !!}">
						<i class="fa fa-trash"></i> Trashed
					</a>
					@endif
					
				</div>
		
				<div class="col-md-6">
					{!! Form::open(['method' => 'get', 'url' => 'calendars','id' => 'filter-form']) !!}
					{{-- Form::submit('Filter') --}}
					
					<input class="btn btn-info float-right filter-btn" type="submit" form="filter-form" value="Search"/>
					<select id="statusSelect" class="form-control filter-selectable float-right" name="statusFilter">
						<option selected="true" disabled>Select Status</option>
						<option value="0">Pending</option>
						<option value="1">Approved</option>
						<option value="2">Rejected</option>
						<option value="3">Acknowledge</option>
					</select>
					<select id="categorySelect" class="form-control filter-selectable float-right" name="categoryFilter">
						<option selected="true" disabled>Select Category</option>
						<option value="Meeting">Meeting</option>
						<option value="Shift">Shift</option>
						<option value="Request Time Off">Request Time Off</option>
						<option value="Call Out Sick">Call Out Sick</option>
						<option value="Late To Work">Late To Work</option>
					</select>
					
					
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		<div class="card-body">
			@include('calendars.table')
		</div>
		<div class="card-footer">
			@include('vendor.pagination.paginate', ['records' => $calendars])
		</div>
	</div>

@endsection
