@extends('layouts.app')

@section('content')
	<div class="">
		<div class="">
			@include('calendars.show_fields')
			<a href="{!! route('calendars.index') !!}" class="btn btn-default">Back</a>
		</div>
		
	</div>
@endsection
