<?php //echo "<pre>"; var_dump($calendar->users[0]);?>
<div class="row row-sm mg-t-20">
	<div class=" col-lg-6">
		<div class="card card-calendar">
		<form>

		<!-- Id Field -->
		{{-- <div class="form-group ">
			{!! Form::label({'id', 'Id:','class','col-md-1'}) !!}
			<p>{!! $calendar->id !!}</p>
		</div> --}}

		<!-- Title Field -->
		<div class="form-group row">
			{{-- {!! Form::label({'title', 'Title:','class','col-md-11'}) !!} --}}
			<h3 class="col-md-12" >{!! $calendar->title !!}</h3>
		</div>

		<!-- Is Private Field -->
		<!-- <div class="form-group">
			{!! Form::label('is_private', 'Privacy:') !!}
			<p>{!! $calendar->is_private !!}</p>
		</div> -->

		<!-- Category Field -->
		<div class="form-group row ">
			{!! Form::label('category', 'Type:',array('class'=>'col-sm-1 col-md-2 col-form-label col-form-label-sm')) !!}
			<p class="col-sm-10  mb-0">{!! $calendar->category !!}</p>
		</div>

		<!-- From Time Field -->
		<div id="fromTime" class="form-group row ">
			{!! Form::label('from_time', 'From:',array('class'=>'col-sm-1 col-md-2 col-form-label col-form-label-sm')) !!}
			<p class="col-sm-10  mb-0">{!! $calendar->from_time !!}</p>
		</div>
		<!-- <pre id="ar"><i class="fa fa-long-ar-right" style="font-size:24px"></i></pre> -->
		<!-- To Time Field -->
		<div id="toTime" class="form-group row ">
			{!! Form::label('to_time', 'To:',array('class'=>'col-sm-1 col-md-2 col-form-label col-form-label-sm')) !!}
			<p class="col-sm-10  mb-0">{!! $calendar->to_time !!}</p>
		</div>
		<!-- Notes Field -->
		<div class="form-group row ">
			{!! Form::label('notes', 'Notes:',array('class'=>'col-sm-1 col-md-2 col-form-label col-form-label-sm')) !!}
			<p class="col-sm-10  mb-0">{!! $calendar->notes !!}</p>
		</div>

		</form>
		<div class="card-footer calendar-footer">
		<hr>
		<!-- Created At Field -->
		<div class="form-group float-left">
			{!! Form::label('created_at', 'Created At:') !!}
			<p>{!! $calendar->created_at !!}</p>
		</div>

		<!-- Updated At Field -->
		<div class="form-group float-right">
			{!! Form::label('updated_at', 'Updated At:') !!}
			<p>{!! $calendar->updated_at !!}</p>
		</div>
		</div>
		
	</div>
</div>
<div class="  col-lg-6">
	<div class="card card-calendar">
		<div class="list-group list-group-user event-users">
			@foreach ($calendar->users as $user)			   
			<div class="list-group-item">
				@if($user->employee)
					<img src="{!! $user->employee->avatar !!}" alt="{!! $user->name !!}">
				@endif
				<div class="user-name-address">
					<p>	
					<span class="event-user status-{!! $user->pivot->status !!}">{!! $user->name !!}</span>
					</p>
				</div>
				<!-- <div class="user-btn-wrapper">
					<a href="#" class="btn btn-outline-light btn-icon">
						<div class="tx-20"><i class="icon ion-android-chat"></i></div>
					</a>
					<a href="#" class="btn btn-outline-light btn-icon">
						<div class="tx-20"><i class="icon ion-android-remove"></i></div>
					</a>
					<a href="#" class="btn btn-outline-light btn-icon">
						<div class="tx-20"><i class="icon ion-android-more-vertical"></i></div>
					</a>
					</div> -->
				</div><!-- list-group-item -->
				@endforeach
			</div>
		</div>
	</div>
</div>