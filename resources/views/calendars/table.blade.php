<table class="table table-striped table-hover" id="calendars-table">
	<thead>
		<tr>
			<th>Title</th>
			<th>Users</th>
			<th>Category</th>
			<th>From</th>
			<th>To</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($calendars as $calendar)
			<tr>
				<td>{!! $calendar->title !!}</td>
				<td>
					@foreach ($calendar->users as $user)
					<span class="event-user status-{!! $user->pivot->status !!}">{!! $user->name !!}</span>
					@endforeach
				</td>
				<td>{!! $calendar->category !!}</td>
				<td>{!! Carbon::parse($calendar->from_time)->format('D M d h:i a') !!}</td>
				<td>{!! Carbon::parse($calendar->to_time)->format('D M d h:i a') !!}</td>
				<td>@if($calendar->status_string == "Approved")
					<label class="badge badge-success">Approved</label>
					@elseif($calendar->status_string == "Rejected")
					<label class="badge badge-danger">Rejected</label>
					@else
					<label class="badge badge-secondary">Pending</label>
				@endif
				</td>
				<td>
					{!! Form::open(['route' => ['calendars.destroy', $calendar->id], 'method' => 'delete']) !!}
					<div class="more-action">
						<div id="more-btn" class="more-btn">
							<span class="more-dot"></span>
							<span class="more-dot"></span>
							<span class="more-dot"></span>
						</div>
						<div class="more-menu">
							<div class="more-menu-caret">
								<div class="more-menu-caret-outer"></div>
								<div class="more-menu-caret-inner"></div>
							</div>
							<ul class="more-menu-items" tabindex="-1" role="menu" aria-labelledby="more-btn" aria-hidden="true">
							@if(request()->has('trashed'))
									<li class="more-menu-item" role="presentation">
										<a href="{!! route('calendars.restore', [$calendar->id]) !!}" class='btn btn-warning btn-sm' title="Restore"><i class="fa fa-refresh"></i></a>
									</li>
								@else
									<li class="more-menu-item" role="presentation">
										<a href="{!! route('calendars.show', [$calendar->id]) !!}" class='btn btn-success btn-sm' title="Show Details"><i class="fa fa-eye"></i></a>
									</li>
									<li class="more-menu-item" role="presentation">
										<a href="{!! route('calendars.edit', [$calendar->id]) !!}"  class='btn btn-info btn-sm' title="Edit"><i class="fa fa-edit"></i></a>
									</li>
								@endif
									<li class="more-menu-item" role="presentation">
										{!! Form::button('<i class="fa fa-trash"></i>', ['title' => 'Delete','type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
									</li>
								<div class="vl"></div>
								@if($calendar->status == 0)
								<li class="more-menu-item" role="presentation">
									<a href="{!! route('calendars.approve', [$calendar->id]) !!}" class='btn btn-success btn-sm' title="Approve"><i class="fa fa-check-circle"></i></a>
								</li>
								<li class="more-menu-item" role="presentation">
									<a href="{!! route('calendars.reject', [$calendar->id]) !!}" class='btn btn-danger btn-sm'  title="Reject"><i class="fa fa-times-circle"></i></a>
								</li>
								@elseif($calendar->status == 1)
								<li class="more-menu-item" role="presentation">
									<a href="{!! route('calendars.reject', [$calendar->id]) !!}" class='btn btn-danger btn-sm' title="Reject"><i class="fa fa-times-circle"></i></a>
								</li>
								@else
								<li class="more-menu-item" role="presentation">
									<a href="{!! route('calendars.approve', [$calendar->id]) !!}" class='btn btn-success btn-sm'  title="Approve"><i class="fa fa-check-circle"></i></a>
								</li>
								@endif

							</ul>
						</div>
					</div>
					{!! Form::close() !!}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
