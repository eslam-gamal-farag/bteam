@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::model($contactForm, ['route' => ['contactForms.update', $contactForm->id], 'method' => 'patch', 'files' => true]) !!}
				@include('contact_forms.fields')
			{!! Form::close() !!}
		</div>
	</div>
@endsection