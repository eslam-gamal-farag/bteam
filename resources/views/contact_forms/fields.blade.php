<!-- Type Field -->
<div class="form-group col-sm-6">
	{!! Form::label('type', 'Type:') !!}
	{!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Subject Field -->
<div class="form-group col-sm-6">
	{!! Form::label('subject', 'Subject:') !!}
	{!! Form::text('subject', null, ['class' => 'form-control']) !!}
</div>

<!-- Message Field -->
<div class="form-group col-sm-6">
	{!! Form::label('message', 'Message:') !!}
	{!! Form::text('message', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
	{!! Form::label('user_id', 'User Id:') !!}
	{!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('contactForms.index') !!}" class="btn btn-default">Cancel</a>
</div>
