@extends('layouts.app')

@section('content')
	<div class="card card-primary">
		<div class="card-body">
			@include('contact_forms.show_fields')
			<a href="{!! route('contactForms.index') !!}" class="btn btn-default">Back</a>
		</div>
	</div>
@endsection
