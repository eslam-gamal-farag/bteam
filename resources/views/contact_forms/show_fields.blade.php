<!-- Id Field -->
<div class="form-group">
	{!! Form::label('id', 'Id:') !!}
	<p>{!! $contactForm->id !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
	{!! Form::label('type', 'Type:') !!}
	<p>{!! $contactForm->type !!}</p>
</div>

<!-- Subject Field -->
<div class="form-group">
	{!! Form::label('subject', 'Subject:') !!}
	<p>{!! $contactForm->subject !!}</p>
</div>

<!-- Message Field -->
<div class="form-group">
	{!! Form::label('message', 'Message:') !!}
	<p>{!! $contactForm->message !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
	{!! Form::label('user_id', 'User Id:') !!}
	<p>{!! $contactForm->user_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
	{!! Form::label('created_at', 'Created At:') !!}
	<p>{!! $contactForm->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
	{!! Form::label('updated_at', 'Updated At:') !!}
	<p>{!! $contactForm->updated_at !!}</p>
</div>

