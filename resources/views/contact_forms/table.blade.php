<table class="table table-striped table-hover" id="contactForms-table">
	<thead>
		<tr>
			<th>Type</th>
        <th>Subject</th>
        <th>Message</th>
        <th>User</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	@foreach($contactForms as $contactForm)
		<tr>
			<td>{!! $contactForm->type !!}</td>
			<td>{!! $contactForm->subject !!}</td>
			<td>{!! $contactForm->message !!}</td>
			<td>{!! $contactForm->user->name !!}</td>
			<td>
				{!! Form::open(['route' => ['contactForms.destroy', $contactForm->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('contactForms.show', [$contactForm->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
					<a href="{!! route('contactForms.edit', [$contactForm->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-edit"></i></a>
					{!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>