@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::model($department, ['route' => ['floorplans.update', $department->id], 'method' => 'patch', 'files' => true]) !!}
				@include('departments.fields')
			{!! Form::close() !!}
		</div>
	</div>
@endsection