{!! Form::hidden('organization_id', session()->get('organization_id')) !!}

<div class="row">
	<!-- Name Field -->
	<div class="form-group col-sm-6">
		{!! Form::label('name', 'Name:') !!}
		{!! Form::text('name', null, ['class' => 'form-control']) !!}
	</div>
	
	<!-- Industry Field -->
	<div class="form-group col-sm-6">
		{!! Form::label('data', 'Data:') !!}
		{!! Form::textarea('data', null, ['class' => 'form-control']) !!}
	</div>


</div>

<div class="row">
	<!-- Submit Field -->
	<div class="form-group col-sm-12">
		{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
		<a href="{!! route('floorplans.index') !!}" class="btn btn-default">Cancel</a>
	</div>
</div>
