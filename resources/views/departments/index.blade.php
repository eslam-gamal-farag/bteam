@extends('layouts.app')

@section('content')
	@include('flash::message')
	<div class="card card-primary">
		<div class="card-header">
			@if(request()->has('trashed'))
			<a class="btn btn-success" href="{!! route('floorplans.index') !!}">
				All
			</a>
			@else
			<a class="btn btn-primary" href="{!! route('floorplans.create') !!}">
				<i class="fa fa-plus"></i> Add New
			</a>
			<a class="btn btn-danger" href="{!! route('floorplans.index', 'trashed') !!}">
				<i class="fa fa-trash"></i> Trashed
			</a>
			@endif
		</div>
		<div class="card-body">
			@include('departments.table')
		</div>
		<div class="card-footer">
			@include('vendor.pagination.paginate', ['records' => $departments])
		</div>
	</div>
@endsection
