@extends('layouts.app')

@section('content')
	<div class="card card-primary">
		<div class="card-body">
			@include('departments.show_fields')
			<a href="{!! route('floorplans.index') !!}" class="btn btn-default">Back</a>
		</div>
	</div>
@endsection
