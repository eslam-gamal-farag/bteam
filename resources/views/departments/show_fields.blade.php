<div class="row ShowData pd-40">
	<div class="col-lg-8 col-md-12 col-xs-12" id="departmentOrganizationData centerElement">
		<!-- Name Field -->
		<div class="form-group">
			{!! Form::label('name', 'Name:') !!}
			<p>{!! $department->name !!}</p>
		</div>
		<!-- Name Field -->
		<div class="form-group">
			{!! Form::label('data', 'Data:') !!}
			<p>{!! $department->data !!}</p>
		</div>
		<!-- Created At Field -->
		<div class="form-group">
			{!! Form::label('created_at', 'Created At:') !!}
			<p>{!! $department->created_at !!}</p>
		</div>

		<!-- Updated At Field -->
		<div class="form-group">
			{!! Form::label('updated_at', 'Updated At:') !!}
			<p>{!! $department->updated_at !!}</p>
		</div>
	</div>
	<!-- Organization Id Field -->
	<div class="form-group col-lg-4 col-md-12 col-xs-12" id="organizationLogo">
	<img src="{!! \App\Models\Organization::find($department->organization_id)->logo !!}" alt="Organization Logo" width="100px">
		<p>{!! \App\Models\Organization::find($department->organization_id)->name !!}</p>
	</div>
</div>
@include('employees.table')
