<table class="table table-striped table-hover" id="departments-table">
	<thead>
		<tr>
			<th>Floor Plan</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	@foreach($departments as $department)
		<tr>
			<td>{!! $department->name !!}</td>
			<td>
				{!! Form::open(['route' => ['floorplans.destroy', $department->id], 'method' => 'delete']) !!}
					<div class="more-action">
						<div id="more-btn" class="more-btn">
							<span class="more-dot"></span>
							<span class="more-dot"></span>
							<span class="more-dot"></span>
						</div>
						<div class="more-menu">
							<div class="more-menu-caret">
								<div class="more-menu-caret-outer"></div>
								<div class="more-menu-caret-inner"></div>
							</div>
							<ul class="more-menu-items" tabindex="-1" role="menu" aria-labelledby="more-btn" aria-hidden="true">
								@if(request()->has('trashed'))
									<li class="more-menu-item" role="presentation">
										<a href="{!! route('departments.restore', [$department->id]) !!}" class='btn btn-warning btn-sm' title="Restore"><i class="fa fa-refresh"></i></a>
									</li>
								@else
									<li class="more-menu-item" role="presentation">
										<a href="{!! route('floorplans.show', [$department->id]) !!}" class='btn btn-success btn-sm' title="Show Details"><i class="fa fa-eye"></i></a>
									</li>
									<li class="more-menu-item" role="presentation">
										<a href="{!! route('floorplans.edit', [$department->id]) !!}"  class='btn btn-info btn-sm' title="Edit"><i class="fa fa-edit"></i></a>
									</li>							
								@endif
									<li class="more-menu-item" role="presentation">
										{!! Form::button('<i class="fa fa-trash"></i>', ['title' => 'Delete','type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
									</li>															
							</ul>
						</div>
					</div>
				{!! Form::close() !!}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>