@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::open(['route' => 'projectresponsibilities.store', 'files' => true]) !!}
				@include('employees.fields')
			{!! Form::close() !!}
		</div>
	</div>
@endsection
