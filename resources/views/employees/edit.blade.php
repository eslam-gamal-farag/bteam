@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::model($user, ['route' => ['projectresponsibilities.update', $user->id], 'method' => 'patch', 'files' => true]) !!}
				@include('employees.fields')
			{!! Form::close() !!}
		</div>
	</div>
@endsection