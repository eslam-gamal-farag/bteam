
<div class="row">
	<!-- Name Field -->
	<div class="form-group col-sm-6">
		{!! Form::label('name', 'Name:') !!}
		<div class="name-avatar">
			<div class="avatar">
				<div class="circle">
					<!-- User Profile Image -->
					<img class="profile-pic" src="/assets/images/avatar.png">
				</div>
				<div class="p-image">
					<i class="fa fa-camera upload-button"></i>
					<input class="file-upload" type="file"  name="employee[avatar]" id="profile-img" accept="image/*"/>
				</div>
			</div>

			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>
	</div>

	<!-- Email Field -->
	<div class="form-group col-sm-6">
		{!! Form::label('email', 'Email:') !!}
		{!! Form::email('email', null, ['class' => 'form-control']) !!}
	</div>
</div>
<div class="row">
	<!-- Role Field -->
	<div class="form-group col-sm-6">
		{!! Form::label('roles', 'Role:') !!}
		{!! Form::select('organization[pivot][role_id]', $roles, null, ['class' => 'form-control select2']) !!}
	</div>
	
	<!-- Department Field -->
	<div class="form-group col-sm-6">
		{!! Form::label('department', 'Floor Plan:') !!}
		{!! Form::select('employee[department_id]', $departments, null, ['id' => 'department_field','class' => 'form-control select2', 'multiple' => false]) !!}
	</div>
</div>
<div class="row">
	<!-- Job Title Field -->
	<div class="form-group col-sm-6">
		{!! Form::label('job_title', 'Job Title:') !!}
		{!! Form::text('employee[job_title]', null, ['class' => 'form-control']) !!}
	</div>
	
	<!-- Cost Field -->
	<div class="form-group col-sm-6">
		{!! Form::label('cost', 'Cost per hour:') !!}
		{!! Form::number('employee[cost]', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="row">
	<!-- Submit Field -->
	<div class="form-group col-sm-12">
		{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
		<a href="{!! route('projectresponsibilities.index') !!}" class="btn btn-default">Cancel</a>
	</div>
</div>
