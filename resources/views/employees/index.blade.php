@extends('layouts.app')

@section('content')
	@include('flash::message')
	<div class="card card-primary">
		<div class="card-header">
			@if(request()->has('trashed'))
			<a class="btn btn-primary" href="{!! route('projectresponsibilities.index') !!}">
				All
			</a>
			@else
			<a class="btn btn-primary" href="{!! route('projectresponsibilities.create') !!}">
				<i class="fa fa-plus"></i> Add New
			</a>
			<a class="btn btn-primary" href="{!! route('importUsers') !!}">
				<i class="fa fa-plus"></i> Import Users
			</a>
			<a class="btn btn-primary" href="{!! route('exportUsers') !!}">
				<i class="fa fa-plus"></i>Export Users
			</a>
			<a class="btn btn-danger" href="{!! route('projectresponsibilities.index', ['trashed']) !!}">
				<i class="fa fa-trash"></i> Trashed
			</a>
			@endif
		</div>
		<div class="card-body">
			@include('employees.table')
		</div>
		<div class="card-footer">
			@include('vendor.pagination.paginate', ['records' => $users])
		</div>
	</div>
@endsection
