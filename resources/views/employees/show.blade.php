@extends('layouts.app')

@section('content')
	<div class="card card-primary">
		<div class="card-body">
			@include('employees.show_fields')
			<a href="{!! route('projectresponsibilities.index') !!}" class="btn btn-default">Back</a>
		</div>
	</div>
@endsection
