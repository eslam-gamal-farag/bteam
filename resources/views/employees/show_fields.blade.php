<div class="row ShowData">
	<div class="pd-30 col-lg-4 col-sm-12 col-xs-12">
		<!-- Avatar Field -->
		<div class="form-group">
			<img id="userAvatar"  src= "{!! $user->employee->avatar !!}" alt="user avatar" width="150px">
		</div>
	</div>

	<div id="userData" class="pd-30 col-lg-8 col-sm-12 col-xs-12">
		<!-- User name Field -->
		<div class="form-group">
			{!! Form::label('name', 'Name:') !!}
			<p>{!! $user->name !!}</p>
		</div>

		<!-- User email Field -->
		<div class="form-group">
			{!! Form::label('email', 'Email:') !!}
			<p>{!! $user->email !!}</p>
		</div>

		<!-- User job title Field -->
		<div class="form-group">
			{!! Form::label('job_title', 'Job Title:') !!}
			<p>{!! $user->employee->job_title !!}</p>
		</div>

		<!-- Department Field -->
		<div class="form-group">
			{!! Form::label('department', 'Floor Plan:') !!}
			<p>{!! $user->employee->department->name !!}</p>
		</div>

		<!-- Cost Field -->
		<div class="form-group">
			{!! Form::label('cost', 'Cost:') !!}
			<p>{!! $user->employee->cost !!}</p>
		</div>
	</div>
</div>
