@extends('layouts.app')

@section('content')
	<div class="card card-primary">
		<div class="card-header">
			{!! Form::open(['url' => 'laborcost', 'method' => 'get']) !!}
			{{-- {!! Form::datetimeLocal('reportStartDate', null, ['class' => 'from-control d-none', 'id'=>'startDate']) !!} --}}
			{{-- {!! Form::datetimeLocal('reportEndDate', null, ['class' => 'from-control d-none', 'id'=>'endDate']) !!} --}}
			<select name="laborcostPeriod" id="laborcostPeriod" onchange="dateChange()">
				<option value="1">Last Week</option>
				<option value="2">Last 2 Weeks</option>
				<option value="3">Last Month</option>
				<option value="4">Last Quarter</option>
				<option value="5">Last Year</option>
			</select>
			{{-- <script>
				function dateChange() {
						if($('#laborcostPeriod').val() == 1) {
							var d = new Date(date),
							month = '' + (d.getMonth() + 1),
							day = '' + d.getDate(),
							year = d.getFullYear();
							var	startDate = [year, month, day].join('-');
							var endDate = startDate.setDate(startDate.getDate() - 7);
							

							$('#startDate').attr('value',startDate);
							$('#endDate').attr('value',endDate);
						}
				}
			</script> --}}
			{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>
		
		<div class="card-body">
			<table class="table table-striped table-hover" id="reports-table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Cost</th>
					</tr>
				</thead>
				<tbody>
					<?php $total = 0; ?>
					@foreach ($users as $user)
					<?php $total += $user->employee? $user->employee->cost * $user->totalHours : 0 ?>
					<tr>
						<td>{{ $user->name }}</td>
						<td>{{ $user->employee? $user->employee->cost * $user->totalHours : '' }}</td>
					</tr>
					@endforeach
					<tfoot>
						<tr>
							<th>Total</th>
							<th>{!! $total !!}</th>
						</tr>
					</tfoot>
				</tbody>
			</table>
		</div>
	</div>
@endsection