<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Twitter -->
	<meta name="twitter:site" content="@bteam.io">
	<meta name="twitter:creator" content="@bteam.io">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="BTeam.io">
	<meta name="twitter:description" content="bteam.io">
	<meta name="twitter:image" content="######bteam.io#######">

	<!-- Facebook -->
	<meta property="og:url" content="http://bteam.io">
	<meta property="og:title" content="BTeam.io">
	<meta property="og:description" content="BTeam.io">
	<meta property="og:image" content="######BTeam.io#######">
	<meta property="og:image:secure_url" content="######BTeam.io#######">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="1200">
	<meta property="og:image:height" content="600">

	<!-- Meta -->
	<meta name="description" content="BTeam.io">
	<meta name="author" content="momentum-sol">


	<title>BTeam.io Admin</title>

	<!-- vendor css -->
	<link href="{!! url('/lib/font-awesome/css/font-awesome.css') !!}" rel="stylesheet">
	<link href="{!! url('/lib/Ionicons/css/ionicons.css') !!}" rel="stylesheet">
	<link href="{!! url('/lib/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">
	<link href="{!! url('/lib/perfect-scrollbar/css/perfect-scrollbar.min.css') !!}" rel="stylesheet">
	<link href="{!! url('/lib/select2/css/select2.min.css') !!}" rel="stylesheet">

	<!-- BTeam.io CSS -->
	<link rel="stylesheet" href="{!! url('/css/slim.css') !!}">
	<link rel="stylesheet" href="{!! url('/css/slim.one.css') !!}">
	<link rel="stylesheet" href="{!! url('/css/buttercup.css') !!}">
	
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
	@yield('css')
</head>

<body>
@if (!Auth::guest())
	@include('layouts.header')

	<div class="slim-body">
		@include('layouts.sidebar')
		<div class="slim-mainpanel">
			<div class="container">
				<div class="slim-pageheader">
					<ol class="breadcrumb slim-breadcrumb">
						{{--  <li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">{!! Request::segment(1) !!}</li>  --}}
					</ol>
					<h6 class="slim-pagetitle">{!! Request::segment(1) !!}</h6>
				</div><!-- slim-pageheader -->
				@yield('content')
			</div><!-- container -->
	
			<div class="slim-footer mg-t-0">
				<div class="container-fluid">
					<p>Copyright 2019 &copy; All Rights Reserved.</p>
					<p>Powered by <a href="#">Integrated.io</a></p>
				</div><!-- container-fluid -->
			</div><!-- slim-footer -->
		</div><!-- slim-mainpanel -->
	</div>
	
@else
	<nav class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-header">

				<!-- Collapsed Hamburger -->
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
						data-target="#app-navbar-collapse">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<!-- Branding Image -->
				<a class="navbar-brand" href="{!! url('/') !!}">
					Buttercup
				</a>
			</div>

			<div class="collapse navbar-collapse" id="app-navbar-collapse">
				<!-- Left Side Of Navbar -->
				<ul class="nav navbar-nav">
					<li><a href="{!! url('/home') !!}">Home</a></li>
				</ul>

				<!-- Right Side Of Navbar -->
				<ul class="nav navbar-nav navbar-right">
					<!-- Authentication Links -->
					<li><a href="{!! url('/login') !!}">Login</a></li>
					<li><a href="{!! url('/register') !!}">Register</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div id="page-content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
	@endif

	<script src="{!! url('/lib/jquery/js/jquery.js') !!}"></script>
	<script src="{!! url('/lib/jquery-ui/js/jquery-ui.js') !!}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
	<script src="{!! url('/lib/popper.js/js/popper.js') !!}"></script>
	<script src="{!! url('/lib/bootstrap/js/bootstrap.js') !!}"></script>
	<script src="{!! url('/lib/jquery.cookie/js/jquery.cookie.js') !!}"></script>
	<script src="{!! url('/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}"></script>
	<script src="{!! url('/lib/select2/js/select2.full.min.js') !!}"></script>
	<script src="{!! url('/js/slim.js') !!}"></script>
	<script src="{!! url('/js/buttercup.js') !!}"></script>
	@yield('scripts')
</body>
</html>