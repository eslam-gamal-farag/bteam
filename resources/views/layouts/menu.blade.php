@can('manage-users')
<li class="sidebar-nav-item with-sub">
	<a href="" class="sidebar-nav-link {{ Request::is('users*') || Request::is('permissions*') || Request::is('roles*') ? 'active' : '' }}">
		<i class="fa fa-user"></i>
		<span>User Management</span>
	</a>
	<ul class="nav sidebar-nav-sub">
		<li class="nav-sub-item">
			<a class="nav-sub-link {{ Request::is('users*') ? 'active' : '' }}" href="{!! route('users.index') !!}">Users</a>
		</li>
		{{-- <li class="nav-sub-item">
			<a class="nav-sub-link {{ Request::is('roles*') ? 'active' : '' }}" href="{!! route('roles.index') !!}">Roles</a>
		</li>
		<li class="nav-sub-item">
			<a class="nav-sub-link {{ Request::is('permissions*') ? 'active' : '' }}" href="{!! route('permissions.index') !!}">Permissions</a>
		</li> --}}
	</ul>
</li>
@endcan
<li class="sidebar-nav-item with-sub">
	<a href="" class="sidebar-nav-link {{ Request::is('projects*') || Request::is('floorplans*') || Request::is('projectresponsibilities*') ? 'active' : '' }}">
	<i class="fa fa-building-o"></i>
	<span>Pro Management</span>
	</a>
	<ul class="nav sidebar-nav-sub">
		@can('manage-users')
		<li class="nav-sub-item">
			<a class="nav-sub-link {{ Request::is('projects*') ? 'active' : '' }}" href="{!! route('projects.index') !!}">Projects</a>
		</li>
		@endcan
		<li class="nav-sub-item">
				<a class="nav-sub-link {{ Request::is('floorplans*') ? 'active' : '' }}" href="{!! route('floorplans.index') !!}">Floor Plans</a>
		</li>
		<li class="nav-sub-item">
				<a class="nav-sub-link {{ Request::is('projectresponsibilities*') ? 'active' : '' }}" href="{!! route('projectresponsibilities.index') !!}">Project Responsibilities</a>
		</li>
		<li class="nav-sub-item">
				<a class="nav-sub-link {{ Request::is('clientdata*') ? 'active' : '' }}" href="{!! route('clientdata.index') !!}">Client Data</a>
			</a>
		</li>
	</ul>
</li>
