<div class="slim-sidebar">
	<label class="sidebar-label">
		<a href="{!! route('orgselect') !!}">
			Pro: {!! isset($organization)? $organization->name : '' !!}
		</a>
	</label>
	<ul class="nav nav-sidebar">
		@include('layouts.menu')
	</ul>
</div><!-- slim-sidebar -->