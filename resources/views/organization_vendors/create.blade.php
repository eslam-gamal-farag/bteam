@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::open(['route' => 'clientdata.store', 'files' => true]) !!}
				@include('organization_vendors.fields')
			{!! Form::close() !!}
		</div>
	</div>
@endsection
