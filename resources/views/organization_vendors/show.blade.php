@extends('layouts.app')

@section('content')
	<div class="card card-primary">
		<div class="card-body">
			@include('organization_vendors.show_fields')
			<a href="{!! route('clientdata.index') !!}" class="btn btn-default">Back</a>
		</div>
	</div>
@endsection
