<!-- Id Field -->
<div class="form-group">
	{!! Form::label('id', 'Id:') !!}
	<p>{!! $organizationVendors->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
	{!! Form::label('name', 'Name:') !!}
	<p>{!! $organizationVendors->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
	{!! Form::label('email', 'Email:') !!}
	<p>{!! $organizationVendors->email !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
	{!! Form::label('phone', 'Phone:') !!}
	<p>{!! $organizationVendors->phone !!}</p>
</div>

<!-- Organization Id Field -->
<div class="form-group">
	{!! Form::label('organization_id', 'Project Id:') !!}
	<p>{!! $organizationVendors->organization_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
	{!! Form::label('created_at', 'Created At:') !!}
	<p>{!! $organizationVendors->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
	{!! Form::label('updated_at', 'Updated At:') !!}
	<p>{!! $organizationVendors->updated_at !!}</p>
</div>

