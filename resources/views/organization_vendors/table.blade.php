<table class="table table-striped table-hover" id="organizationVendors-table">
	<thead>
		<tr>
			<th>Name</th>
       		<th>Email</th>
			<th>Phone</th>
			<th>Project Id</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	@foreach($organizationVendors as $organizationVendors)
		<tr>
			<td>{!! $organizationVendors->name !!}</td>
            <td>{!! $organizationVendors->email !!}</td>
            <td>{!! $organizationVendors->phone !!}</td>
            <td>{!! \App\Models\Organization::find($organizationVendors->organization_id)->name !!}</td>
			<td>
				{!! Form::open(['route' => ['clientdata.destroy', $organizationVendors->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('clientdata.show', [$organizationVendors->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
					<a href="{!! route('clientdata.edit', [$organizationVendors->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-edit"></i></a>
					{!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>