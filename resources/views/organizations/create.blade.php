@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::open(['route' => 'projects.store', 'files' => true]) !!}
				<div class="row">
					@include('organizations.fields')
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
