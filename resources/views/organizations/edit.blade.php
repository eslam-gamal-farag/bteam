@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::model($organization, ['route' => ['projects.update', $organization->id], 'method' => 'patch', 'files' => true]) !!}
				<div class="row">
					@include('organizations.fields')
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection