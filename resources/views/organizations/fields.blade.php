<!-- Name Field -->
<div class="form-group col-sm-6">
	{!! Form::label('name', 'Name:') !!}
	<div class="name-avatar">
			<div class="avatar">
				<div class="circle">
					<!-- User Profile Image -->
					<img class="profile-pic" src="/assets/images/avatar.png">
				</div>
				<div class="p-image">
					<i class="fa fa-camera upload-button"></i>
					<input class="file-upload" type="file"  name="logo" id="profile-img" accept="image/*"/>
				</div>
			</div>

			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>
</div>

<!-- Logo Field -->
<!-- <div class="form-group col-sm-6">
	{!! Form::label('logo', 'Logo:') !!}
	<div class="custom-file">
		{!! Form::file('logo', ['class' => 'custom-file-input']) !!}
		{!! Form::label('logo', 'Choose Image', ['class' => 'custom-file-label']) !!}
		</div>
</div> -->


<!-- Users Field -->
<div class="form-group col-sm-6">
	{!! Form::label('users', 'Users:') !!}
	{!! Form::select('users[]', $users, null, ['class' => 'form-control select2', 'multiple' => true]) !!}
</div>

<div class="clearfix"></div>


<!-- Industry Field -->
<div class="form-group col-sm-6">
	{!! Form::label('industry', 'Data:') !!}
	{!! Form::textarea('industry', null, ['class' => 'form-control']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('projects.index') !!}" class="btn btn-default">Cancel</a>
</div>
