@extends('layouts.app')

@section('content')
	<div class="card card-primary">
		<div class="card-body">
			@include('organizations.show_fields')
			<a href="{!! route('projects.index') !!}" class="btn btn-default">Back</a>
		</div>
	</div>
@endsection
