<div class="row pd-20" id="orgContent">
	<div class="col-lg-3">
		<div id="organizationShowLogo">
			<!-- Logo Field -->
			<div class="form-group">
			{{-- {!! Form::label('logo', 'Logo:') !!} --}}
				<img src=" {!! $organization->logo !!}" alt="Organization Logo" srcset="" width="250px" height="250px">
			</div>
		</div>
	</div>
	<div class="col-lg-9 pd-30 ShowData">
		<!-- Name Field -->
		<div class="form-group">
			{!! Form::label('name', 'Name:') !!}
			<p>{!! $organization->name !!}</p>
		</div>
		<!-- Industry Field -->
		<div class="form-group">
			{!! Form::label('industry', 'Data:') !!}
			<p>{!! $organization->industry !!}</p>
		</div>
		<!-- Created At Field -->
		<div class="form-group">
			{!! Form::label('created_at', 'Created At:') !!}
			<p>{!! $organization->created_at !!}</p>
		</div>			
	</div>
</div>
<h3>Floor Plans</h3>
@include('departments.table')