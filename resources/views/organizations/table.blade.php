<table class="table table-striped table-hover" id="organizations-table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Logo</th>
			<th>Data</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	@foreach($organizations as $organization)
		<tr>
			<td>{!! $organization->name !!}</td>
			<td><img width="50" src="{!! $organization->logo !!}" /></td>
			<td>{!! $organization->industry !!}</td>
			<td>
				{!! Form::open(['route' => ['projects.destroy', $organization->id], 'method' => 'delete']) !!}
					<div class="more-action">
						<div id="more-btn" class="more-btn">
							<span class="more-dot"></span>
							<span class="more-dot"></span>
							<span class="more-dot"></span>
						</div>
						<div class="more-menu">
							<div class="more-menu-caret">
								<div class="more-menu-caret-outer"></div>
								<div class="more-menu-caret-inner"></div>
							</div>
							<ul class="more-menu-items" tabindex="-1" role="menu" aria-labelledby="more-btn" aria-hidden="true">
								<li class="more-menu-item" role="presentation">
									<a href="{!! route('projects.show', [$organization->id]) !!}" class='btn btn-success btn-sm' title="Show Details"><i class="fa fa-eye"></i></a>

								</li>
								<li class="more-menu-item" role="presentation">
									<a href="{!! route('projects.edit', [$organization->id]) !!}" class='btn btn-info btn-sm' title="Edit"><i class="fa fa-edit"></i></a>
								</li>
								<li class="more-menu-item" role="presentation">
									{!! Form::button('<i class="fa fa-trash"></i>', ['title' => 'Delete','type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
								</li>
							</ul>
						</div>
					</div>
				{!! Form::close() !!}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
