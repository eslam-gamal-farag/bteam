@extends('layouts.app')

@section('content')
	@include('flash::message')
	<h3>Please Select an Project</h3>
	<div class="row">
		@foreach ($organizations as $org)
		<div class="col-lg-3 mg-b-10">
			<div class="card-contact">
				<div class="tx-center">
					<img class="card-img img-fluid" src="{!! url($org->logo) !!}" />
					<h5 class="mg-t-10 mg-b-10">
						{!! $org->name !!}
					</h5>
					{!! Form::open(['route' => 'orgselect']) !!}
						{!! Form::hidden('organization_id', $org->id) !!}
						{!! Form::button('Select', ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@endforeach
	</div>
	<div>
		{{--  {!! Form::open(['route' => 'orgselect', 'files' => true]) !!}
			{!! Form::select('organization_id', $organizations, null, ['id'=>'organizationId','class' => 'form-control select2', 'multiple' => false]) !!}
			{!! Form::submit('Go', ['class' => 'btn btn-primary']) !!}
		{!! Form::close() !!}  --}}
	</div>
@endsection
