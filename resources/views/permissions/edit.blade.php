@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::model($permission, ['route' => ['permissions.update', $permission->id], 'method' => 'patch']) !!}
				<div class="row">
					@include('permissions.fields')
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection