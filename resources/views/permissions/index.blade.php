@extends('layouts.app')

@section('content')
	@include('flash::message')
	<div class="card card-primary">
		<div class="card-header">
			<a class="btn btn-primary" href="{!! route('permissions.create') !!}">
				<i class="fa fa-plus"></i> Add New
			</a>
		</div>
		<div class="card-body">
			@include('permissions.table')
		</div>
		<div class="text-center">
			
		</div>
	</div>
@endsection

