@extends('layouts.app')

@section('content')
	<div class="card card-primary">
		<div class="card-body">
			@include('permissions.show_fields')
		</div>
		<div class="card-footer">
				<a href="{!! route('permissions.index') !!}" class="btn btn-default">Back</a>
		</div>
	</div>
@endsection
