<table class="table table-striped table-hover" id="permissions-table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Guard Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	@foreach($permissions as $permission)
		<tr>
			<td>{!! $permission->name !!}</td>
			<td>{!! $permission->guard_name !!}</td>
			<td>
				{!! Form::open(['route' => ['permissions.destroy', $permission->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('permissions.show', [$permission->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
					<a href="{!! route('permissions.edit', [$permission->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-edit"></i></a>
					{!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>