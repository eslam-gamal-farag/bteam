@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::open(['route' => 'roles.store']) !!}
				<div class="row">
					@include('roles.fields')
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
