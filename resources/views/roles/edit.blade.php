@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'patch']) !!}
				<div class="row">
					@include('roles.fields')
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection