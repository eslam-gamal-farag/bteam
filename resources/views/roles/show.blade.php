@extends('layouts.app')

@section('content')
	<div class="card card-primary">
		<div class="card-body">
				@include('roles.show_fields')
		</div>
		<div class="card-footer">
			<a href="{!! route('roles.index') !!}" class="btn btn-default">Back</a>
		</div>
	</div>
@endsection
