@extends('layouts.app')

@section('content')
	@include('adminlte-templates::common.errors')
	<div class="card card-primary">
		<div class="card-body">
			{!! Form::open(['route' => 'users.store']) !!}
				<div class="row">
					@include('users.fields')
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
