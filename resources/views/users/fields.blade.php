<!-- Name Field -->
<div class="form-group col-sm-6">
	{!! Form::label('name', 'Name:') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
	{!! Form::label('email', 'Email:') !!}
	{!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
	{!! Form::label('password', 'Password:') !!}
	{!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
	{!! Form::label('password_confirmation', 'Confirm Password:') !!}
	{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>

<!-- Role Field -->
<div class="form-group col-sm-6">
	{!! Form::label('roles', 'Role:') !!}
	{!! Form::select('roles', $roles, null, ['class' => 'form-control select2']) !!}
</div>

{{--  <!-- Organizations Field -->
<div class="form-group col-sm-6">
	{!! Form::label('organizations', 'Organizations:') !!}
	<button class="org-role-btn btn"><i class="fa fa-plus"></i></button>
	<div class="org-role-input">
		{!! Form::label('organization', 'Organization Name:') !!}
		{!! Form::select('organizations[0][organization_id]', $organizations, null, ['class' => 'form-control select2']) !!}
		{!! Form::label('organization', 'Organization Role:') !!}
		{!! Form::select('organizations[0][role_id]', $roles, null, ['class' => 'form-control select2']) !!}
	</div>
</div>  --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('users.index') !!}" class="btn btn-danger">Cancel</a>
</div>
