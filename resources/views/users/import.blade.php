@extends('layouts.app')

@section('content')
@include('adminlte-templates::common.errors')
<div class="card card-primary">
	<div class="card-header">
		<a href="{{ url('downloadEmployeeCsv') }}"><button class="btn btn-success">Download Sample</button></a>
	</div>
	{!! Form::open(['url' => 'importUsers', 'method' => 'post', 'files' => true]) !!}
	<div class="card-body">
		<div class="custom-file">
			{!! Form::file('import_file', ['class' => 'custom-file-input']) !!}
			{!! Form::label('import_file', 'Choose file', ['class' => 'custom-file-label']) !!}
		</div>
	</div>
	<div class="card-footer">
		{!! Form::submit('Import', ['class' => 'btn btn-primary']) !!}
	</div>
	{!! Form::close() !!}
</div>
@endsection
