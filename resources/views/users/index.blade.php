@extends('layouts.app')

@section('content')
	@include('flash::message')
	<div class="card card-primary">
		<div class="card-header">
			<a class="btn btn-primary" href="{!! route('users.create') !!}">
				<i class="fa fa-plus"></i> Add New
			</a>
		</div>
		<div class="card-body">
			@include('users.table')
		</div>
		<div class="card-footer">
		@include('vendor.pagination.paginate', ['records' => $users])
		</div>
	</div>
@endsection

