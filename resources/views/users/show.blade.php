@extends('layouts.app')

@section('content')
	<div class="card card-primary">
		<div class="card-body">
			@include('users.show_fields')
			<a href="{!! route('users.index') !!}" class="btn btn-default">Back</a>
		</div>
	</div>
@endsection