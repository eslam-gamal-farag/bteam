<!-- Id Field -->
<div class="form-group">
	{!! Form::label('id', 'Id:') !!}
	<b>{!! $user->id !!}</b>
</div>

<!-- Name Field -->
<div class="form-group">
	{!! Form::label('name', 'Name:') !!}
	<b>{!! $user->name !!}</b>
</div>

<!-- Email Field -->
<div class="form-group">
	{!! Form::label('email', 'Email:') !!}
	<b>{!! $user->email !!}</b>
</div>

<!-- Email Verified At Field -->
<div class="form-group">
	{!! Form::label('email_verified_at', 'Email Verified At:') !!}
	<b>{!! $user->email_verified_at !!}</b>
</div>

<!-- Password Field -->
<div class="form-group">
	{!! Form::label('password', 'Password:') !!}
	<b>{!! $user->password !!}</b>
</div>

<!-- Remember Token Field -->
<div class="form-group">
	{!! Form::label('remember_token', 'Remember Token:') !!}
	<b>{!! $user->remember_token !!}</b>
</div>

<!-- Created At Field -->
<div class="form-group">
	{!! Form::label('created_at', 'Created At:') !!}
	<b>{!! $user->created_at !!}</b>
</div>

<!-- Updated At Field -->
<div class="form-group">
	{!! Form::label('updated_at', 'Updated At:') !!}
	<b>{!! $user->updated_at !!}</b>
</div>

<!-- Deleted At Field -->
<div class="form-group">
	{!! Form::label('deleted_at', 'Deleted At:') !!}
	<b>{!! $user->deleted_at !!}</b>
</div>