<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th scope="col">Roles</th>
			<th scope="col">Name</th>
			<th scope="col">Email</th>
			<th scope="col">Email Verified At</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
	@foreach($users as $user)
		<tr>
			<td>{!! $user->roles->pluck('name')->implode('<br>') !!}</td>
			<td>{!! $user->name !!}</td>
			<td>{!! $user->email !!}</td>
			<td>{!! $user->email_verified_at !!}</td>
			<td>
				{!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
					<a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-edit"></i></a>
					{!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>