
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title>Buttercup.io</title>

	<meta name="description" content="Buttercup.io">

	<meta property="og:title" content="Buttercup.io">
	<meta property="og:description" content="Buttercup.io">
	<meta property="og:image" content="#">
	<meta property="og:url" content="https://buttercup.io">
	<meta name="twitter:card" content="summary_large_image">

	<link rel="stylesheet" href="{!! url('css/welcome.css') !!}">
	<link rel="icon" type="image/png" href="images/favicon.png" />
</head>
<body>
	<div class="wrapper">
		<div class="top-section">
			<img class="icon" src="{!! url('/assets/images/bc_logo_full.png') !!}" alt="" height="78">
			{{--  <h1>Buttercup.io</h1>  --}}
			<h2>Team Communication and Management Software</h2>
			<a href="#" class="download-button ios">Download for ios</a>
			<a href="#" class="download-button android">Download for android</a>
			<div class="iphone-container">
				<img class="phone" src="{!! url('/assets/images/iphone.png') !!}" alt="" height="608">
				<a class="signup-button" href="{!! route('register') !!}"></a>
				<a class="signin-button" href="{!! route('login') !!}"></a>
			</div>
		</div>

		<div class="features">
			<div class="features__item  features__item--realtime">
				<p class="heading">Feature 1</p>
				<p class="description">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</p>
			</div>
			<div class="features__item  features__item--realtime">
				<p class="heading">Feature 2</p>
				<p class="description">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</p>
			</div>
			<div class="features__item  features__item--realtime">
				<p class="heading">Feature 3</p>
				<p class="description">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</p>
			</div>
			<div class="features__item  features__item--realtime">
				<p class="heading">Feature 4</p>
				<p class="description">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</p>
			</div>
			<div class="features__item  features__item--realtime">
				<p class="heading">Feature 5</p>
				<p class="description">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</p>
			</div>
		</div>
		<div class="separator"></div>
		<div class="about">
			<p>
				Buttercup.io app made by <a href="#">Integrated.io</a>.
				Feel free to <a href="#">get in touch</a> with questions, issues, or praise you might have.
			</p>
			<p class="copyright">
				Copyright 2018 &copy; All Rights Reserved.
			</p>
		</div>
	</div>
</body>
</html>