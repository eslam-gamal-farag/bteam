<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Auth::routes(['verify' => true]);

Route::apiResource('calendars', 'CalendarAPIController');
Route::apiResource('users', 'UserAPIController');
Route::apiResource('permissions', 'PermissionAPIController');
Route::apiResource('roles', 'RoleAPIController');
Route::apiResource('organizations', 'OrganizationAPIController');
Route::apiResource('contact_forms', 'ContactFormAPIController');
Route::apiResource('organization_vendors', 'OrganizationVendorsAPIController');
Route::apiResource('employees', 'EmployeeAPIController');

Route::get('user', 'Auth\LoginController@user');
Route::post('register_device', 'Auth\RegisterController@registerDevice');

/**
 * Chat routes
 */
Route::get('conversations', 'ChatAPIController@getConversations');
Route::post('conversations', 'ChatAPIController@createChat');
Route::get('conversations/{id}', 'ChatAPIController@getConversationById');
Route::post('conversations/{id}/send', 'ChatAPIController@sendMessage');

Route::get('getMessage', 'ChatAPIController@getMessageById');
Route::get('unreadMessagesCount', 'ChatAPIController@unreadMessagesCount');
Route::get('unreadMessagesCountPerConv', 'ChatAPIController@unreadMessagesCountPerConv');
Route::get('getConversationBetweenTwoUsers', 'ChatAPIController@getConversationBetweenTwoUsers');
Route::post('markAsRead', 'ChatAPIController@markMessageAsRead');
Route::post('addUserToConversation', 'ChatAPIController@addUserToConversation');
Route::post('removeUserFromConversation', 'ChatAPIController@removeUserFromConversation');
