<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('auth.login');
});

Auth::routes(['verify' => true]);

Route::get('/proselect','HomeController@orgselect')->name('orgselect');
Route::post('/proselect','HomeController@orgselect')->name('orgselect');

Route::resource('users', 'UserController');
// Route::resource('permissions', 'PermissionController');
// Route::resource('roles', 'RoleController');
Route::resource('projects', 'OrganizationController');
Route::group(['middleware' => 'orgSelect'], function () {
	Route::get('/home', 'HomeController@index')->name('home');

	
	Route::resource('floorplans', 'DepartmentController');
	
	Route::get('floorplans/{id}/restore', 'DepartmentController@restore')->name('departments.restore');
	Route::resource('projectresponsibilities', 'EmployeeController');
	Route::get('projectresponsibilities/{id}/restore', 'EmployeeController@restore')->name('employees.restore');
	Route::post( '/getUserDepartments', array('as' => 'getUserDepartments', 'uses' => 'EmployeeController@getUserDepartments'));
	
	Route::post('/getOrgUsers', array('as' => 'getOrgUsers', 'uses'=> 'OrganizationController@getOrgUsers'));
	Route::post('/getDepUsers', array('as' => 'getDepUsers', 'uses'=> 'CalendarController@getDepUsers'));
	
	Route::get('/importUsers','UserController@import')->name('importUsers');
	Route::get('/exportUsers','UserController@export')->name('exportUsers');
	Route::get('/downloadEmployeeCsv', 'UserController@getDownload');
	
	// Route::get('/calendars/{id}/approve', array('as' => 'calendars.approve', 'uses'=> 'CalendarController@approve'));
	// Route::get('/calendars/{id}/reject', array('as' => 'calendars.reject', 'uses'=> 'CalendarController@reject'));
	// Route::get('laborcost', 'CostController@index')->name('laborcost');
	// Route::resource('calendars', 'CalendarController');
	// Route::get('calendars/{id}/restore', 'CalendarController@restore')->name('calendars.restore');
	// Route::get('/importCalendars','CalendarController@import')->name('importCalendars');
	// Route::get('/exportCalendars','CalendarController@exportCalendars')->name('exportCalendars');
	// Route::get('/downloadCalendarCsv', 'CalendarController@getDownload');

	
});

// Route::resource('contactForms', 'ContactFormController');
Route::resource('clientdata', 'OrganizationVendorsController');
