<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrganizationVendorsApiTest extends TestCase {
	use MakeOrganizationVendorsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

	/**
	 * @test
	 */
	public function testCreateOrganizationVendors() {
		$organizationVendors = $this->fakeOrganizationVendorsData();
		$this->json('POST', '/api/v1/organizationVendors', $organizationVendors);

		$this->assertApiResponse($organizationVendors);
	}

	/**
	 * @test
	 */
	public function testReadOrganizationVendors() {
		$organizationVendors = $this->makeOrganizationVendors();
		$this->json('GET', '/api/v1/organizationVendors/'.$organizationVendors->id);

		$this->assertApiResponse($organizationVendors->toArray());
	}

	/**
	 * @test
	 */
	public function testUpdateOrganizationVendors() {
		$organizationVendors = $this->makeOrganizationVendors();
		$editedOrganizationVendors = $this->fakeOrganizationVendorsData();

		$this->json('PUT', '/api/v1/organizationVendors/'.$organizationVendors->id, $editedOrganizationVendors);

		$this->assertApiResponse($editedOrganizationVendors);
	}

	/**
	 * @test
	 */
	public function testDeleteOrganizationVendors() {
		$organizationVendors = $this->makeOrganizationVendors();
		$this->json('DELETE', '/api/v1/organizationVendors/'.$organizationVendors->id);

		$this->assertApiSuccess();
		$this->json('GET', '/api/v1/organizationVendors/'.$organizationVendors->id);

		$this->assertResponseStatus(404);
	}
}
