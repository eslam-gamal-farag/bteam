<?php

use App\Models\OrganizationVendors;
use App\Repositories\OrganizationVendorsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrganizationVendorsRepositoryTest extends TestCase {
	use MakeOrganizationVendorsTrait, ApiTestTrait, DatabaseTransactions;

	/**
	 * @var OrganizationVendorsRepository
	 */
	protected $organizationVendorsRepo;

	public function setUp() {
		parent::setUp();
		$this->organizationVendorsRepo = App::make(OrganizationVendorsRepository::class);
	}

	/**
	 * @test create
	 */
	public function testCreateOrganizationVendors() {
		$organizationVendors = $this->fakeOrganizationVendorsData();
		$createdOrganizationVendors = $this->organizationVendorsRepo->create($organizationVendors);
		$createdOrganizationVendors = $createdOrganizationVendors->toArray();
		$this->assertArrayHasKey('id', $createdOrganizationVendors);
		$this->assertNotNull($createdOrganizationVendors['id'], 'Created OrganizationVendors must have id specified');
		$this->assertNotNull(OrganizationVendors::find($createdOrganizationVendors['id']), 'OrganizationVendors with given id must be in DB');
		$this->assertModelData($organizationVendors, $createdOrganizationVendors);
	}

	/**
	 * @test read
	 */
	public function testReadOrganizationVendors() {
		$organizationVendors = $this->makeOrganizationVendors();
		$dbOrganizationVendors = $this->organizationVendorsRepo->find($organizationVendors->id);
		$dbOrganizationVendors = $dbOrganizationVendors->toArray();
		$this->assertModelData($organizationVendors->toArray(), $dbOrganizationVendors);
	}

	/**
	 * @test update
	 */
	public function testUpdateOrganizationVendors() {
		$organizationVendors = $this->makeOrganizationVendors();
		$fakeOrganizationVendors = $this->fakeOrganizationVendorsData();
		$updatedOrganizationVendors = $this->organizationVendorsRepo->update($fakeOrganizationVendors, $organizationVendors->id);
		$this->assertModelData($fakeOrganizationVendors, $updatedOrganizationVendors->toArray());
		$dbOrganizationVendors = $this->organizationVendorsRepo->find($organizationVendors->id);
		$this->assertModelData($fakeOrganizationVendors, $dbOrganizationVendors->toArray());
	}

	/**
	 * @test delete
	 */
	public function testDeleteOrganizationVendors() {
		$organizationVendors = $this->makeOrganizationVendors();
		$resp = $this->organizationVendorsRepo->delete($organizationVendors->id);
		$this->assertTrue($resp);
		$this->assertNull(OrganizationVendors::find($organizationVendors->id), 'OrganizationVendors should not exist in DB');
	}
}
