<?php

use Faker\Factory as Faker;
use App\Models\OrganizationVendors;
use App\Repositories\OrganizationVendorsRepository;

trait MakeOrganizationVendorsTrait {
	/**
	 * Create fake instance of OrganizationVendors and save it in database
	 *
	 * @param array $organizationVendorsFields
	 * @return OrganizationVendors
	 */
	public function makeOrganizationVendors($organizationVendorsFields = []) {
		/** @var OrganizationVendorsRepository $organizationVendorsRepo */
		$organizationVendorsRepo = App::make(OrganizationVendorsRepository::class);
		$theme = $this->fakeOrganizationVendorsData($organizationVendorsFields);
		return $organizationVendorsRepo->create($theme);
	}

	/**
	 * Get fake instance of OrganizationVendors
	 *
	 * @param array $organizationVendorsFields
	 * @return OrganizationVendors
	 */
	public function fakeOrganizationVendors($organizationVendorsFields = []) {
		return new OrganizationVendors($this->fakeOrganizationVendorsData($organizationVendorsFields));
	}

	/**
	 * Get fake data of OrganizationVendors
	 *
	 * @param array $postFields
	 * @return array
	 */
	public function fakeOrganizationVendorsData($organizationVendorsFields = []) {
		$fake = Faker::create();

		return array_merge([
			'name' => $fake->word,
            'email' => $fake->word,
            'phone' => $fake->word,
            'organization_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
		], $organizationVendorsFields);
	}
}
